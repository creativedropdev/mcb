<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use TCG\Voyager\Traits\Resizable;
class Catalog extends Model
{
	use Resizable;
	public function catcategory()
    {
        return $this->belongsTo(CatalogCategory::class, 'category');
    }
    public function save(array $options = [])
    {

    	if($this->file=="[]")
    	{
    		unset($this->file);
    	}

        if($this->file_sp=="[]")
    	{
    		unset($this->file_sp);
    	}

        if($this->file_ar=="[]")
    	{
    		unset($this->file_ar);
    	}
    	
        if($this->file_fr=="[]")
    	{
    		unset($this->file_fr);
    	}
         if($this->file_pt=="[]")
    	{
    		unset($this->file_pt);
    	}

        if($this->cover=="")
        {
            $this->cover="settings/cover.png";
        }


    	
    	parent::save();
    }
}
