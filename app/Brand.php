<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;
use TCG\Voyager\Traits\Resizable;
use Illuminate\Support\Str;
use App;
class Brand extends Model
{
       use Translatable;
       use Resizable;

    protected $translatable = ['title', 'slug', 'description','menu'];


    public function product()
    {
        return $this->belongsToMany(Product::class,'product_brands')->withPivot('product_id')->orderBy("id","desc");
    }
    public function getExcerptAttribute()
    {

        return Str::words($this->translate(App::getLocale())->description, '25');
    }
    
    
    
        public function parseSliders()

    {
        $data=$this->bb_parse($this->description);
    // dd($data);

        $this->description =$data['description'];
       
        $this->description=$this->bb_parse_original($this->description);
        $this->sliders=$data['sliders'];
        //  dd($this->description);
    }


    public function getSlidersJavascriptAttribute()
    {

        $js="";
        foreach($this->sliders as $slider)
        {

            $slider=CommonSlider::find($slider);
         
            if(!empty($slider) )
            {
                if($slider->type!=0)
                {
               $js.=$slider->showSlider()."
               ";
                }
            }
        }
    
        return $js;

    }
    public function bb_parse($string) {
        $tags = 'slider';
        $sliders=[];
        // $pattern= '/\[(\/?shortcode_name.*?(?=\]))\]/';
        while (preg_match_all('`\[('.$tags.')=?(.*?)(.+?)\]`', $string, $matches)) foreach ($matches[0] as $key => $match) {
            list($tag, $param, $innertext) = array($matches[1][$key], $matches[2][$key], $matches[3][$key]);
            switch ($tag) {

                case 'slider':
                // dd($innertext);
                $param=trim($innertext);
                $sliders[]=$param;
                $slider=CommonSlider::find($param);
                if($slider->type==0)//images
                {
                    $replacement=$slider->showSlider();
                }
                else
                {
                    $replacement='<div id="slider_'.$param.'"></div>';
                }

                // $replacement = "<span style=\"font-size: $param;\">".$slider->showSlider()[0]."</span>";


                break;

            }
            $string = str_replace($match, $replacement, $string);
        }
        return array('description' => $string,
        'sliders' => $sliders);
    }



    public function bb_parse_original($string) {
        $tags = 'b|i|size|color|center|quote|url|img|caption';
        while (preg_match_all('`\[('.$tags.')=?(.*?)\](.+?)\[/\1\]`', $string, $matches)) foreach ($matches[0] as $key => $match) {
            list($tag, $param, $innertext) = array($matches[1][$key], $matches[2][$key], $matches[3][$key]);
            switch ($tag) {
                case 'b': $replacement = "<strong>$innertext</strong>"; break;
                case 'i': $replacement = "<em>$innertext</em>"; break;
                case 'size': $replacement = "<span style=\"font-size: $param;\">$innertext</span>"; break;
                case 'color': $replacement = "<span style=\"color: $param;\">$innertext</span>"; break;
                case 'center': $replacement = "<div class=\"centered\">$innertext</div>"; break;
                case 'quote': $replacement = "<blockquote>$innertext</blockquote>" . $param? "<cite>$param</cite>" : ''; break;
                case 'url': $replacement = '<a href="' . ($param? $param : $innertext) . "\">$innertext</a>"; break;
                case 'img':
                    list($width, $height) = preg_split('`[Xx]`', $param);
                    $replacement = "<img src=\"$innertext\" " . (is_numeric($width)? "width=\"$width\" " : '') . (is_numeric($height)? "height=\"$height\" " : '') . '/>';
                break;
                case 'video':
                    $videourl = parse_url($innertext);
                    parse_str($videourl['query'], $videoquery);
                    if (strpos($videourl['host'], 'youtube.com') !== FALSE) $replacement = '<embed src="http://www.youtube.com/v/' . $videoquery['v'] . '" type="application/x-shockwave-flash" width="425" height="344"></embed>';
                    if (strpos($videourl['host'], 'google.com') !== FALSE) $replacement = '<embed src="http://video.google.com/googleplayer.swf?docid=' . $videoquery['docid'] . '" width="400" height="326" type="application/x-shockwave-flash"></embed>';
                break;
                case 'caption':
                // <div id="attachment_7063" style="width: 310px" class="wp-caption alignnone">
                // </div>
                dd($param);
                break;
            }
            $string = str_replace($match, $replacement, $string);
        }
        return $string;
    }
    
    
    
}
