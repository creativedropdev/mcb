<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Voyager;

class CommonSlider extends Model
{
    public function getWidgetsAttribute()
    {


    }


    public function getVideosList()
    {
        $videos = explode(PHP_EOL, $this->videos);
            $vidsList=array();
            foreach($videos as $video)
            {

                if(strlen($video)>5)
                {



                    $video1=parse_url($video);
                    parse_str($video1['query'], $video);
                    if(isset($video['v']))
                    {
                        $video=$video['v'];
                        $vidsList[]=$video;
                    }
                    else
                    {
                        $video=null;
                    }



                }




            }
            return ($vidsList);

    }
    public function showSlider()
    {

        $code="";

        if($this->type==0) //images
        {

            $carousel_inner="";
            $carousel_indicators="";
            $titles = explode(PHP_EOL, $this->titles);


            $images=json_decode($this->images);
            // $images = explode(PHP_EOL, $this->images);

            $active="active";
            $n=0;
            foreach($images as $image)
            {

                // dd($image);
                $carousel_inner.='<div class="carousel-item '.$active.'">
                <img class="d-block w-100" src="'.Voyager::image($image).'">
                <div class="carousel-caption d-none d-md-block">
                <h5>
                '.((isset($titles[$n]))? $titles[$n]: "").'
                </h5>
                </div>
              </div>';

              $carousel_indicators.='<li data-target="#carouselindicators_'.$this->id.'" data-slide-to="'.$n.'" class=" '.$active.'"></li>';
                // dd();
                $n++;
                $active="";
            }

            $carousel='<div id="carouselindicators_'.$this->id.'" class="carousel slide commonSlider" data-ride="carousel">
            <ol class="carousel-indicators">
            '.$carousel_indicators.'
            </ol>
            <div class="carousel-inner">
            '.$carousel_inner.'
            </div>
            <a class="carousel-control-prev" href="#carouselindicators_'.$this->id.'" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselindicators_'.$this->id.'" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>';
          
          if($this->theme==1)
          {
              $carousel.="<style>
                .carousel-indicators li
                {
                    background-color:black;
                }
                .carousel-indicators .active
                {
                    background-color: #ccc;
                }
                .carousel-control-next-icon {

                    background-image: url(data:image/svg+xml;charset=utf8,%3Csvg\ xmlns=\'http://www.w3.org/2000/svg\'\ fill=\'%23000\'\ viewBox=\'0\ 0\ 8\ 8\'%3E%3Cpath\ d=\'M2.75\ 0l-1.5\ 1.5\ 2.5\ 2.5-2.5\ 2.5\ 1.5\ 1.5\ 4-4-4-4z\'/%3E%3C/svg%3E);

                 }
                 .carousel-control-prev-icon {

                    background-image: url(data:image/svg+xml;charset=utf8,%3Csvg\ xmlns=\'http://www.w3.org/2000/svg\'\ fill=\'%23000\'\ viewBox=\'0\ 0\ 8\ 8\'%3E%3Cpath\ d=\'M5.25\ 0l-4\ 4\ 4\ 4\ 1.5-1.5-2.5-2.5\ 2.5-2.5-1.5-1.5z\'/%3E%3C/svg%3E);
                
                }

              </style>";
          }
          else if($this->theme==2)
          {
              $carousel.="<style>
                .carousel-indicators li
                {
                    background-color:#005ab5;
                }
                .carousel-indicators .active
                {
                    background-color: #e9292f;
                }
                .carousel-control-next-icon {

                    background-image: url(data:image/svg+xml;charset=utf8,%3Csvg\ xmlns=\'http://www.w3.org/2000/svg\'\ fill=\'%23005ab5\'\ viewBox=\'0\ 0\ 8\ 8\'%3E%3Cpath\ d=\'M2.75\ 0l-1.5\ 1.5\ 2.5\ 2.5-2.5\ 2.5\ 1.5\ 1.5\ 4-4-4-4z\'/%3E%3C/svg%3E);

                 }
                 .carousel-control-prev-icon {

                    background-image: url(data:image/svg+xml;charset=utf8,%3Csvg\ xmlns=\'http://www.w3.org/2000/svg\'\ fill=\'%23005ab5\'\ viewBox=\'0\ 0\ 8\ 8\'%3E%3Cpath\ d=\'M5.25\ 0l-4\ 4\ 4\ 4\ 1.5-1.5-2.5-2.5\ 2.5-2.5-1.5-1.5z\'/%3E%3C/svg%3E);
                
                }

              </style>";
          }

          return $carousel;
        }
        else
        {


            $videos=$this->getVideosList();
            // dd($videos);
            $code.="$('#slider_".$this->id."').youtube_video({
                videos: ".json_encode($videos).",
                autoplay:true,

                show_playlist: 'yes',
				playlist_type: 'horizontal',

              });";



        return $code;



        }
    }
}
