<?php

namespace App\Http\Controllers;
use App\CatalogCategory;
use App\Catalog;
use Illuminate\Http\Request;

class CatalogController extends Controller
{
    //

    public function index(){
        // $brands = new Brand();
        $CatalogCategory = CatalogCategory::all();
        // dd($CatalogCategory);
        return view('Front.Catalog.index')->with('CatalogCategory',$CatalogCategory);
    }
}
