<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Brand;
use App\Slider;
use App\Product;
use App\Testimonial;
use Cookie;

class LanguageController extends Controller
{
    //
    public function changeLanguage(Request $request){
       
       $input=$request->input();

       $uri=$input['uri'];
       $lang=$input['lang'];

       if(strpos($uri, "/en")===0 OR strpos($uri, "/ar")===0)
       {

            $uri=substr($uri, 3);

       }

       $uri="/".$lang.$uri;

        $cookie = Cookie::forever('locale', $lang);


        return redirect($uri)->withCookie($cookie);


    }
}
