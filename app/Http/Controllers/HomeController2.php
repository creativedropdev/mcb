<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Branch;
use App\Brand;
use App\Slider;
use App\Product;
use App\Testimonial;
use App\Post;
use App;
use Response;
class HomeController extends Controller
{
    //
    public function home(){
        $brands = Brand::all();
        $slider = Slider::all();
        $products = Product::all();
        $testimonials =Testimonial::all();
        $posts = Post::orderBy("id","desc")->take(3)->get();
        $branches = Branch::all();
        // dd($posts);
        // print_r($brands);
        // exit();
        return view('Front.home')->with(['branches'=>$branches, 'brands'=> $brands,'slider'=>$slider,'products' => $products,'testimonials'=>$testimonials, 'posts'=>$posts]);
    }


       public function redirect(){
        

        return redirect('/'.App::getLocale()."/");
        // dd();
    }
    
    public function css( Request $request)
    {
       if($request->get('css')!=null)
       {
        //   dd($request->get('css'));
        
            $contents=urldecode($request->get('css'));
            $response = Response::make($contents);
    $response->header('Content-Type', 'text/css');
    return $response;


        //   echo urldecode($request->get('css'));
       }
    }
}
