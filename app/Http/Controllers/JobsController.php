<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Job;
use App\Page;
use App;
use App\Http\Controllers\PageController;
use Illuminate\Support\Facades\Session;


class JobsController extends Controller
{
    //
    public function getAllJobs()
    {
        $jobs = Job::all();
        return view('career.opening-jobs')->with('jobs', $jobs);


    }

    public function getSinglejob($slug,Request $request)
    {

        $page = Page::where('slug', '=', $slug)->first();
        
        if($page==null)
        {
        $getSingleJob = Job::where('slug', '=', $slug)->get();
        return view('career.apply-jobform')->with('singleJob', $getSingleJob);
        }
        else{

            //invoke PageController if Page exists for same slug.
            return app()->call('App\Http\Controllers\PageController@getPage',['slug'=>$slug]);
        }
    }
    public function sendEmail(request $request)
    {
        $data  = $request->all();
//       dd($data);
         Session::flash('message', 'Thank you very much for application us. We will back to you soon');
        Session::flash('alert-class', 'alert-success'); 
//        return view('Front.Contact.contactus', compact('contacts', 'branches', 'layoutData'));
            return \Illuminate\Support\Facades\Redirect::to('/en/career/internship-program');


    }
    
    
    public function internshipHandler(request $request)
    {
        $data  = $request->all();
       Session::flash('message', 'Thank you very much for application. We will back to you soon');
        Session::flash('alert-class', 'alert-success'); 
//        return view('Front.Contact.contactus', compact('contacts', 'branches', 'layoutData'));
            return \Illuminate\Support\Facades\Redirect::to('/en/career/internship-program');


    }
}
