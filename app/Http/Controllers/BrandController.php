<?php

namespace App\Http\Controllers;
use App\Brand;
use App\Product;
use Illuminate\Http\Request;
use App;
use DB;
class BrandController extends Controller
{
    //
    public function getSingleBrand($brandSlug)
    {
        $brands = new Brand();
        $singleBrand = Brand::where('slug', '=', $brandSlug)->with('product')->first();
        $brandVidoes = App\Page::where('id', '=', 36)->first();
//        print ">>".$brandVidoes->title.$brandVidoes->body; die;
         if($singleBrand==null)
        {
            //check in arabic products
            $singleBrand=DB::table('translations')->where('table_name','brands')->where('column_name','slug')->where('value',$brandSlug)->select('foreign_key');
            if($singleBrand->count()>0)
            {
                
                $singleBrand = Brand::find($singleBrand->first()->foreign_key);
                // $thumbnail=$page->thumbnail('banner');
                $layoutData['langSwitchUrl']=$singleBrand->translate('en')->slug;
                $singleBrand=$singleBrand->translate('ar');
                
                App::setLocale('ar');
                // $page->thumbnailImage=$thumbnail;
                // dd($page);
                  
            }
            else
            {
                dd("Brand not found");
            }

            // dd($singleBrand);

        }
        
        if(App::getLocale()=='en'){
            $singleBrand->parseSliders();
            $common['slidersJs']=$singleBrand->SlidersJavascript;
        
        }
        $singleBrand->description=App\Widget::first()->parseWidget( $singleBrand->description);
        $brand=$singleBrand;
        return view('Front.Brands.singlebrand', compact('brand','singleBrand','commmon', 'brandVidoes'));
    }
    public function allBrands(){
        // $brands = new Brand();

        if(App::getLocale()=='en')
        {
            $layoutData['langSwitchUrl']='/ar/brands-ar';
        }
        else
        {
            $layoutData['langSwitchUrl']='/en/brands';
        }
        
        $allbrands = Brand::all()->translate(App::getLocale());
        return view('Front.Brands.brands', compact('allbrands','layoutData'));
    }
}
