<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Category;
use DB;
class PostController extends Controller
{
    //


    public function postList()
    {
        // $blogs = Post::all();
         $categories=Category::all();
        $posts = Post::where('status', '=', 'PUBLISHED')->orderBy('id', 'DESC')->withTranslations('ar')->paginate(10);
        // die();
        return view('Blogs.index',compact('posts','categories'));
    }

    public function deletePost($blogId)
    {
        // $postDelete = Post::where('id', '=', $blogId)->delete();
        // if ($postDelete) {
        //     return redirect('posts');
        // }
    }

    public function getSinglePost($slug, Request $request)
    {
        $categories=Category::all();

        if($request->get('s')!=null)
        {

            $searchTerm=$request->get('s');

            $posts = Post::where('title', 'LIKE', "%".$searchTerm."%")->withTranslations('ar')->paginate(10);;
            $posts->withPath('/blog?s='.$searchTerm);
            // dd($posts);


            return view('Blogs.index',compact('posts','categories','searchTerm'));
        }
        

        $post = Post::where('slug', '=', $slug)->first();

        // dd($post->translate('ar'));
        if($post==null)
        {

            $category=Category::where('slug', '=', $slug)->first();

            // $category->post;
            // die;
//             $department = Department::findOrFail($id);
// $past = $department->users()
//     ->wherePivot('term_end_date', '<', '2017-10-10')
//     ->get(); /

             $ids=DB::table('post_category_pivot')->where('category_id', $category->id)->select('post_id')->get()->toArray();
             $ids1=[];
             foreach ($ids as $key => $value) {
                 # code...
                $ids1[$key]=$value->post_id;
                // dd($value);
             }
             // dd($ids1);
        // dd(Post::whereIn('id',$ids1)->first());
            $posts = Post::whereIn('id',$ids1)->where('status', '=', 'PUBLISHED')->orderBy('id', 'DESC')->withTranslations('ar')->paginate(10);
             // $posts = $category->post->paginate(10);

             // foreach ($posts as $post) {
             //        $post->withTranslations('ar');
             //     # code...
             // }
            // dd($blogs);
            return view('Blogs.index',compact('posts','categories'));
            // dd($category->post);

        }

        $post->parseSliders();
        $common['slidersJs']=$post->SlidersJavascript;
        $common['title']=($post->seo_title!="")?$post->seo_title:$post->title;
        $common['description']=$post->meta_description;
        $common['keywords']=$post->meta_keywords;
        $common['canonical']=$post->canonical;
        // dd($common);
        
        $pageTemplates = ['webinar', 'webinar-ar' ,'goal', 'goal-ar', 'milestones'];
        
        //print $post->category_id; die;
        if($post->category_id == '9')
        {
            return view('Blogs.single-webinar' ,compact('post','categories','common'));

        }
//         return view('Front.Pages.single-page',compact('page','layoutData','common'));
         
            
        return view('Blogs.single-post',compact('post','categories','common'));
    }




}
