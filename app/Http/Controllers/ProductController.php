<?php

namespace App\Http\Controllers;

use Spatie\SchemaOrg\Schema;
          use Spatie\SchemaOrg\LocalBusiness;
          use Spatie\SchemaOrg\Products;
          use Spatie\SchemaOrg\Brand as SchemaBrand;

use App\Product;
use App\Brand;
use App\ProductCategory;
use App\Post;
use App\Page;
use Illuminate\Http\Request;
use DB;
use App\ProductPartNumber;
use Voyager;
use App;
use App\Widget;
use App\ActiveWidget;
use App\CommonSlider;
class ProductController extends Controller
{
    //

    public function productList(Request $request)
    { $slug = "";
        // $products = new Product();
        // // $allproducts = Product::all();

        // $allproducts = Product::with('brand')->with('productCategory')->get();
        $mainCatText="";
        
        if($request->segment(2)!=null)
        {

            

            if($request->segment(2)=="industrial")
            {
                $mainCat=1;
                $mainCatText="Industrial";
            }
            else
            if($request->segment(2)=="oil-seal")
            {
                $mainCat=8;
                $mainCatText="oil-seal";
            }
            else
            {

                $mainCat=0;
                $mainCatText="Automotive";
            }

            // dd(App::getLocale());
            $categories = ProductCategory::where('parent',$mainCat)->with('product')->get();

        }
        else
        {
                $categories = ProductCategory::with('product')->get();
        }


        // dd($allproducts);
        // foreach ($allproducts as $value) {
        //     # code...
        //     dd($value->product);
        // }
        return view('Front.Products.index', compact('categories','mainCatText', 'slug'));
  // echo json_encode($allproducts);
    }


    public function categories($category="")
    {
        // $products = new Product();
        // // $allproducts = Product::all();
        // $allproducts = Product::all();
        // $allproducts = Product::with('brands')->with('product_categories');

        $mainCatText="";
        //Get All Categories;
        if($category=="")
        {
            $categories=ProductCategory::all();

        }
        else
        {

            $categories=ProductCategory::where("slug",$category)->get();

        }



//    print_r($allproducts);
        // return view('Front.Products.index', compact('products','allproducts'));
        return view('Front.Products.categories', compact('categories','mainCatText'));

        // echo json_encode($allproducts);
    }


    public function getsingleProduct($slug)
    {
        
        // dd(CommonSlider::findOrFail(2)->showSlider());

            // App::setLocale('ar');
        // $products = new Product();
        // dd("d");

        App::setLocale('en');
        $widgets= ActiveWidget::where('type','0')->first()->widgets;

        $product = Product::where('slug', $slug)->first();
        if($product==null)
        {
            //check in arabic products
            $product=DB::table('translations')->where('table_name','products')->where('column_name','slug')->where('value',$slug)->select('foreign_key');

            if($product->count()>0)
            {

                $product = Product::find($product->first()->foreign_key);
                $thumbnail=$product->thumbnail('cropped');
                $layoutData['langSwitchUrl']=$product->translate('en')->slug;

                App::setLocale('ar');

                $product->parseSliders();
                $product=$product->translate('ar');
                $product->description=$product->descriptionTranslated;
                // dd($product->SlidersJavascript);




                $product->image=$thumbnail;
                // dd($product);

            }
            else
            {
                $product=null;


            }
        }
        else
        {
                $product->parseSliders();
              $layoutData['langSwitchUrl']=$product->translate('ar')->slug;
        }

        // dd($product);
        $mainCatText="";
        if($product==null)
        {


                 $categories=ProductCategory::where("slug",$slug)->get();
          if($categories->count()==0)
            {


                 $categories=DB::table('translations')->where('table_name','product_categories')->where('column_name','slug')->where('value',$slug)->select('foreign_key');

                  $categories=ProductCategory::where("id",$categories->first()->foreign_key)->get();

                   App::setLocale('ar');

            }
            // dd($common);
            $common['title']=$categories->first()->name;
            $mainCatText = $categories->first()->name;


             return view('Front.Products.index', compact('categories','mainCatText','common', 'slug'));

        }
        else
        {
        // return view('Front.Products.singleproduct')->with('product',$product);

        // dd($product);


        // dd($product->SlidersJavascript);
         $categories=ProductCategory::all();

         // dd($product->brand->first()->title);

         $business = ['name' => 'MCB'];
    // ->contactPoint(Schema::contactPoint()->areaServed('Worldwide'));
    //          ->if(isset($business['email']), function (LocalBusiness $schema) {
    //     $schema->email($business['email']);
    // });
// dd($product);

$localBusiness = Schema::Product()
    ->name($product->title)
     ->image(asset(Voyager::image($product->image)))
    ->description($product->description)
    ->if(isset($product), function (\Spatie\SchemaOrg\Product $schema) use ($product) {
        $brands=[];
       foreach($product->brand as $brand){

                $brands[]=Schema::Brand()->name($brand->title);

                    }
        $schema->brand($brands);
    })
    ->if(isset($product), function (\Spatie\SchemaOrg\Product $schema) use ($product) {
        $partNumbers=[];

       foreach($product->partNumbers as $partNumber){
                // dd($partNumber);
                $partNumbers[]=$partNumber->number;

                    }
        $schema->mpn($partNumbers);
    })
    ->sku($product->id)
    ;
// foreach($product->brand){brand(Schema::Brand()->name($product->brand->first()->title))}
    $schema=$localBusiness->toScript();
    // die;
            $common['title']=($product['seo_title']!="")?$product['seo_title']:$product['title'];
        //  $common['title'] = $product['seo_title'];
         $common['canonical'] = $product['seo_cname'];
         $common['description'] = $product['seo_description'];
         $common['keywords'] = $product['seo_keywords'];

         $common['slidersJs']=$product->SlidersJavascript;
         return view('Front.Products.singleproduct', compact('product','categories','mainCatText','widgets','layoutData','common','schema', 'slug'));

        }

        // echo Json_encode($singlePrjoect);
    }

    public function search()
    {
        return view('Products.index');
    }

    public function searchProduct(Request $request)
    { 
        if ($request->ajax() && $request->search!="") {
            $output = "";


            $products=ProductPartNumber::where('number','LIKE',"%$request->search%")->with('product')->limit(5)->get();
            if ($products->count()>0) {
                $output .= '<div class="col-lg-3"><div class="row"><div class="col-md-12"><h3>Part Numbers

                        </div></div>';

                        $n=0;
                foreach ($products as $key => $ProductPartNumber) {
                    $product=$ProductPartNumber->product;
                    // var_dump($product);
                    // die;
                    if($product!=null)
                    {
                    $output .= '<div class="row"><div class="col-md-10"><a href="/products/'.$product->slug.'">' .
                      $ProductPartNumber->number .'</a><br>'.

                      '<a href="/products/'.$product->slug.'">'.$product->title .'</a>' .
                      // $product->title .'</a></div>';


                        '</div>';


                      //   $output .= '<div class="col-md-6"><a href="/products/'.$product->slug.'">' .
                      // $product->title .'</a></div>';

                        $output .= '<div class="col-md-2"><a href="/products/'.$product->slug.'">
                      <img class="img-fluid" src="'. Voyager::image( $product->thumbnail('cropped')).'" alt=""> </a>'.

                        '</div></div>';
                    }
                }


                $output .= '</div>';

            }



            $products = DB::table('products')->where('title', 'LIKE', '%' . $request->search . "%")->limit(5)->get();
            if ($products->count()>0) {

                 $output .= '<div class="col-lg-3"><div class="row"><div class="col-md-12"><h3>Products

                        </div></div>';
                foreach ($products as $key => $product) {
                    $output .= '<div class="row"><div class="col-md-12"><a href="/products/'.$product->slug.'">' .
                      $product->title .'</a>'.

                        '</div></div>';
                }
               $output .= '</div>';
            }

            $posts = DB::table('posts')->where('title', 'LIKE', '%' . $request->search . "%")->orWhere('body', 'LIKE', '%' . $request->search . "%")->limit(5)->get();
            if ($posts->count()>0) {

                $output .= '<div class="col-lg-3"><div class="row"><div class="col-md-12"><h3>Posts

                        </div></div>';
                foreach ($posts as $key => $post) {
                    $output .= '<div class="row"><div class="col-md-12"><a href="/'.$post->slug.'">' .
                      $post->title .'</a>'.

                        '</div></div>';
                }
               $output .= '</div>';
            }

            $brands = DB::table('brands')->where('title', 'LIKE', '%' . $request->search . "%")->limit(5)->get();
            if ($brands->count()>0) {

                $output .= '<div class="col-lg-3"><div class="row"><div class="col-md-12"><h3>Brands

                        </div></div>';
                foreach ($brands as $key => $brand) {
                    $output .= '<div class="row"><div class="col-md-12"><a href="/en/brands/'.$brand->slug.'">' .
                      $brand->title .'</a>'.

                        '</div></div>';
                }
                 $output .= '</div>';
            }

             return Response($output);
        }
    }

}
