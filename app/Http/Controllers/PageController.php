<?php

namespace App\Http\Controllers;

use App\Page;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;
use App;
use DB;
class PageController extends Controller
{
    /**
     * This is the module's view path that can be overriden
     */
    protected $viewPath = 'voyager-pages';

    /**
     * Route: Gets a single page and passes data to a view
     *
     * @param string $slug
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getPage($slug = 'home')
    {
         // dd( App::getLocale());
        $page = Page::where('slug', '=', $slug)->first();

        // echo "{$this->viewPath}::modules.pages.default";

        if($page==null)
        {
            //check in arabic products
            $page=DB::table('translations')->where('table_name','pages')->where('column_name','slug')->where('value',$slug)->select('foreign_key');

            if($page->count()>0)
            {

                $page = Page::find($page->first()->foreign_key);
                $thumbnail=$page->thumbnail('banner');
                $layoutData['langSwitchUrl']=$page->translate('en')->slug;
                $page=$page->translate('ar');

                App::setLocale('ar');
                $page->thumbnailImage=$thumbnail;
                // dd($page);

            }
            else
            {

            }
        }
        else
        {
              $layoutData['langSwitchUrl']=$page->translate('ar')->slug;
              // dd($layoutData);
        }


        if($page->category=='0')
        {




        }

        if(App::getLocale()=='ar')
        {

            $layoutData['langSwitchUrl']='/en/company/'.$layoutData['langSwitchUrl'];
        }
        else
        {
        $layoutData['langSwitchUrl']='/ar/company-ar/'.$layoutData['langSwitchUrl'];
        


         $page->parseSliders();
        $common['slidersJs']=$page->SlidersJavascript;
        }
        $pageTemplates = ['webinar', 'webinar-ar' ,'goal', 'goal-ar', 'milestones', 'leadership', 'ceo-ar'];
        
        if($slug=="internship-program")
        {
                     return view('Front.Contact.internship',compact('page','layoutData','common'));

        }
        if(in_array($slug, $pageTemplates))
        {
                     return view('Front.Pages.'.$slug ,compact('page','layoutData','common'));

        }
         return view('Front.Pages.single-page',compact('page','layoutData','common'));


        // return view("{$this->viewPath}::modules.pages.default", [
            // 'page' => $page,
        // ]);
    }
}
