<?php

namespace App\Http\Controllers;

use App;
use App\Branch;
use App\Contact;
use Illuminate\Support\Facades\Input;
use Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;


class ContactController extends Controller
{
    //
    public function contactPage()
    {

        if (App::getLocale() == 'ar') {

            $layoutData['langSwitchUrl'] = '/en/contact/';
        } else {
            $layoutData['langSwitchUrl'] = '/ar/contact-ar';
        }

        $contacts = Contact::all();
        $branches = Branch::all();
        return view('Front.Contact.contactus', compact('contacts', 'branches', 'layoutData'));
    }

    public function quotationPage()
    {
        return view('Front.Contact.quotation');
    }

    public function nls(Request $request)
    {
        $mailData = $request->post('eid');
        $this->sendNLSMail($mailData);
        return "1";
    }
    public function contactPost(Request $request)
    {

        $validate = Validator::make(Input::all(), [
//            'g-recaptcha-response' => 'required|captcha',
        ]);
//         var_dump($validate->messages());
        if ($validate->fails()) {
            // $validate->messages()
            return json_encode(['errors' => ['Form Validation Failed']]);
        } else {

            $r = json_encode(['success' => "form submitted"]);
            $mailData = $request->post('Contact');//["Contact"]['full_name'];
//            print $mailData;
//            print_r($request->post());
//            print_r($mailData);
            $this->sendContactMail($mailData);
//            die;
            
            $contacts = Contact::all();
        $branches = Branch::all();
        Session::flash('message', 'Thank you very much for contact us. We will back to you soon');
        Session::flash('alert-class', 'alert-success'); 
//        return view('Front.Contact.contactus', compact('contacts', 'branches', 'layoutData'));
            return \Illuminate\Support\Facades\Redirect::to('/en/contact');
        }
        // return view('Front.Contact.contactus');
    }
    public function sendNLSMail($mailData) {
        
        $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
        $mailData = ["email"=> $mailData];
        print_r($mailData);
        $beautymail->send('emails.nls_mail', $mailData, function($message) //use ($mailData)
	{
		$message
			->from('info@reapai.com', 'MCB Bearings')
			->to('jawwad.cd@gmail.com', 'Jawwad Ahmed')
			->subject('MCB Newsletter subscription! ');
	});
    }
    public function sendContactMail($mailData) {
        
        $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
        $enquiry_type = isset($mailData['enquiry_type'])? $mailData['enquiry_type'] : "";
	$beautymail->send('emails.contact_mail', $mailData, function($message) use ($mailData, $enquiry_type)
	{
		$message
			->from('info@reapai.com', 'MCB Bearings')
			->to('jawwad.cd@gmail.com', 'Jawwad Ahmed')
//                        ->bcc($mailData['email'], $mailData['full_name'])

			->bcc('jawwad.software@gmail.com', 'Jawwad Ahmed')
			->subject('MCB Contact mail! '. $enquiry_type);
	});
	$beautymail->send('emails.auto_mail', $mailData, function($message) use ($mailData, $enquiry_type)
	{
		$message
			->from('donotreply@reapai.com', 'MCB Bearings')
			->to($mailData['email'], $mailData['full_name'])
			->subject('MCB contact request receipt!' );
	});
    }

    public function quotationPost(Request $request)
    {

//        $validate = Validator::make(Input::all(), [
//            'g-recaptcha-response' => 'required|captcha',
//        ]);
        // var_dump("sd");

         //dd(Input::all());
         $validate = Validator::make(Input::all(), [
//            'g-recaptcha-response' => 'required|captcha',
        ]);
        if ($validate->fails()) {
            // $validate->messages()
            return json_encode(['errors' => ['Form Validation Failed']]);
        } else {
            
             $mailData = $request->post('RFQContact');//["Contact"]['full_name'];
//            print $mailData;
//            print_r($request->post());
//            print_r($mailData);
            $this->sendContactMail($mailData);
            
            Session::flash('message', 'Thank you very much for contact us. We will back to you soon');
            Session::flash('alert-class', 'alert-success'); 
//        return view('Front.Contact.contactus', compact('contacts', 'branches', 'layoutData'));
            return \Illuminate\Support\Facades\Redirect::to('/en/contact');

            //return json_encode(['success' => "form submitted"]);
        }
        // return view('Front.Contact.contactus');
    }

}
