<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\View;
use App\ProductCategory;
use App\Page;
use App\Job;
use App;
use App\Brand;
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function __construct()
    {



    $pages=Page::active()->where('category','1')->get()->translate(App::getLocale());
    
    $careerPages=Page::active()->where('category','2')->get()->translate(App::getLocale());

  	$jobs = Job::count();
    // dd($jobs);
    $categories=ProductCategory::all()->sortBy("sort")->translate(App::getLocale());
    $brands=Brand::all()->sortBy("order")->translate(App::getLocale());

  	$sharedData['categories']=$categories;
    $sharedData['brands']=$brands;
    // dd($categories);
    $sharedData['pages']=$pages;
    $sharedData['careerPages']=$careerPages;

    $sharedData['jobs']=$jobs;
    // $sharedData['locale']=App::getLocale();
        View::share('sharedData', $sharedData);
    }
}
