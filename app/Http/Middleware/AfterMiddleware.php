<?php

namespace App\Http\Middleware;

use Closure;
use App;
use Cookie;
class AfterMiddleware
{
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        // dd(App::getLocale()."sd");
        // Perform action
        // $cookie = Cookie::queue(Cookie::forever('locale', App::getLocale()));
        if($request->segment(1)==null)
        {
        	// App::setLocale("en");
        	return redirect("/".App::getLocale()."/");
        }
        else
        {
        	return $response->cookie(Cookie::forever('locale', App::getLocale()));
        }
        
    }
}