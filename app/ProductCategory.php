<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Traits\Translatable;

class ProductCategory extends Model
{
    use Translatable;

    protected $translatable = ['name', 'slug', 'description'];

    
    public function product()
    {
        return $this->hasMany(Product::class);
    }


}
