<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;
use App\ProductPartNumber;
use TCG\Voyager\Traits\Resizable;
use TCG\Voyager\Traits\Translatable;
use App\CommonSlider;
class Product extends Model
{
    use Translatable;
    use Resizable;

    protected $translatable = ['title', 'description', 'application_title', 'application_description',
        'video_url', 'slug',
    ];

    public function brand()
    {
        return $this->belongsToMany(Brand::class,'product_brands')->withPivot('product_id');
    }
	
	public function productApplications()
    {
        return $this->belongsToMany(ProductApplication::class,'application_assignmets')->withPivot('product_id');
    }
	
	public function productAdvantages()
    {
        return $this->belongsToMany(ProductAdvantage::class,'advantage_assignments')->withPivot('product_id');
    }
	
	public function productAddons()
    {
        return $this->belongsToMany(ProductAddon::class,'addon_assignment')->withPivot('product_id');
    }

    public function partNumbers()
    {
        return $this->hasMany(ProductPartNumber::class);
    }

    // public function commonSliders()
    // {
    //     return $this->hasMany(CommonSlider::class);
    // }

    public function productCategory()
    {
        return $this->belongsTo(ProductCategory::class, 'product_category_id');
    }

    public function save(array $options = [])
    {

        parent::save();

        //delete old part numebrs from table.
        DB::table('product_part_numbers')->where('product_id', $this->id)->delete();

        // insert new part numbers

        $partNumbers = explode(",", $this->ppart_number);
        // dd($partNumbers);
        foreach ($partNumbers as $partNumber) {
            # code...

            ProductPartNumber::create([

                'number'     => $partNumber,
                'product_id' => $this->id,

            ]);
        }

    }

    public function parseSliders()
    {

        if(\App::getLocale()=="ar")
        {
            // $this->description=
            $this->description= $this->translate("ar")->description;
        }
        $data=$this->bb_parse($this->description);
        $this->description =$data['description'];
        $this->descriptionTranslated= $this->description;
        $this->sliders=$data['sliders'];
    }

    public function getDescriptionTranslatedAttribute()
    {
        return $this->descriptionTranslated;

    }
    public function getSlidersJavascriptAttribute()
    {

        $js="";
        foreach($this->sliders as $slider)
        {

            $slider=CommonSlider::find($slider);
            if(!empty($slider) )
            {
                if($slider->type!=0)
                {
               $js.=$slider->showSlider()."
               ";
                }
            }
        }

        return $js;

    }
    public function bb_parse($string) {
        $tags = 'slider';
        $sliders=[];
        // $pattern= '/\[(\/?shortcode_name.*?(?=\]))\]/';
        while (preg_match_all('`\[('.$tags.')=?(.*?)(.+?)\]`', $string, $matches)) foreach ($matches[0] as $key => $match) {
            list($tag, $param, $innertext) = array($matches[1][$key], $matches[2][$key], $matches[3][$key]);
            switch ($tag) {

                case 'slider':
                // dd($innertext);
                $param=trim($innertext);
                $sliders[]=$param;
                $slider=CommonSlider::find($param);
                if($slider->type==0)//images
                {
                    $replacement=$slider->showSlider();
                }
                else
                {
                    $replacement='<div id="slider_'.$param.'"></div>';
                }

                // $replacement = "<span style=\"font-size: $param;\">".$slider->showSlider()[0]."</span>";


                break;

            }
            $string = str_replace($match, $replacement, $string);
        }
        return array('description' => $string,
        'sliders' => $sliders);

    }



}
