<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use TCG\Voyager\Traits\Translatable;
class Widget extends Model
{
   use Translatable;
     protected $translatable = ['name', 'content'];
     
     
    public function parseWidget($string) {
        $tags = 'widget';
        $widgets=[];
        // $pattern= '/\[(\/?shortcode_name.*?(?=\]))\]/';
        while (preg_match_all('`\[('.$tags.')=?(.*?)(.+?)\]`', $string, $matches)) foreach ($matches[0] as $key => $match) {
            list($tag, $param, $innertext) = array($matches[1][$key], $matches[2][$key], $matches[3][$key]);
            switch ($tag) {

                case 'widget':
                // dd($innertext);
                $param=trim($innertext);
                $widgets[]=$param;
                $widget=Widget::find($param);
               
                    $replacement=' <div class="sidebar"><div style="background: '.$widget->bg.'; color:'.$widget->color.'" class="widget">
                        <h3 style="color:'.$widget->color.';" class="widget-title">'.$widget->name.'</h3>
                       '.$widget->content.'
                     </div> </div><!-- Widget End -->';
                

                // $replacement = "<span style=\"font-size: $param;\">".$widget->showwidget()[0]."</span>";


                break;

            }
            $string = str_replace($match, $replacement, $string);
        }
        return $string;
    }
}
