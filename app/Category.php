<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;
use TCG\Voyager\Traits\Resizable;
class Category extends Model
{
    //

    // use Translatable,Resizable;
    // protected $dates = ['created_at'];

    // protected $translatable = ['author_id', 'category_id','	title', 'seo_title','excerpt','body','image','slug','meta_description','meta_keywords','status','featured'];

    //     public function author()
    // {
    //     return $this->belongsTo(User::class, 'author_id');
    // }

 	public function post()
    {
        return $this->belongsToMany(Post::class, 'post_category_pivot')->withPivot('category_id');
    }


}
