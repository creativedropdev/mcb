<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Resizable;


class CatalogCategory extends Model
{

    use Resizable;
	 public function catalog()
    {
        return $this->hasMany(Catalog::class, 'category');
    }
    
}
