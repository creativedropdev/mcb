<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductPartNumber extends Model
{
    // use Translatable;
    protected $guarded      = ['id'];
    protected $translatable = ['number'];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }


}
