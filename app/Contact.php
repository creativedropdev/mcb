<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

use TCG\Voyager\Traits\Spatial;
use App\Widget;
class Contact extends Model
{
	      use Spatial;
	       use Translatable;
	      protected $spatial = ['location'];
	      protected $translatable = ['address', 'phone','email', 'content'];

    public function getWidgetsAttribute()
    {
    
        $widgets[]=$this->widget_1;
        $widgets[]=$this->widget_2;
        $widgets[]=$this->widget_3;

        // dd($widgets); 
        $widgets = Widget::find($widgets);
        return $widgets;
    }

    //  public function save(array $options = [])
    // {

    // 	// dd($this);
    // }
    
}
