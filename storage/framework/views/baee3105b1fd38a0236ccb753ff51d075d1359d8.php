<input type="number"
       class="form-control"
       name="<?php echo e($row->field); ?>"
       type="number"
       <?php if($row->required == 1): ?> required <?php endif; ?>
       step="any"
       placeholder="<?php echo e(isset($options->placeholder)? old($row->field, $options->placeholder): $row->display_name); ?>"
       value="<?php echo e($dataTypeContent->{$row->field} ?? old($row->field) ?? $options->default ?? ''); ?>">
