<?php

if(app()->getLocale()=="ar")
{

    $setting_site="site-arabic.";
    $setting_home="home-arabic.";

}
else
{
    $setting_site="site.";
     $setting_home="home.";
}

?>
<?php $__env->startSection('content'); ?>

<?php
$svgData=['width'=>'80px', 'height'=>'80px'];
?>

<div class="slider">
    
		<section class="home-slider section-bg-banner">
		    <div class="mcb-container">
		    <div class="row">
		        <div class="col-md-12 align-self-center">
		            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
		              <a href="#section-2" class="mouse" aria-hidden="true">
		                <span class="mouse__wheel"></span>
		                <span class="mouse__text">SCROLL TO EXPLORE</span>
		              </a>
		              <ol class="carousel-indicators">
		                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
		                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
		                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
		              </ol>
		              <div class="carousel-inner">
                                <?php $__currentLoopData = $slider; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=> $index): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

        <?php
        if(app()->getLocale()=="ar")
        {


            $index=$index->translate('ar');
        }
        ?>  
		                <div class="carousel-item <?php if($key ==0){ ?>active <?php }?>">
		                    <div class="row">
		                        <div class="col-5 col-sm-5">
		                            <div class="home-banner-text">
		                                <?php echo $index->text; ?>

		                            </div>
		                        </div>
		                        <div class="col-7 col-sm-7">
		                            <img src="<?php echo e(Voyager::image( $index->image )); ?>" class="banner-img-pos" alt="...">
		                            <!--<img src="assets/img/home-slider.png" class="banner-img-pos" alt="...">-->
		                        </div>
		                    </div>
		                </div>
        
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

		                

		              </div>
		              <div class="prc-slider-arrows">
		                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
		                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
		                <span class="sr-only">Previous</span>
		                </a>
		                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
		                <span class="carousel-control-next-icon" aria-hidden="true"></span>
		                <span class="sr-only">Next</span>
		                </a>
		              </div>
		            </div>
		        </div>
		    </div>
		    </div>

		</section>
	</div>
	<!-- slider -->
        <div class="request">
            <div class="mcb-container">
                    <div class="row">
                            <div class="col-12 col-sm-6 col-md-7 align-self-center res-margintop-20 res-marginbottom-20 res-center">
                                    <p class="m-0">You've read it right! We will be back at Automechanika Dubai this October 2020!</p>
                            </div>
                            <div class="col-12 col-sm-6 col-md-5 align-self-center res-marginbottom-20 res-center">
                                    <button class="btn mcb-btn mcb-btn-orange float-right res-no-float" data-toggle="modal" data-target="#mcbModal"><img src="assets/img/call-icon.png" width="20px" alt=""><span class="ml-2">REQUEST FOR A CALLBACK</span></button>
                            </div>
                    </div>
            </div>
        </div>
        <!-- request -->
    <section class="product-section section-bg-grey section-padtop-50">
    <div class="mcb-container">
            <div class="row">
                    <div class="col-sm-6 col-md-6 res-marginbottom-20">
                            <div class="head">
                                    <h3 class="mcb-h2 m-0">Products</h3>
                            </div>
                    </div>
                    <div class="col-sm-6 col-md-6">
                            <div class="product-link">
                                    <a href="/products">
                                            <button class="btn mcb-btn mcb-btn-blue float-right res-no-float"><span class="mr-3">VIEW ALL PRODUCTS</span><i class="fas fa-long-arrow-alt-right"></i></button>
                                    </a>
                            </div>
                    </div>
            </div>

            <div class="bearings">
                    <div class="row">
                            <div class="col-6 col-sm-4 col-md-6 col-lg-4 product-padding-right-10-320">

                                    <div class="parts">
                                            <div class="auto">
                                            <a href="/products/automotive-bearings"><h4 class="mcb-h4 m-0">Automotive<br>Bearings</h4></a>
                                                    <ul>
                                                            <li><a href="#" class="m-dark mcbModalProducts" data-toggle="modal" data-target="#mcbModal"><i class="far fa-comments fa-lg"></i></a></li>
                                                            <li><a target="_blank" href="https://api.whatsapp.com/send?phone=971-54-308-5993&text=Good%20day%20Mineral%20Circles%20Bearings%20Team%21%20Please%20contact%20me%20as%20soon%20as%20possible%20as%20I%20want%20to%20know%20about%20Automotive%20Bearings.&source=&data=%22"
                                                                   class="m-dark"><i class="fab fa-whatsapp fa-lg"></i></a></li>
                                                    </ul>
                                            </div>
                                            <div class="bearing-img bearing-img-fill mt-3">
                                                    <i class="fas fa-long-arrow-alt-right"></i>

                                                    <span>
                                                            <a href="/products/automotive-bearings"><?php echo $__env->make('Front.svg.automotive-bearings', $svgData, \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?></a>
                                                    </span>

                                            </div>
                                    </div>
                                    <!-- parts -->
                            </div>
                            <div class="col-6 col-sm-4 col-md-6 col-lg-4 product-padding-left-10-320">
                                    <div class="parts">
                                            <div class="auto">
                                                    <a href="/products/industrial-bearings"><h4 class="mcb-h4 m-0">Industrial<br>Bearings</h4></a>
                                                    <ul>
                                                            <li><a href="#" class="m-dark mcbModalProducts" data-toggle="modal" data-target="#mcbModal"><i class="far fa-comments fa-lg"></i></a></li>
                                                            <li><a target="_blank" href="https://api.whatsapp.com/send?phone=971-54-308-5993&text=Good%20day%20Mineral%20Circles%20Bearings%20Team%21%20Please%20contact%20me%20as%20soon%20as%20possible%20as%20I%20want%20to%20know%20about%Industrial%20Bearings.&source=&data=%22"
                                                                   class="m-dark"><i class="fab fa-whatsapp fa-lg"></i></a></li>
                                                    </ul>
                                            </div>
                                            <div class="bearing-img bearing-img-fill mt-3">
                                                    <i class="fas fa-long-arrow-alt-right"></i>
                                            <span>
                                                    <a href="/products/industrial-bearings"><?php echo $__env->make('Front.svg.industrial-bearings', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?></a>
                                            </span>
                                            </div>
                                    </div>
                                    <!-- parts -->
                            </div>
                            <div class="col-6 col-sm-4 col-md-6 col-lg-4 product-padding-right-10-320">
                                    <div class="parts">
                                            <div class="auto">
                                                    <a href="/products/universal-joints"><h4 class="mcb-h4 m-0">Universal<br>Joint</h4></a>
                                                    <ul>
                                                            <li><a href="#" class="m-dark mcbModalProducts" data-toggle="modal" data-target="#mcbModal"><i class="far fa-comments fa-lg"></i></a></li>
                                                            <li><a target="_blank" href="https://api.whatsapp.com/send?phone=971-54-308-5993&text=Good%20day%20Mineral%20Circles%20Bearings%20Team%21%20Please%20contact%20me%20as%20soon%20as%20possible%20as%20I%20want%20to%20know%20about%20Universal%20Joint.&source=&data=%22"
                                                                   class="m-dark"><i class="fab fa-whatsapp fa-lg"></i></a></li>
                                                    </ul>
                                            </div>
                                            <div class="bearing-img bearing-img-fill mt-3">
                                                    <i class="fas fa-long-arrow-alt-right"></i>

                                                    <span>
                                                            <a href="/products/universal-joints"><?php echo $__env->make('Front.svg.universal-joints', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?></a>
                                                    </span>


                                            </div>
                                    </div>
                                    <!-- parts -->
                            </div>
                            <div class="col-6 col-sm-4 col-md-6 col-lg-4 product-padding-left-10-320">
                                    <div class="parts">
                                            <div class="auto">
                                                    <a href="/products/oil-seal"><h4 class="mcb-h4 m-0">Oil<br>Seal</h4></a>
                                                    <ul>
                                                            <li><a href="#" class="m-dark mcbModalProducts" data-toggle="modal" data-target="#mcbModal"><i class="far fa-comments fa-lg"></i></a></li>
                                                            <li><a target="_blank" href="https://api.whatsapp.com/send?phone=971-54-308-5993&text=Good%20day%20Mineral%20Circles%20Bearings%20Team%21%20Please%20contact%20me%20as%20soon%20as%20possible%20as%20I%20want%20to%20know%20about%20Oil%20Seal.&source=&data=%22"
                                                                   class="m-dark"><i class="fab fa-whatsapp fa-lg"></i></a></li>
                                                    </ul>
                                            </div>
                                            <div class="bearing-img bearing-img-outline mt-3">
                                                    <i class="fas fa-long-arrow-alt-right"></i>
                                                    <span>
                                                            <a href="/products/oil-seal">
                                                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                     width="80px" height="80px" viewBox="0 0 80 80" enable-background="new 0 0 80 80" xml:space="preserve">
                                                            <g>

                                                                            <circle fill-rule="evenodd" clip-rule="evenodd" fill="none" stroke="#FAA224" stroke-miterlimit="10" cx="40.122" cy="39.869" r="38.675"/>

                                                                            <circle fill-rule="evenodd" clip-rule="evenodd" fill="none" stroke="#FAA224" stroke-miterlimit="10" cx="40.122" cy="39.869" r="36.152"/>

                                                                            <circle fill-rule="evenodd" clip-rule="evenodd" fill="none" stroke="#FAA224" stroke-miterlimit="10" cx="40.122" cy="39.869" r="24.583"/>

                                                                            <circle fill-rule="evenodd" clip-rule="evenodd" fill="none" stroke="#FAA224" stroke-miterlimit="10" cx="40.122" cy="39.869" r="22.298"/>

                                                                            <circle fill-rule="evenodd" clip-rule="evenodd" fill="none" stroke="#FAA224" stroke-miterlimit="10" cx="40.122" cy="39.869" r="20.11"/>
                                                            </g>
                                                            </svg>
                                                            </a>
                                                    </span>

                                            </div>
                                    </div>
                                    <!-- parts -->
                            </div>
                            <div class="col-6 col-sm-4 col-md-6 col-lg-4 product-padding-right-10-320">
                                    <div class="parts">
                                            <div class="auto">
                                                    <a href="/products/cv-joint"><h4 class="mcb-h4 m-0">CV<br>Joint</h4></a>
                                                    <ul>
                                                            <li><a href="#" class="m-dark mcbModalProducts" data-toggle="modal" data-target="#mcbModal"><i class="far fa-comments fa-lg"></i></a></li>
                                                            <li><a target="_blank" href="https://api.whatsapp.com/send?phone=971-54-308-5993&text=Good%20day%20Mineral%20Circles%20Bearings%20Team%21%20Please%20contact%20me%20as%20soon%20as%20possible%20as%20I%20want%20to%20know%20about%20CV%20Joint.&source=&data=%22"
                                                                   class="m-dark"><i class="fab fa-whatsapp fa-lg"></i></a></li>
                                                    </ul>
                                            </div>
                                            <div class="bearing-img bearing-img-outline mt-3">
                                                    <i class="fas fa-long-arrow-alt-right"></i>
                                                    <span>
                                                            <a href="/products/cv-joint"><?php echo $__env->make('Front.svg.cv-joint', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?></a>
                                                    </span>

                                            </div>
                                    </div>
                                    <!-- parts -->
                            </div>
                            <div class="col-6 col-sm-4 col-md-6 col-lg-4 product-padding-left-10-320">
                                    <div class="parts">
                                            <div class="auto">
                                                    <a href="/products/tools-for-service-and-maintenance"><h4 class="mcb-h4 m-0">Tools<br>for Service</h4></a>
                                                    <ul>
                                                            <li><a href="#" class="m-dark mcbModalProducts" data-toggle="modal" data-target="#mcbModal"><i class="far fa-comments fa-lg"></i></a></li>
                                                            <li><a target="_blank" href="https://api.whatsapp.com/send?phone=971-54-308-5993&text=Good%20day%20Mineral%20Circles%20Bearings%20Team%21%20Please%20contact%20me%20as%20soon%20as%20possible%20as%20I%20want%20to%20know%20about%20Tools%20for%20Service.&source=&data=%22"
                                                                   class="m-dark"><i class="fab fa-whatsapp fa-lg"></i></a></li>
                                                    </ul>
                                            </div>
                                            <div class="bearing-img bearing-img-outline mt-3">
                                                    <i class="fas fa-long-arrow-alt-right"></i>
                                                    <span>
                                                            <a href="/products/tools-for-service-and-maintenance">
                                                                    <?php echo $__env->make('Front.svg.tools-for-service-and-maintenance', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                                            </a>
                                                    </span>
                                            </div>
                                    </div>
                                    <!-- parts -->
                            </div>
                    </div>
            </div>
            <!-- bearings -->
    </div>
    <!-- mcb-contianer -->
    </section>
    <section class="quality-bearings res-inherit-height res-qb section-bg-grey res-margintop-10">
				<div class="mcb-container">
					<div class="quality mb-blue">
						<div class="row">
							<div class="col-md-6 quality-head">
								<h2 class="text-white mcb-h2 m-0">Quality Bearings<br>and Durable Packaging</h2>
							</div>
							<div class="col-md-6 res-no-padding">
								<div class="services res-margintop-30">
									<div class="service-one no-bg res-no-padding">
										<div class="service-1">
											&nbsp;
										</div>
										<p class="text-center text-white p-14 mt-2 mb-0">TECHNICAL<br>SERVICES</p>
									</div>
									<div class="service-one no-bg res-no-padding">
										<div class="service-2"> 
											&nbsp;
										</div>
										<p class="text-center text-white p-14 mt-2 mb-0">INDUSTRIAL<br>DIVISION & MRO</p>
									</div>
									<div class="service-one no-bg res-no-padding">
										<div class="service-3">
											&nbsp;
										</div>
										<p class="text-center text-white p-14 mt-2 mb-0">AUTOMOTIVE<br>DIVISION</p>
									</div>
								</div>
								<!-- services -->
							</div>
						</div>
					</div>
					<!-- quality -->
				</div>
				<!-- mcb-container -->
			</section>
			<!-- section-quality-bearings -->

			<section class="experience section-bg-grey section-padtop-50">
				<div class="mcb-container">
					<div class="row">
						<div class="col-sm-6 col-md-6 res-marginbottom-20">
							<div class="exp">
								<div class="exp-content">
									<h3 class="mcb-h3"><?php echo e(setting($setting_site.'home_about_us_heading')); ?></h3>
									<p class="m-0"><?php echo setting($setting_site.'home_about_us_description'); ?></p>
								</div>
								<div class="connect connect-marg">
									<!--<button class="btn mcb-btn mcb-btn-hover" data-toggle="modal" data-target="#mcbModal">CONNECT NOW</button>-->
									<img src="assets/img/bearing-1.png" alt="">
								</div>
								<!-- connect -->
							</div>
							<!-- exp -->
						</div>

						<div class="col-sm-6 col-md-6">
							<div class="exp">
								<div class="exp-content">
									<h3 class="mcb-h3"><?php echo e(setting($setting_home.'mcb_about_heading')); ?></h3>
									<?php echo setting($setting_home.'mcb_about_description'); ?>

								</div>
								<div class="connect">
									<!--<button class="btn mcb-btn mcb-btn-hover" data-toggle="modal" data-target="#mcbModal">CONNECT NOW</button>-->
									<img src="assets/img/bearing-2.png" alt="">
								</div>
								<!-- connect -->
							</div>
							<!-- exp -->
						</div>
					</div>
					</div>
					<!-- mcb-contianer -->
			</section>
			<!-- section-experience -->



			<section class="mcb-data section-padtop-50 section-padbottom-50 section-bg-grey">
				<div class="mcb-container">
					<div class="row">
						<div class="col-md-12">
							<ul class="mcb-ul mb-0">
								<li class="brand-border">
									<div class="inner-data d-flex align-items-start">
										<img src="assets/img/data1.svg" class="pr-3" alt="./">
										<div class="inner-content">
											<h1 class="m-orange"><span class="count">50</span><span class="m-blue">+</span></h1>
											<p class="m-orange mcb-bold mb-0 p-14">CLIENTS FROM<br>50+ COUNTRIES</p>
										</div>
									</div>
								</li>
								<li class="brand-border">
									<div class="inner-data d-flex align-items-start">
										<img src="assets/img/data2.svg" class="pr-3" alt="./">
										<div class="inner-content">
											<h1 class="m-orange"><span class="count">4</span><span class="m-blue">+</span></h1>
											<p class="m-orange mcb-bold mb-0 p-14">MILLION OF<br>BEARING IN STOCK</p>
										</div>
									</div>
								</li>
								<li class="brand-border m-auto">
									<div class="inner-data d-flex align-items-start">
										<img src="assets/img/data3.svg" class="pr-3" alt="./">
										<div class="inner-content">
											<h1 class="m-orange"><span class="count">11</span></h1>
											<p class="m-orange mcb-bold mb-0 p-14">BRANDS</p>
										</div>
									</div>
								</li>
								<li class="brand-border">
									<div class="inner-data d-flex align-items-start">
										<img src="assets/img/data4.svg" class="pr-3" alt="./">
										<div class="inner-content">
											<h1 class="m-orange"><span class="count">19</span><span class="m-blue">+</span></h1>
											<p class="m-orange mcb-bold mb-0 p-14">BEARING<br>50+ TYPES</p>
										</div>
									</div>
								</li>
								<li>
									<div class="inner-data d-flex align-items-start">
										<img src="assets/img/data5.svg" class="pr-3" alt="./">
										<div class="inner-content">
											<h1 class="m-orange"><span class="count">50,000</span><span class="m-blue">+</span></h1>
											<p class="m-orange mcb-bold mb-0 p-14">PART<br>NEMBERS</p>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</section>
			<!-- section-mcb-data -->

			

			<section class="clients section-bg-grey home-clients section-padbottom-50">
				<div class="mcb-container">
					<div class="row">
						<div class="col-md-12">
							<h2 class="mcb-h2"><strong>Here What Out Clients Say</strong></h2>
						</div>
					</div>
					<div class="row carousel-row">
						<div class="col-md-12">
							<div class="client-slider">
						<?php $__currentLoopData = $testimonials; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $testimonial): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                
                                                 <?php


                                                if(app()->getLocale()=="ar")
                                                {
                                                    $testimonial=$testimonial->translate('ar');

                                                }
                                                ?>
						<div class="col-md-4">
			                  		<div class="client-review">
			                  			<!--<h3 class="m-dark mcb-h3 m-0"></h3>-->
			                  			<p class="mt-4 mb-3 m-0"><?php echo $testimonial->description; ?></p>
			                  			<div class="client-profile">
			                  				<div class="client-img">
			                  					<img src="<?php echo e(Voyager::image( $testimonial->image )); ?>" alt="">
			                  				</div>
			                  				<div class="client-info">
			                  					<h5 class="m-dark mcb-h5"><?php echo e($testimonial->name); ?></h5>
			                  					<p><?php echo e($testimonial->designation); ?></p>
			                  				</div>
			                  			</div> <!-- client-profile -->
			                  		</div>
			                  		<!-- client-review -->
			                  	</div>
                                                
                                                   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

			                  	
							
							</div>
						</div>
					</div>
				</div>
			</section>

			<section class="brands section-bg-grey section-padbottom-50">
				<div class="mcb-container">
					<div class="row">
						<div class="col-md-12 align-self-center">
							<div class="brand-items">
                                                            <?php $__currentLoopData = $brands; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							  <div class="col"><div class="brand-logo">
                                                                  <a href="/<?php echo e(app()->getLocale()); ?>/brands/<?php echo e($index->slug); ?>">
                                                                      <img src="<?php echo e(Voyager::image( $index->image )); ?>" alt="">
                                                                  </a>
                                                          </div></div>
                                                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							  <div class="col"><div class="brand-logo"><img src="assets/img/brands-img/brand-2.png" alt=""></div></div>
							  <div class="col"><div class="brand-logo"><img src="assets/img/brands-img/brand-3.png" alt=""></div></div>
							  <div class="col"><div class="brand-logo"><img src="assets/img/brands-img/brand-4.png" alt=""></div></div>
							  <div class="col"><div class="brand-logo"><img src="assets/img/brands-img/brand-5.png" alt=""></div></div>
							  <div class="col"><div class="brand-logo"><img src="assets/img/brands-img/brand-6.png" alt=""></div></div>
							  <div class="col"><div class="brand-logo"><img src="assets/img/brands-img/brand-7.png" alt=""></div></div>
							  <div class="col"><div class="brand-logo"><img src="assets/img/brands-img/brand-8.png" alt=""></div></div>
							  <div class="col"><div class="brand-logo"><img src="assets/img/brands-img/brand-9.png" alt=""></div></div>
							  <div class="col"><div class="brand-logo"><img src="assets/img/brands-img/brand-10.png" alt=""></div></div>
							  <div class="col"><div class="brand-logo"><img src="assets/img/brands-img/brand-11.png" alt=""></div></div>
						</div>
					</div>
				</div>
			</section>
			<!-- brands -->
   	<section class="quote section-padtop-30 section-padbottom-30">
		<div class="mcb-container">
			<!-- <div class="row">
				<div class="offset-md-1 col-md-8">
					<div class="quote-content">
						<p class="text-white">You will get more than just a price by requesting a quote from us!</p>
					</div>
				</div>
				<div class="col-md-2">
					<div class="quote-link">
						<button class="btn mcb-btn mcb-btn-orange" data-toggle="modal" data-target="#mcbModal"><i class="fas fa-link"></i><span class="pl-2">CONNECT NOW</span></button>
					</div>
				</div>
			</div> -->
			<div class="row">
				<div class="col-md-12">
					<div class="quote-link text-center">
						<button class="btn mcb-btn mcb-btn-orange" data-toggle="modal" data-target="#mcbModal"><i class="fas fa-link"></i><span class="pl-2">CONNECT NOW</span></button>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="quote-content text-center">
						<p class="text-white">You will get more than just a price by requesting a quote from us!</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- quote -->
        
        	<?php echo $__env->make('Front.Contact.map', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	<!-- map-area -->
        
    <?php $__env->stopSection(); ?>

<?php echo $__env->make('layout.default', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>