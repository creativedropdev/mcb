<?php use Illuminate\Support\Facades\Storage;
//Storage::exists('products/October2020/CGJVOGucXSEIjaL9bhT3-cropped.jpg')
?>


<?php $__env->startSection('content'); ?>

<?php

if(app()->getLocale()=="ar")
{

    $setting_site="site-arabic.";


}
else
{
    $setting_site="site.";

}

?>

<?php
$svgData=['width'=>'86px', 'height'=>'86px'];

?>
<section class="mcb-slider products-banner">
	<h1 class="mcb-h1 m-0"><?php echo e($mainCatText); ?> </h1>
</section>


<style>
    .container
    {
    max-width:90%;
    }
    </style>

<?php echo $__env->make('Front.banner', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<section class="section-bg-grey section-padtop-50 section-padbottom-50">
	<div class="mcb-container">
		<div class="row">
			<div class="col-lg-4">
    
         	<?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
         	
            <?php if($category->product->count()>0): ?>
            
                        <ul class="nav nav-tabs product-page-tabs tabs-left">

                          <?php $__currentLoopData = $sharedData['categories']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ckey =>$categorycat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                           <?php
                           if($slug == $categorycat->slug) $_cat = $categorycat->name;
                           if(app()->getLocale()=="ar")
                           {
                                $categorycat=$categorycat->translate('ar');
                           }
                           ?>

                           <?php if($categorycat->slug!=""): ?>
                           <li class=" <?php if( ($slug == $categorycat->slug) )  { echo "active";  } ?>  products-item">
                               <a aria-selected="true" class="pn-ProductNav_Link" 
                                  href="/products/<?php echo e($categorycat->slug); ?>" >
                                   <span class="svg-ico-nav"><?php echo $__env->make('Front.svg.'.$categorycat->slug, $svgData, \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?></span>
                               <?php echo e($categorycat->name); ?></a>
                           </li>
                           <?php endif; ?>

                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </ul>
                     


                    <div class="rfq-section desktop-rfq">
					<h3 class="mcb-h3 text-white">Request For Quotation</h3>
					<div class="rfq-form">
                                            <form class="contactMe" action="/request-for-quotation" method="POST"
                            enctype="multipart/form-data">

						<div class="contact-inputs mt-3">
							<p class="font-weight-bold p-14 mb-1 text-white">Contact Person*</p>
                                                        <input name="RFQContact[full_name]" id="RFQContact_name" type="text" class="form-control" required="">
						</div>

						<div class="contact-inputs mt-3">
							<p class="font-weight-bold p-14 mb-1 text-white">Email*</p>
                                                        <input name="RFQContact[email]" id="RFQContact_email" type="text" class="form-control" required="">
						</div>
                                                
						<div class="contact-inputs mt-3">
							<p class="font-weight-bold p-14 mb-1 text-white">Phone</p>
							<input name="RFQContact[phone]" id="RFQContact_phone" type="text" class="form-control">
						</div>

						<div class="contact-inputs mt-3">
							<p class="font-weight-bold p-14 mb-1 text-white">Company Name</p>
							<input name="RFQContact[company]" id="RFQContact_company" type="text" class="form-control">
						</div>

						<div class="mt-3">
							<div class="contact-inputs enquiry message-box">
								<p class="font-weight-bold p-14 mb-1 text-white">Message *</p>
                                                                <textarea name="RFQContact[message_text]" class="form-control w-100" style="height: 100px;" required=""></textarea>
							</div>
							<!-- contact-inputs -->
						</div>


<!--						<div class="contact-inputs mt-3">
							<input id="file" type="file" name="filename" class="form-control h-100">
						</div>-->

<!--						<div class="contact-inputs rfq-captcha section-padtop-15">
							<div class="g-recaptcha" data-sitekey="6LcmQc0ZAAAAAEDSVqw-cF5NtfC3FDLx2n2RUdgE"></div>
						</div>-->
						<!-- contact-inputs -->
                                                
                                                <div class="form-group">
                                                    <?php if(isset($_cat)): ?>
                                                    <input type="hidden" name="RFQContact[enquiry_type]" id="RFQContact_enquiry_type" value="<?php echo e($_cat); ?>" class="form-control" placeholder="General Enquiry" required="">
                                                    <?php endif; ?>
                                                </div>
						<div class="question mt-3">
							<div>
								<button class="mcb-btn mcb-trans mcb-btn-orange minus-margin-for-rfq">SUBMIT
									REQUEST</button>
							</div>
							<!-- contact-inputs -->
						</div>
                                                <?php echo csrf_field(); ?>
						<!-- question -->
                                            </form>
					</div>
				</div>
				<!-- rfq-form -->

                  </div>
                  
			<!-- col -->

		<div class="col-lg-8 res-paddingtop-30">
                        <div class="row">
                            
                    <?php if($mainCatText == ""): ?>   
                    <div class="col-lg-12 col-md-12 ts-service-content">
                        <h2 class="section-title">
                           <span><?php echo e($category->translate(app()->getLocale())->name); ?></span>
                        </h2>
                    </div>
                    
                   <?php endif; ?> 

                 <?php $__currentLoopData = $category->product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                 <?php
                  $img=$product->thumbnail('cropped');

                    if(app()->getLocale()=="ar")
                    {
                        $product=$product->translate('ar');
                    }
                 ?>

                 <?php if($product->slug!=""): ?>
                 <div class="col-6 col-sm-4 col-md-3 product-padding-right-10-320 product-card">
                                        <a href="/products/<?php echo e($product->slug); ?>">
                            <div class="product-brand">
                                <!-- social-brand -->
                                <div class="product-img text-center section-padtop-30 section-padbottom-30">
                                    
                                       
                                        <?php if(!$img): ?> 
<!--                                        <img class="img-fluid" src="<?php echo e(Voyager::image( $img )); ?>" alt="">-->
                                        <?php echo e("NO IMAGE"); ?>

                                    <?php else: ?>
                                        <img class="img-fluid" src="<?php echo e(Voyager::image( $img )); ?>" alt="">
                                        <img src="https://mcb.workspace.destring.com/storage/<?php echo e(( $img )); ?>" alt="" class="w-100">
                                        <!--<img src="/assets/img/skf-product.png" alt="" class="w-50">-->
                                    <?php endif; ?>
                                </div>
                                <!-- product-img -->
                                <div class="truck product-wheel">
                                        <h6 class="mcb-h6"><?php echo e($product->title); ?></h6>
                                        <p class="p-14 m-0">View Product <span class="ml-2"><i
                                                                class="fas fa-long-arrow-alt-right"></i></span></p>
                                </div>
                        </div>
                        <!-- product-brand -->
                    </a>
                    <div class="social-brand">
                            <ul>
                                    <li><a href="#"  class="m-dark mcbModalProducts" data-toggle="modal" data-target="#mcbModal"><i class="far fa-comments"></i></a></li>
                                    <li><a class="m-dark" target="_blank" href=https://api.whatsapp.com/send?phone=<?php echo e(trim(setting('contact.whatsapp'),'+')); ?>&text=Good%20day%20Mineral%20Circles%20Bearings%20Team%21%20Please%20contact%20me%20as%20soon%20as%20possible%20as%20<?php echo e(rawurlencode('I want to know about '.$product->title.'.')); ?>&source=&data=" class="readmore contact"><i class="fab fa-whatsapp"></i></a></li>
                                    <!--<li><a href="#" class="m-dark"><i class="fab fa-whatsapp"></i></a></li>-->
                            </ul>
                    </div>
                </div>
                 
                    <?php endif; ?>
               <!-- Col end -->
               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

</div>
                    </div>
                

            </div>
            <!-- Row End -->
            <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
         </div>
         <!-- Service Container -->
      </div>
      <!-- Container end -->
   </section>





<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout.default', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>