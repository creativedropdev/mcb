<style>
    #map {
        height: 400px;
        width: 100%;
    }
</style>
<?php $__empty_1 = true; $__currentLoopData = $dataTypeContent->getCoordinates(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $point): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
    <input type="hidden" name="<?php echo e($row->field); ?>[lat]" value="<?php echo e($point['lat']); ?>" id="lat"/>
    <input type="hidden" name="<?php echo e($row->field); ?>[lng]" value="<?php echo e($point['lng']); ?>" id="lng"/>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
    <input type="hidden" name="<?php echo e($row->field); ?>[lat]" value="<?php echo e(config('voyager.googlemaps.center.lat')); ?>" id="lat"/>
    <input type="hidden" name="<?php echo e($row->field); ?>[lng]" value="<?php echo e(config('voyager.googlemaps.center.lng')); ?>" id="lng"/>
<?php endif; ?>

<script type="application/javascript">
    function initMap() {
        <?php $__empty_1 = true; $__currentLoopData = $dataTypeContent->getCoordinates(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $point): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
            var center = {lat: <?php echo e($point['lat']); ?>, lng: <?php echo e($point['lng']); ?>};
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
            var center = {lat: <?php echo e(config('voyager.googlemaps.center.lat')); ?>, lng: <?php echo e(config('voyager.googlemaps.center.lng')); ?>};
        <?php endif; ?>
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: <?php echo e(config('voyager.googlemaps.zoom')); ?>,
            center: center
        });
        var markers = [];
        <?php $__empty_1 = true; $__currentLoopData = $dataTypeContent->getCoordinates(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $point): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
            var marker = new google.maps.Marker({
                    position: {lat: <?php echo e($point['lat']); ?>, lng: <?php echo e($point['lng']); ?>},
                    map: map,
                    draggable: true
                });
            markers.push(marker);
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
            var marker = new google.maps.Marker({
                    position: center,
                    map: map,
                    draggable: true
                });
        <?php endif; ?>

        google.maps.event.addListener(marker,'dragend',function(event) {
            document.getElementById('lat').value = this.position.lat();
            document.getElementById('lng').value = this.position.lng();
        });
    }
</script>
<div id="map"></div>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=<?php echo e(config('voyager.googlemaps.key')); ?>&callback=initMap"></script>
