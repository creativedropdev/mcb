<?php $__env->startSection('content'); ?>

<section class="mcb-slider brand-banner">
       <h1 class="mcb-h1 m-0"><?php echo e($brand->title); ?></h1>
</section><section class="brands-skf section-bg-grey section-padtop-50 section-padbottom-50">
	<div class="mcb-container">
		<div class="row">
			
			<div class="col-lg-6 align-self-center">
				<div class="brand-image clients brand-slide">
					<!-- <img src="assets/img/brand-skf-1.png" alt=""> -->
                                   <div class="brand-img-slider">
                                          <img src="https://mcb.workspace.destring.com/storage/<?php echo e(( $brand->image )); ?>" alt="">
<!--                                          <img src="assets/img/skf.png" alt="">
                                          <img src="assets/img/skf.png" alt="">-->
                                   </div>
				</div>
			</div>

			<div class="col-lg-6 align-self-center">
                            <?php echo $brand->description; ?>

				
			</div>
		
		</div>
	</div>
</section>

<section class="clients brand-clients section-bg-grey section-padbottom-50">
	<div class="mcb-container">
		<div class="row">
		       <div class="col-md-12">
                            <h2 class="mcb-h2 section-padbottom-30 m-0"><strong>Brand Products</strong></h2>
                     </div>
              </div>
              <div class="row carousel-row">
              <div class="col-md-12">
	        <div class="product-slider">
                    <?php $__currentLoopData = $brand->product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                   <?php
                $img=$product->thumbnail('cropped');

                if(app()->getLocale()=="ar")
                {
                $product=$product->translate('ar');
                }
                ?>

                <?php if($product->slug!=""): ?>
	        	<div class="col product-padding-left-10-320 product-padding-right-10-320">
              		<a href="/products/<?php echo e($product->slug); ?>">
                            
                            
                                   <div class="product-brand">
                                   <!-- social-brand -->
                                   <div class="product-img text-center section-padtop-30 section-padbottom-30">
                                          <!--<img src="assets/img/skf-product.png" alt="" class="w-50">-->
                                          <img class="img-fluid brand-image" src="https://mcb.workspace.destring.com/storage/<?php echo e(( $product->image )); ?>" alt="" class="w-50">
                                   </div>
                                   <!-- product-img -->
                                   <div class="truck product-wheel">
                                          <h6 class="mcb-h6"><?php echo e($product->title); ?></h6>
                                          <p class="p-14 m-0">
                                              
                                              View Product <span class="ml-2"><i class="fas fa-long-arrow-alt-right"></i></span></p>
                                   </div>
                            </div>
                            <!-- product-brand -->
                            </a>
                            <div class="social-brand">
                                   <ul>
                                          <li><a href="#" class="m-dark mcbModalProducts" data-toggle="modal" data-target="#mcbModal"><i class="far fa-comments"></i></a></li>
                                          <li><a class="m-dark" target="_blank" href=https://api.whatsapp.com/send?phone=<?php echo e(trim(setting('contact.whatsapp'),'+')); ?>&text=Good%20day%20Mineral%20Circles%20Bearings%20Team%21%20Please%20contact%20me%20as%20soon%20as%20possible%20as%20<?php echo e(rawurlencode('I want to know about '.$product->title.'.')); ?>&source=&data=" class="readmore contact" ><i class="fab fa-whatsapp"></i></a></li>
                                   </ul>
                            </div>
                        </div>
                    <?php endif; ?>    
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	        </div>
	            
	        </div>
		</div>
	</div>
	<!-- mcb-container -->
</section>
<!-- clients -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.default', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>