<div class="request">
	<div class="mcb-container">
		<div class="row">
			<div class="col-12 col-sm-6 col-md-7 align-self-center res-margintop-20 res-marginbottom-20 res-center">
				<p class="m-0">You've read it right! We will be back at Automechanika Dubai this October 2020!</p>
			</div>
			<div class="col-12 col-sm-6 col-md-5 align-self-center res-marginbottom-20 res-center">
				<button class="btn mcb-btn mcb-btn-orange float-right res-no-float" data-toggle="modal" data-target="#mcbModal"><img src="assets/img/call-icon.png" width="20px" alt=""><span class="ml-2">REQUEST FOR A CALLBACK</span></button>
			</div>
		</div>
	</div>
</div>
<!-- request -->