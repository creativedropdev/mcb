	<section class="map-area">
		<div class="row no-gutters">
			<div class="col-sm-6 col-md-6">
				<div class="map-slider">
					<div id="demo" class="carousel slide">
                                            
					  <div class="carousel-inner">
                                              <?php $__currentLoopData = $branches; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bkey=> $branch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div class="carousel-item <?php if($bkey == 0){ ?> active <?php }?>">
					      <!--<img src="<?php echo e(Voyager::image( $branch->image )); ?>" alt="" height="400px" width="100%">-->
					      <img src="https://mcb.workspace.destring.com/storage/<?php echo e($branch->image); ?>" alt="<?php echo $branch->title; ?>" height="400px" width="100%">
					      <div class="carousel-caption">
					        <h3 class="text-white float-left p-2 mb-orange"><?php echo $branch->title; ?></h3>
					      </div>
					    </div>
                                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                              
					    
					  </div>
					  <a class="carousel-control-prev" href="#demo" data-slide="prev">
					    <span class="carousel-control-prev-icon"></span>
					  </a>
					  <a class="carousel-control-next" href="#demo" data-slide="next">
					    <span class="carousel-control-next-icon"></span>
					  </a>
					</div>
				</div>
				<!-- map-slider -->

				<div class="map-dropdown">
					<select class="form-control">
                                            <?php $__currentLoopData = $branches; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $branch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo str_slug($branch->title); ?>"><?php echo $branch->title; ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</select>
				</div>

			</div>
			<!-- col -->

			<div class="col-sm-6 col-md-6">
				<div  id="map">
				</div> <!-- map -->
			</div>
		</div>
		<!-- row -->
	</section>
	<!-- map-area -->
        
        