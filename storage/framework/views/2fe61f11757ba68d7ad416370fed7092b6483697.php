<?php $__env->startSection('content'); ?>

<?php
if(app()->getLocale()=="ar")
{

    $setting_site="site-arabic.";


}
else
{
    $setting_site="site.";

}
$img = "";

?>

<?php echo $schema; ?>

<section class="brands-skf section-bg-grey section-padtop-50 section-padbottom-50">
	<div class="mcb-container">
		<div class="row">
			
			<div class="col-lg-6 ">
				<div class="brand-image">
                                    <?php if(!$product->image): ?> 
<!--                                        <img class="img-fluid" src="<?php echo e(Voyager::image( $img )); ?>" alt="">-->
                                        <?php echo e("NO IMAGE"); ?>

                                    <?php else: ?>
                                        <img src="<?php echo e(Voyager::image( $product->image)); ?>" alt="" class="single-product-image img-fluid">
                                        <img src="https://mcb.workspace.destring.com/storage/<?php echo e(( $product->image )); ?>" alt="" class="w-100">
                                        <!--<img src="/assets/img/skf-product.png" alt="" class="w-50">-->
                                    <?php endif; ?>
                                        <?php if(app()->getLocale()=="en"): ?>


                      <!--<img src="<?php echo e(Voyager::image( $product->thumbnail('cropped'))); ?>" alt="<?php echo e($product->title); ?>" class="single-product-image img-fluid">-->
                        <?php else: ?>

                        <?php

                        ?>
                                              

   <?php endif; ?>
				</div>
			</div>

			<div class="col-lg-6 align-self-center">
				<div class="brand-content">
                                    <h2><?php echo e($product->title); ?></h2>
                                    <h3><?php echo e($product->productCategory->translate(app()->getLocale())->name); ?></h3>
					
					<p class="f-600">
						<?php echo $product->description; ?>

					</p>
					<p class="f-600">
						<h3 class="column-title no-border">
                        <span><?php echo e(setting($setting_site.'applications')); ?> </span>
                     </h3>
                     <?php echo e($product->application_title); ?>





                     <div class="service-list">
                        <div class="row">
                           <div class="col-lg-6 col-md-12">
                              <ul class="check-list unstyled">

                                <?php

                                  $applications=explode(PHP_EOL,$product->application_description);

                                  $totalApplications=count($applications);
                                  $newColumn=floor($totalApplications/2);
                                  $n=0;
                                ?>

                                <?php $__currentLoopData = $applications; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $application): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>



                                 <li>   <?php echo e($application); ?>

</li>

 <?php
                                  if($n==$newColumn)
                                  {
                                    echo '
                              </ul> <!-- Check list end -->
                           </div> <!-- Col end -->
                           <div class="col-lg-6 col-md-12">
                              <ul class="check-list unstyled">';
                                  }

                                  $n++;
                                ?>

<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</p>
				</div>

				
			</div>
		
		</div>
	</div>
</section>


<!-- Tabs -->
<section id="tab" class="section-bg-grey">
	<div class="mcb-container">
		<div class="row">
			<div class="col-md-12">
				<nav>
					<div class="nav nav-tabs nav-fill brand-tab-list" id="nav-tab" role="tablist">
						<a class="nav-item nav-link active brand-tab" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true"><h5 class="mcb-h5 mb-0">Corporate Video</h5></a>
						<a class="nav-item nav-link brand-tab" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false"><h5 class="mcb-h5 mb-0">Media Files</h5></a>
						<a class="nav-item nav-link brand-tab res-margintop-10" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false"><h5 class="mcb-h5 mb-0">RFQ</h5></a>
					</div>
				</nav>
				<div class="brand-tabs tab-content py-3 px-3 px-sm-0 section-padtop-30 product-padding-left-0-320 product-padding-right-0-320" id="nav-tabContent">
					<div class="tab-pane brand-tab-content fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
						<div class="row">
							<div class="col-md-6 col-lg-6 col-xl-4">
								<iframe width="560" height="315" src="https://www.youtube.com/embed/WWBpRAF6_5I" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
						Et et consectetur ipsum labore excepteur est proident excepteur ad velit occaecat qui minim occaecat veniam. Fugiat veniam incididunt anim aliqua enim pariatur veniam sunt est aute sit dolor anim. Velit non irure adipisicing aliqua ullamco irure incididunt irure non esse consectetur nostrud minim non minim occaecat. Amet duis do nisi duis veniam non est eiusmod tempor incididunt tempor dolor ipsum in qui sit. Exercitation mollit sit culpa nisi culpa non adipisicing reprehenderit do dolore. Duis reprehenderit occaecat anim ullamco ad duis occaecat ex.
					</div>
					<div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
						Et et consectetur ipsum labore excepteur est proident excepteur ad velit occaecat qui minim occaecat veniam. Fugiat veniam incididunt anim aliqua enim pariatur veniam sunt est aute sit dolor anim. Velit non irure adipisicing aliqua ullamco irure incididunt irure non esse consectetur nostrud minim non minim occaecat. Amet duis do nisi duis veniam non est eiusmod tempor incididunt tempor dolor ipsum in qui sit. Exercitation mollit sit culpa nisi culpa no
					</div>
				</div>
			
			</div>
		</div>
	</div>
</section>
<!-- ./Tabs -->
 <?php if(count($product->brand)>0): ?>

<section class="clients brand-clients section-bg-grey section-padbottom-50 section-padtop-50">
	<div class="mcb-container">
		<div class="row">
		       <div class="col-md-12">
                            <h2 class="mcb-h2 section-padbottom-30 m-0"><strong><?php echo e(setting($setting_site.'available_brands')); ?></strong></h2>
                     </div>
              </div>
              <div class="row carousel-row">
              <div class="col-md-12">
	        <div class="product-slider">
                    <?php $__currentLoopData = $product->brand; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $brand): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	        	<div class="col product-padding-left-10-320 product-padding-right-10-320">
              		<a href="/<?php echo e(app()->getLocale()); ?>/brands/<?php echo e($brand->slug); ?>">
                                   <div class="product-brand">
                                   <!-- social-brand -->
                                   <div class="product-img text-center section-padtop-30 section-padbottom-30">
                                          <?php if(!$brand->image): ?> 
<!--                                        <img class="img-fluid" src="<?php echo e(Voyager::image( $img )); ?>" alt="">-->
                                        <?php echo e("NO IMAGE"); ?>

                                    <?php else: ?>
                                        <img src="<?php echo e(Voyager::image( $brand->image)); ?>" alt="<?php echo e($brand->title); ?>" class="single-product-image img-fluid">
                                        <img src="https://mcb.workspace.destring.com/storage/<?php echo e(( $brand->image )); ?>" alt="" class="w-50">
                                        <!--<img src="/assets/img/skf-product.png" alt="" class="w-50">-->
                                    <?php endif; ?>
                                   </div>
                                   <!-- product-img -->
                                   <div class="truck product-wheel">
                                          <h6 class="mcb-h6"><?php echo e($brand->title); ?></h6>
                                          <p class="p-14 m-0">View  <span class="ml-2"><i class="fas fa-long-arrow-alt-right"></i></span></p>
                                   </div>
                            </div>
                            <!-- product-brand -->
                            </a>
                            <div class="social-brand">
                                   <ul>
                                          <li><a href="#" class="m-dark mcbModalProducts" data-toggle="modal" data-target="#mcbModal"><i class="far fa-comments"></i></a></li>
                                          <li>
                                              <a class="m-dark" target="_blank" href=https://api.whatsapp.com/send?phone=<?php echo e(trim(setting('contact.whatsapp'),'+')); ?>&text=Good%20day%20Mineral%20Circles%20Bearings%20Team%21%20Please%20contact%20me%20as%20soon%20as%20possible%20as%20<?php echo e(rawurlencode('I want to know about '.$brand->title.'.')); ?>&source=&data=" class="readmore contact" ><i class="fab fa-whatsapp"></i></a></li>
                                   </ul>
                            </div>
              	</div>
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     

	        </div>
	            
	        </div>
		</div>
	</div>
	<!-- mcb-container -->
</section>
  <?php endif; ?>
<!-- clients -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout.default', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>