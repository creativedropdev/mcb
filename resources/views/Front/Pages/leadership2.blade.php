@extends('layout.default')
@section('content')

<section class="mcb-slider the-goal-banner">
	<h1 class="mcb-h1">{!! $page->title !!}</h1>
</section>
@include('Front.banner')

<section class="work section-bg-grey section-padtop-50 section-padbottom-50">
	<div class="mcb-container">
		<div class="row">
			<div class="offset-md-2 col-md-8">
				<div class="work-content text-center">
					<h2 class="mcb-h2 m-0">
						Our Management Team
					</h2>
					<p class="p-14 mt-3">
						Our management team comprises of highly qualified experts with over 30 years of experience in the bearing industry. Together with the staff, they work with utmost dedication to living the company’s goal of being the best technical service distributor in the Middle East and Africa for bearings and related products.
					</p>

					<a href="javascript:void(0)" class="btn mcb-btn-blue mt-3 mcb-h5 text-white mb-0 pl-5 pr-5 mcb-bold our-leadership">Our Leadership</a>
				</div>
			</div>
		</div>
	</div>
	<!-- contianer -->
</section>
<!-- work -->

<section class="leaders-tabs section-bg-grey section-padbottom-50">
	<div class="mcb-container">
		<div class="row">
			<div class="col-md-12">
				<nav>
					
					<div class="nav nav-tabs nav-fill my-leader-tabs" id="nav-tab" role="tablist">
						
						<button class="btn nav-item nav-link leader-tab-link active" data-toggle="tab" data-target="#nav-leader-tab-1" role="tab" aria-controls="nav-leader-tab-1" aria-selected="true">
							<div class="col">
								<div class="founder-bg">
									<div class="founder-content">
										<h5 class="mcb-h5 mb-0 text-white">Amar Ridha 1</h5>
										<h6 class="mcb-h6 text-white">Founder/product</h6>
									</div>
								</div>
							</div>
						</button>

						<button class="btn nav-item nav-link leader-tab-link" data-toggle="tab" data-target="#nav-leader-tab-2" role="tab" aria-controls="nav-leader-tab-2" aria-selected="false">
							<div class="col">
								<div class="founder-bg">
									<div class="founder-content">
										<h5 class="mcb-h5 mb-0 text-white">Amar Ridha 2</h5>
										<h6 class="mcb-h6 text-white">Founder/product</h6>
									</div>
								</div>
							</div>
						</button>

						<button class="btn nav-item nav-link leader-tab-link" data-toggle="tab" data-target="#nav-leader-tab-3" role="tab" aria-controls="nav-leader-tab-3" aria-selected="false">
							<div class="col">
								<div class="founder-bg">
									<div class="founder-content">
										<h5 class="mcb-h5 mb-0 text-white">Amar Ridha 3</h5>
										<h6 class="mcb-h6 text-white">Founder/product</h6>
									</div>
								</div>
							</div>
						</button>

						<button class="btn nav-item nav-link leader-tab-link" data-toggle="tab" data-target="#nav-leader-tab-4" role="tab" aria-controls="nav-leader-tab-4" aria-selected="false">
							<div class="col">
								<div class="founder-bg">
									<div class="founder-content">
										<h5 class="mcb-h5 mb-0 text-white">Amar Ridha 4</h5>
										<h6 class="mcb-h6 text-white">Founder/product</h6>
									</div>
								</div>
							</div>
						</button>

						<button class="btn nav-item nav-link leader-tab-link" data-toggle="tab" data-target="#nav-leader-tab-5" role="tab" aria-controls="nav-leader-tab-5" aria-selected="false">
							<div class="col">
								<div class="founder-bg">
									<div class="founder-content">
										<h5 class="mcb-h5 mb-0 text-white">Amar Ridha 5</h5>
										<h6 class="mcb-h6 text-white">Founder/product</h6>
									</div>
								</div>
							</div>
						</button>

						<button class="btn nav-item nav-link leader-tab-link" data-toggle="tab" data-target="#nav-leader-tab-6" role="tab" aria-controls="nav-leader-tab-6" aria-selected="false">
							<div class="col">
								<div class="founder-bg">
									<div class="founder-content">
										<h5 class="mcb-h5 mb-0 text-white">Amar Ridha 6</h5>
										<h6 class="mcb-h6 text-white">Founder/product</h6>
									</div>
								</div>
							</div>
						</button>

						<button class="btn nav-item nav-link leader-tab-link" data-toggle="tab" data-target="#nav-leader-tab-7" role="tab" aria-controls="nav-leader-tab-7" aria-selected="false">
							<div class="col">
								<div class="founder-bg">
									<div class="founder-content">
										<h5 class="mcb-h5 mb-0 text-white">Amar Ridha 7</h5>
										<h6 class="mcb-h6 text-white">Founder/product</h6>
									</div>
								</div>
							</div>
						</button>

						<button class="btn nav-item nav-link leader-tab-link" data-toggle="tab" data-target="#nav-leader-tab-8" role="tab" aria-controls="nav-leader-tab-8" aria-selected="false">
							<div class="col">
								<div class="founder-bg">
									<div class="founder-content">
										<h5 class="mcb-h5 mb-0 text-white">Amar Ridha 8</h5>
										<h6 class="mcb-h6 text-white">Founder/product</h6>
									</div>
								</div>
							</div>
						</button>
						
					</div>


				</nav>
				<div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
					
					<div class="tab-pane fade show active" id="nav-leader-tab-1" role="tabpanel" aria-labelledby="nav-leader-tab-1">
						
						<div class="row mt-5 top-margin-0-320">
							
							<div class="col-sm-3 col-md-3 product-padding-left-0-320 product-padding-right-0-320">
								<div class="leader-img">
									<img src="/assets/img/leader-profile-1.png" alt="">
								</div>
							</div>

							<div class="col-sm-9 col-md-9 product-padding-left-0-320 product-padding-right-0-320">
								<div class="leader-content">
									<div class="leader-head d-flex justify-content-between">
										<h4 class="mcb-h4 m-dark mb-0">Amar Ridha Button 1</h4>
										<i class="fas fa-times m-dark fa-lg hide-tab"></i>
									</div>
									<h6 class="mcb-h6 m-dark">Founder/product</h6>
									<hr class="mb-dark">
									<p class="p-14 mt-4">
										Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
									</p>
									<p class="p-14">
										Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
									</p>

									<a href="#" class="m-blue leader-link mcb-h6 mcb-bold">READ MORE</a>
								</div>
							</div>
						</div>
					</div>

					<div class="tab-pane fade" id="nav-leader-tab-2" role="tabpanel" aria-labelledby="nav-leader-tab-2">
						
						<div class="row mt-5 top-margin-0-320">
							
							<div class="col-sm-3 col-md-3 product-padding-left-0-320 product-padding-right-0-320">
								<div class="leader-img">
									<img src="/assets/img/leader-profile-1.png" alt="">
								</div>
							</div>

							<div class="col-sm-9 col-md-9 product-padding-left-0-320 product-padding-right-0-320">
								<div class="leader-content">
									<div class="leader-head d-flex justify-content-between">
										<h4 class="mcb-h4 m-dark mb-0">Amar Ridha Button 2</h4>
										<i class="fas fa-times m-dark fa-lg hide-tab"></i>
									</div>
									<h6 class="mcb-h6 m-dark">Founder/product</h6>
									<hr class="mb-dark">
									<p class="p-14 mt-4">
										Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
									</p>

									<a href="#" class="m-blue leader-link mcb-h6 mcb-bold">READ MORE</a>
								</div>
							</div>
						</div>
					</div>

					<div class="tab-pane fade" id="nav-leader-tab-3" role="tabpanel" aria-labelledby="nav-leader-tab-3">
						
						<div class="row mt-5 top-margin-0-320">
							
							<div class="col-sm-3 col-md-3 product-padding-left-0-320 product-padding-right-0-320">
								<div class="leader-img">
									<img src="/assets/img/leader-profile-1.png" alt="">
								</div>
							</div>

							<div class="col-sm-9 col-md-9 product-padding-left-0-320 product-padding-right-0-320">
								<div class="leader-content">
									<div class="leader-head d-flex justify-content-between">
										<h4 class="mcb-h4 m-dark mb-0">Amar Ridha Button 3</h4>
										<i class="fas fa-times m-dark fa-lg hide-tab"></i>
									</div>
									<h6 class="mcb-h6 m-dark">Founder/product</h6>
									<hr class="mb-dark">
									<p class="p-14 mt-4">
										Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
									</p>
									<p class="p-14">
										Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
									</p>

									<a href="#" class="m-blue leader-link mcb-h6 mcb-bold">READ MORE</a>
								</div>
							</div>
						</div>
					</div>
					
					<div class="tab-pane fade" id="nav-leader-tab-4" role="tabpanel" aria-labelledby="nav-leader-tab-4">
						
						<div class="row mt-5 top-margin-0-320">
							
							<div class="col-sm-3 col-md-3 product-padding-left-0-320 product-padding-right-0-320">
								<div class="leader-img">
									<img src="/assets/img/leader-profile-1.png" alt="">
								</div>
							</div>

							<div class="col-sm-9 col-md-9 product-padding-left-0-320 product-padding-right-0-320">
								<div class="leader-content">
									<div class="leader-head d-flex justify-content-between">
										<h4 class="mcb-h4 m-dark mb-0">Amar Ridha Button 4</h4>
										<i class="fas fa-times m-dark fa-lg hide-tab"></i>
									</div>
									<h6 class="mcb-h6 m-dark">Founder/product</h6>
									<hr class="mb-dark">
									<p class="p-14 mt-4">
										Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
									</p>
									<p class="p-14">
										Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
									</p>

									<a href="#" class="m-blue leader-link mcb-h6 mcb-bold">READ MORE</a>
								</div>
							</div>
						</div>
					</div>

					<div class="tab-pane fade" id="nav-leader-tab-5" role="tabpanel" aria-labelledby="nav-leader-tab-5">
						
						<div class="row mt-5 top-margin-0-320">
							
							<div class="col-sm-3 col-md-3 product-padding-left-0-320 product-padding-right-0-320">
								<div class="leader-img">
									<img src="/assets/img/leader-profile-1.png" alt="">
								</div>
							</div>

							<div class="col-sm-9 col-md-9 product-padding-left-0-320 product-padding-right-0-320">
								<div class="leader-content">
									<div class="leader-head d-flex justify-content-between">
										<h4 class="mcb-h4 m-dark mb-0">Amar Ridha Button 5</h4>
										<i class="fas fa-times m-dark fa-lg hide-tab"></i>
									</div>
									<h6 class="mcb-h6 m-dark">Founder/product</h6>
									<hr class="mb-dark">
									<p class="p-14 mt-4">
										Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
									</p>
									<p class="p-14">
										Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
									</p>

									<a href="#" class="m-blue leader-link mcb-h6 mcb-bold">READ MORE</a>
								</div>
							</div>
						</div>
					</div>

					<div class="tab-pane fade" id="nav-leader-tab-6" role="tabpanel" aria-labelledby="nav-leader-tab-6">
						
						<div class="row mt-5 top-margin-0-320">
							
							<div class="col-sm-3 col-md-3 product-padding-left-0-320 product-padding-right-0-320">
								<div class="leader-img">
									<img src="/assets/img/leader-profile-1.png" alt="">
								</div>
							</div>

							<div class="col-sm-9 col-md-9 product-padding-left-0-320 product-padding-right-0-320">
								<div class="leader-content">
									<div class="leader-head d-flex justify-content-between">
										<h4 class="mcb-h4 m-dark mb-0">Amar Ridha Button 6</h4>
										<i class="fas fa-times m-dark fa-lg hide-tab"></i>
									</div>
									<h6 class="mcb-h6 m-dark">Founder/product</h6>
									<hr class="mb-dark">
									<p class="p-14 mt-4">
										Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
									</p>
									<p class="p-14">
										Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
									</p>

									<a href="#" class="m-blue leader-link mcb-h6 mcb-bold">READ MORE</a>
								</div>
							</div>
						</div>
					</div>

					<div class="tab-pane fade" id="nav-leader-tab-7" role="tabpanel" aria-labelledby="nav-leader-tab-7">
						
						<div class="row mt-5 top-margin-0-320">
							
							<div class="col-sm-3 col-md-3 product-padding-left-0-320 product-padding-right-0-320">
								<div class="leader-img">
									<img src="/assets/img/leader-profile-1.png" alt="">
								</div>
							</div>

							<div class="col-sm-9 col-md-9 product-padding-left-0-320 product-padding-right-0-320">
								<div class="leader-content">
									<div class="leader-head d-flex justify-content-between">
										<h4 class="mcb-h4 m-dark mb-0">Amar Ridha Button 7</h4>
										<i class="fas fa-times m-dark fa-lg hide-tab"></i>
									</div>
									<h6 class="mcb-h6 m-dark">Founder/product</h6>
									<hr class="mb-dark">
									<p class="p-14 mt-4">
										Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
									</p>
									<p class="p-14">
										Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
									</p>

									<a href="#" class="m-blue leader-link mcb-h6 mcb-bold">READ MORE</a>
								</div>
							</div>
						</div>
					</div>

					<div class="tab-pane fade" id="nav-leader-tab-8" role="tabpanel" aria-labelledby="nav-leader-tab-8">
						
						<div class="row mt-5 top-margin-0-320">
							
							<div class="col-sm-3 col-md-3 product-padding-left-0-320 product-padding-right-0-320">
								<div class="leader-img">
									<img src="/assets/img/leader-profile-1.png" alt="">
								</div>
							</div>

							<div class="col-sm-9 col-md-9 product-padding-left-0-320 product-padding-right-0-320">
								<div class="leader-content">
									<div class="leader-head d-flex justify-content-between">
										<h4 class="mcb-h4 m-dark mb-0">Amar Ridha Button 8</h4>
										<i class="fas fa-times m-dark fa-lg hide-tab"></i>
									</div>
									<h6 class="mcb-h6 m-dark">Founder/product</h6>
									<hr class="mb-dark">
									<p class="p-14 mt-4">
										Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
									</p>
									<p class="p-14">
										Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
									</p>

									<a href="#" class="m-blue leader-link mcb-h6 mcb-bold">READ MORE</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			
			</div>
		</div>
	</div>
</section>

<!-- leadership-tabs -->

@endsection