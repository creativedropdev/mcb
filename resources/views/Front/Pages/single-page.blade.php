@extends('layout.default')

@section('content')
<article>
    {!! $page->body !!}
</article>
@endsection