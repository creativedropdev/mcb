@extends('layout.default')
@php
use App\Post;
use App\Category;
$category=Category::where('slug', '=', 'webinar')->first();


             $ids=DB::table('post_category_pivot')->where('category_id', $category->id)->select('post_id')->get()->toArray();
             $ids1=[];
             foreach ($ids as $key => $value) {
                 
                $ids1[$key]=$value->post_id;
               
             }

            $webinars = Post::whereIn('id',$ids1)->where('status', '=', 'PUBLISHED')->orderBy('id', 'DESC')->withTranslations('ar')->paginate(10);

@endphp

@section('content')

{!! $page->body !!}

<section class="section-bg-grey section-padbottom-50">
<div class="mcb-container">
<div class="row">
<div class="col-md-12 text-center">
<h2 class="mcb-h2">CHOOSE YOUR TOPIC</h2>
</div>
</div>
<div class="row mt-3 justify-content-center">
    @foreach ($webinars as $key => $webinar) 
        <div class="col-sm-6 col-md-6 col-lg-6 col-xl-4 mt-3">
            <div class="presented text-center"><a href="/blog/{{ $webinar->slug }}"><span class="mcb-bold btn mcb-btn-blue mcb-h5 text-white mb-0 pt-1 pb-1">{{ $webinar->title }}</span></a></div>
        </div>
@endforeach
</div>
</div>
</section>

@endsection