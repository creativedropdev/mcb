@extends('layout.default')
@section('content')

<section class="mcb-slider the-goal-banner">
	<h1 class="mcb-h1">{!! $page->title !!}</h1>
</section>
@include('Front.banner')

    <section class="work section-bg-grey section-padtop-50 section-padbottom-50">
    	<div class="mcb-container">
    		<div class="row justify-content-center">
    			<div class="col-md-8">
    				<div class="work-content text-center">
    					<h2 class="mcb-h2 m-0">
    						Our Management Team
    					</h2>
    					<p class="p-14 mt-3">
    						Our management team comprises of highly qualified experts with over 30 years of experience in the bearing industry. Together with the staff, they work with utmost dedication to living the companyâ€™s goal of being the best technical service distributor in the Middle East and Africa for bearings and related products.
    					</p>
    
    					<!-- <a href="#" class="btn mcb-btn-blue mt-3"><h5 class="mcb-h5 text-white mb-0 pl-5 pr-5">Our Leadership</h5></a> -->
    					<a href="javascript:void(0)" class="btn mcb-btn-blue mt-3 mcb-h5 text-white mb-0 pl-5 pr-5 mcb-bold our-leadership">Our Leadership</a>
    				</div>
    			</div>
    		</div>
    	</div>
    	<!-- contianer -->
    </section>
    <!-- work -->

	<section class="section-bg-grey section-padbottom-50">
		<div class="mcb-container">
			<div class="leadership-wrapper">
				<div class="row">
					
					<div class="col-sm-4 col-md-3">
						<div class="leader-img">
							<img src="/assets/img/leaders-and-team-images/l-amar-ridha.jpg" alt="">
						</div>
					</div>

					<div class="col-sm-8 col-md-7">
						<div class="leader-content">
							<h3 class="mcb-h3 mb-0">AMAR RIDHA</h3>
							<h6 class="mcb-h6 m-dark">Founder/ Product and Technical Director</h6>
							<p class="p-14 mt-4">
								Believing in the future of bearings, he envisioned of having a specialized bearing company in the MEA region and thus founded Mineral Circles Bearings (MCB) in 1984. Today, with over 30 years’ experience in the industry, he is considered as one of Middle East and Africa’s experts on automotive bearings.
							</p>
						</div>
					</div>
				
				</div>
			</div>

			<div class="leadership-wrapper">
				<div class="row">
					
					<div class="col-sm-4 col-md-3">
						<div class="leader-img">
							<img src="/assets/img/leaders-and-team-images/l-hassanein-alwan.jpg" alt="">
						</div>
					</div>

					<div class="col-sm-8 col-md-7">
						<div class="leader-content">
							<h3 class="mcb-h3 mb-0">HASSANEIN ALWAN</h3>
							<h6 class="mcb-h6 m-dark">Managing Director</h6>
							<p class="p-14 mt-4">
								Prior to acquiring more than 10 years’ experience in the bearing industry’s sales, marketing, and business development sectors, he initially obtained his machinery expertise from Sweden’s premier university, the KTH Royal Institute of Technology, with a degree of Master of Science in Engineering. Thereafter, he became one of SAAB’s aeronautical engineers before joining MCB in the last decade.
							</p>
						</div>
					</div>
				
				</div>
			</div>

			<div class="leadership-wrapper">
				<div class="row">
					
					<div class="col-sm-4 col-md-3">
						<div class="leader-img">
							<img src="/assets/img/leaders-and-team-images/l-safa-alwan.jpg" alt="">
						</div>
					</div>

					<div class="col-sm-8 col-md-7">
						<div class="leader-content">
							<h3 class="mcb-h3 mb-0">SAFA ALWAN</h3>
							<h6 class="mcb-h6 m-dark">Finance and Investment Director</h6>
							<p class="p-14 mt-4">
								Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
							</p>
						</div>
					</div>
				
				</div>
			</div>
			
		</div>
	</section>
	<!-- leader -->


@endsection