@extends('layout.default')

@section('content')

<section class="mcb-slider milestones-banner">
	<h1 class="mcb-h1 m-0">{!! $page->title !!}</h1>
</section>
@include('Front.banner')
<section class="work section-bg-grey section-padtop-50 section-padbottom-50">
	<div class="mcb-container">
		<div class="row">
			<div class="offset-md-1 col-md-10">
				<div class="work-content text-center">
					{!! $page->excerpt !!}
					
				</div>
			</div>
		</div>
	</div>
	<!-- contianer -->
</section>
<!-- work -->
{!! $page->body !!}
@endsection