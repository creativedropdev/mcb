@extends('layout.default')
@php

@endphp
@section('content')

<section class="mcb-slider the-goal-banner">
	<h1 class="mcb-h1">{!! $page->title !!}</h1>
</section>
@include('Front.banner')
<section class="section-bg-grey ">
			<div class="mcb-container">
				<div class="section-padbottom-50 section-padtop-50">
					<div class="row">
                                            <div class="col-sm-6 col-md-6 align-self-center">
							<div class="trusted-img">
								<img src="/assets/img/goal-pic.png" alt="./">
							</div>
                                            </div>
                                            <div class="col-sm-6 col-md-6 align-self-center" style="direction: rtl; text-align: right">
							<div class="trusted-content">
								{!! $page->excerpt !!}
							</div> <!-- trusted-content -->
                                            </div>
						
					</div>
				</div>
				<!-- trusted -->
			</div>
		</section>
		<!-- trusted -->

			<section class="section-padbottom-50 section-bg-grey goal-page-tab">
				<div class="mcb-container">
					<section id="tabs" class="goal-tab">
							<div class="row">
								<div class="col-md-12">
									<nav>
                                                                            <div class="nav nav-tabs nav-fill goal-tabs" id="nav-tab" style="direction: rtl" role="tablist">
											<a class="nav-item nav-link active goal-tab-link" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true"><h5 class="mcb-h5 mb-0 text-white">{{ setting('our-goal.easy-towork-with-ar') }}</h5></a>
											
											<a class="nav-item nav-link goal-tab-link center-tab" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false"><h5 class="mcb-h5 mb-0 text-white">{{ setting('our-goal.trust-ar') }}</h5></a>
											
											<a class="nav-item nav-link goal-tab-link res-last-tab" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false"><h5 class="mcb-h5 mb-0 text-white">{{ setting('our-goal.reliable-ar') }}</h5></a>
										</div>
									</nav>
									<div class="tab-content px-sm-0" id="nav-tabContent">
										<div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                                                                        <div class="row goal-tab-row">
												
												<div class="col-sm-6 col-md-6 align-self-center">
													<div class="goal-content">{!! setting('our-goal.easy-towork-with-desc-ar') !!}</div>
												</div>
                                                                                            <div class="col-sm-6 col-md-6">
													<div class="goal-img">
														<img src="/assets/img/goal-tab-1.png" alt="">
													</div>
												</div>
											</div>
											
										</div>
										<div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
											<div class="row goal-tab-row">{!! setting('our-goal.trust-desc-ar') !!}</div>
										</div>
										<div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
											<div class="row goal-tab-row">
												<div class="col-sm-6 col-md-6">
													<div class="goal-img">
														<img src="/assets/img/goal-tab-1.png" alt="">
													</div>
												</div>
												<div class="col-sm-6 col-md-6 align-self-center">
													<div class="goal-content">{!! setting('our-goal.reliable-ar-ar') !!}</div>
												</div>
											</div>
										</div>
									</div>
								
								</div>
							</div>
					</section>
				</div>
			</section>
		<!-- ./Tabs -->

		<section class="timeline section-bg-grey section-padbottom-50 for-goal">
			<div class="mcb-container">
				<div class="row no-gutters">
					

					
			            <div class="col-lg-3 col-md-3 col-xs-3 bhoechie-tab-menu">
			              <div class="list-group">
			                <a href="#" class="list-group-item active text-center miles-item">
			                  <h4 class="glyphicon glyphicon-plane mcb-h5 m-0">Easy to Work With</h4>
			                </a>
			                <a href="#" class="list-group-item text-center miles-item">
			                  <h4 class="glyphicon glyphicon-plane mcb-h5 m-0">Trustworthy</h4>
			                </a>
			                <a href="#" class="list-group-item text-center miles-item">
			                  <h4 class="glyphicon glyphicon-plane mcb-h5 m-0">Reliable</h4>
			                </a>
			              </div>
			            </div>
			            <div class="col-lg-9 col-md-9 col-xs-9 bhoechie-tab">
			                <!-- flight section -->
			                <div class="bhoechie-tab-content active">
			                    <img src="/assets/img/goal-tab-1.png" alt="">
			                    <div class="timeline-content">
			                    	<h4 class="mcb-h4 text-white">Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum has been.</h4>
									<p class="p-14 text-white mb-0">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
			                    </div>
			                </div>
			                <!-- train section -->
			                <div class="bhoechie-tab-content">
			                    <img src="/assets/img/goal-tab-1.png" alt="">
			                    <div class="timeline-content">
			                    	<p class="p-14 text-white">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</p>
			                    </div>
			                </div>
			    
			                <!-- hotel search -->
			                <div class="bhoechie-tab-content">
			                    <img src="/assets/img/goal-tab-1.png" alt="">
			                    <div class="timeline-content">
			                    	<p class="p-14 text-white">"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure."</p>
			                    </div>
			                </div>
			            </div>
			
				

				</div>
			</div>
		</section>


		<section class="section-bg-grey section-padbottom-50">
			<div class="mcb-container">
				<div class="row">
					<div class="col-md-12">
						<div class="behind-button text-center">
							<a href="#" class="btn mcb-btn-blue"><h5 class="mcb-h5 m-0 pl-5 pr-5 text-white">Behind The Scenes</h5></a>
						</div>
					</div>
				</div>
			</div>
		</section>

		<!-- <section class="section-bg-grey section-padbottom-50">
			<div class="mcb-container">
				<div class="row">
					<div class="col-md-4 pr-1">
						<div class="trusted-img culture-img">
							<img src="/assets/img/goal-img-1.png" alt="./">
						</div>
					</div>
					<div class="col-md-4 pr-1 pl-1">
						<div class="trusted-video">
							<iframe width="560" height="250	" src="https://www.youtube.com/embed/nDwaxOE30BA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
					</div>
					<div class="col-md-4 pl-1">
						<div class="trusted-img culture-img">
							<img src="/assets/img/goal-img-2.png" alt="./">
						</div>
					</div>
				</div>
			</div>
		</section> -->


		<!-- Tabs -->
<section id="tab" class="section-bg-grey section-padbottom-50">
        <div class="mcb-container">
            <div class="row">
                
                <ul class="nav nav-fill ">
                <li class="active col-md-6"><a class="nav-item nav-link active brand-tab the-goal-tab-2 mcb-h5 mb-0" href="#menu1" data-toggle="tab"><strong>IMAGES</strong></a></li>
                <li class="col-md-6"><a class="nav-item nav-link brand-tab the-goal-tab-2 mcb-h5 mb-0" href="#menu2" data-toggle="tab"><strong>VIDEOS</strong></a></li>
                </ul>
            
            </div>
            <div class="tab-content">
                <div id="menu1" class="tab-pane active in ">

                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-4 pr-1">
                    <div class="trusted-img culture-img"><img src="/assets/img/goal-img-1.png" alt="./" /></div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-4 pr-1 pl-1">
                    <div class="trusted-img culture-img"><img src="/assets/img/goal-img-1.png" alt="./" /></div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-4 pl-1">
                    <div class="trusted-img culture-img res-paddingtop-30"><img src="/assets/img/goal-img-2.png" alt="./" /></div>
                    </div>
                </div>
                </div>
                <div id="menu2" class="tab-pane fade">

                <div class="row">
					<div class="col-sm-6">
						<div class="res-padleft-10 res-padright-10 the-goal-tab-video trusted-video">
							<iframe src="https://www.youtube.com/embed/0h4_sJbnRsI" allowfullscreen="" width="560"
								height="315" frameborder="0"></iframe>
						</div>
					</div>
				</div>
                </div>
            </div>
            
        </div>
</section>
<!-- ./Tabs --> 
@endsection