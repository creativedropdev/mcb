@extends('layout.default')

@section('content')
 @if(app()->getLocale()=="en")
  <div class="banner-area bg-overlay" id="banner-area" style="background-image:url({{ Voyager::image( $page->thumbnail('banner')) }});">
   @else
<div class="banner-area bg-overlay" id="banner-area" style="background-image:url({{ Voyager::image( $page->image) }});">
   @endif
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <div class="banner-heading">
                  <h1 class="banner-title">{{$page->title}} <span></span></h1>
                  <ol class="breadcrumb">
                     <li>Home</li>

                     <li><a href="#"> {{$page->title}}</a></li>

                  </ol>
                  <!-- Breadcumb End -->
               </div>
               <!-- Banner Heading end -->
            </div>
            <!-- Col end-->
         </div>
         <!-- Row end-->
      </div>
      <!-- Container end-->
   </div>
   <!-- Banner area end-->



      <style>
       @if($page->container_width!="1140px")

        @media only screen and (min-width: 1200px) {
        .container{
            max-width:{{$page->container_width}};
        }
        }

      @endif
        {{$page->custom_css}}

      </style>

   <section id="main-container" class="main-container pb-120">
      <div id="ts-service-details" class="ts-service-detials">
         <div class="container">
            <div class="row">



               <div class="col-lg-12 col-md-12">
                  <div class="ts-service-content">
                     <h2 class="section-title">
                        <span>{{$page->title}}</span>
                     </h2>
                     <div class="row">

                     @if(!$page->has_sidebar)
                        <div class="col-lg-12">

                           {!! $page->body !!}

                        </div>

                     @else
                     <div class="col-lg-9">

                  {!! $page->body !!}

                  </div>
                        <div class="col-lg-3 col-md-12">

               <div class="sidebar sidebar-left">
                  <div class="widget no-padding no-border">
                        <ul class="service-menu unstyled">
                            @if(Request::segment(2)=="company")
                          @foreach($sharedData['pages'] as $catPage)


                           <li @if(Request::segment(3)==$catPage->slug) class="active" @endif  ><a @if(Request::segment(3)==$catPage->slug) class="active" @endif href="/en/company/{{ $catPage->slug }}">{{ $catPage->title }}</a></li>

                           @endforeach
                           @endif
                        </ul>
                     </div>
                    @php
                   // dd($page);
                    @endphp
                    @foreach($page->widgets as $widget)
                      @if($widget->name!="No Widget")

                     <div style="background: {{$widget->bg}}; color:{{$widget->color}};" class="widget">
                        <h3 style="color:{{$widget->color}};" class="widget-title">{{$widget->name}}</h3>
                       {!! $widget->content !!}
                     </div> <!-- Widget End -->
                        @endif
                     @endforeach






               </div><!-- Sidebar end -->
            </div>
@endif
                     </div>










                  </div> <!-- Service content end -->


               </div> <!-- Col end -->
            </div><!-- Main row end -->

         </div><!-- Container end -->
      </div><!-- Service details end -->
      </section><!-- Main container end -->


@stop
