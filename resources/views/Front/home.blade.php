@extends('layout.default')


@php

if(app()->getLocale()=="ar")
{

    $setting_site="site-arabic.";
    $setting_home="home-arabic.";
    $setting_testimonials =  setting('home-arabic.testimonials');
    $request_callback_desc =  setting('home-arabic.request_callback_desc_ar');
    $quality_bearing =  setting('home-arabic.quality_bearing_ar');
    $all_products = setting('menu-arabic.products');

}
else
{
    $setting_site="site.";
    $setting_home="home.";
    $setting_testimonials = setting('home.testimonials') ;
    $request_callback_desc = setting('home.request_callback_desc') ;
    $quality_bearing = setting('home.quality_bearing') ;
    $all_products =  "View All Products";
}

@endphp
@section('content')

@php
$svgData=['width'=>'80px', 'height'=>'80px'];
@endphp

<div class="slider">
    
		<section class="home-slider section-bg-banner">
		    <div class="mcb-container">
		    <div class="row">
		        <div class="col-md-12 align-self-center">
		            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
		              <a href="#section-2" class="mouse" aria-hidden="true">
		                <span class="mouse__wheel"></span>
		                <span class="mouse__text">SCROLL TO EXPLORE</span>
		              </a>
		              <ol class="carousel-indicators">
		                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
		                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
		                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
		              </ol>
		              <div class="carousel-inner">
                                @foreach($slider as $key=> $index)

        @php
        if(app()->getLocale()=="ar")
        {


            $index=$index->translate('ar');
        }
        @endphp  
		                <div class="carousel-item <?php if($key ==0){ ?>active <?php }?>">
		                    <div class="row">
		                        <div class="col-5 col-sm-5">
		                            <div class="home-banner-text">
		                                {!! $index->text !!}
		                            </div>
		                        </div>
		                        <div class="col-7 col-sm-7">
		                            <img src="{{ Voyager::image( $index->image ) }}" class="banner-img-pos" alt="...">
		                            <!--<img src="assets/img/home-slider.png" class="banner-img-pos" alt="...">-->
		                        </div>
		                    </div>
		                </div>
        
@endforeach

		                

		              </div>
		              <div class="prc-slider-arrows">
		                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
		                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
		                <span class="sr-only">Previous</span>
		                </a>
		                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
		                <span class="carousel-control-next-icon" aria-hidden="true"></span>
		                <span class="sr-only">Next</span>
		                </a>
		              </div>
		            </div>
		        </div>
		    </div>
		    </div>

		</section>
	</div>
	<!-- slider -->
        <div class="request">
            <div class="mcb-container">
                @if(app()->getLocale()=="en")
                    <div class="row">
                            <div class="col-12 col-sm-6 col-md-7 align-self-center res-margintop-20 res-marginbottom-20 res-center">
                                    <p class="m-0">{{ $request_callback_desc }}</p>
                            </div>
                            <div class="col-12 col-sm-6 col-md-5 align-self-center res-marginbottom-20 res-center">
                                    <button class="btn mcb-btn mcb-btn-orange float-right res-no-float" data-toggle="modal" data-target="#mcbModal"><img src="assets/img/call-icon.png" width="20px" alt=""><span class="ml-2">REQUEST FOR A CALLBACK</span></button>
                            </div>
                    </div>
                @else
                    
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-7 align-self-center res-margintop-20 res-marginbottom-20 res-center">
                        <p class="m-0 text-right">{{ $request_callback_desc }}</p>
                        </div>
                        <div class="col-12 col-sm-6 col-md-5 align-self-center res-marginbottom-20 res-center"><button class="btn mcb-btn mcb-btn-orange res-no-float" data-toggle="modal" data-target="#mcbModal"><span class="ml-2">طلب معاودة الاتصال </span> <img src="/assets/img/call-icon.png" alt="" width="20px" /></button></div>
                    </div> <!-- row -->
                @endif    
                    
            </div>
        </div>
        <!-- request -->
    <section id="section-2" class="product-section section-bg-grey section-padtop-50">
    <div class="mcb-container" >
            <div class="row">
                    <div class="col-sm-6 col-md-6 res-marginbottom-20">
                            <div class="head">
                                    <h3 class="mcb-h2 m-0">Products</h3>
                            </div>
                    </div>
                    <div class="col-sm-6 col-md-6">
                            <div class="product-link">
                                    <a href="/products">
                                            <button class="btn mcb-btn mcb-btn-blue float-right res-no-float"><span class="mr-3">{{ $all_products}}</span><i class="fas fa-long-arrow-alt-right"></i></button>
                                    </a>
                            </div>
                    </div>
            </div>

            <div class="bearings">
                    <div class="row justify-content-center">
                            <div class="col-6 col-sm-6 col-md-6 col-lg-4 product-padding-right-10-320">

                                    <div class="parts">
                                            <div class="auto">
                                            <a href="/products/{{ $sharedData['categories'][4]->slug }}"><h4 class="mcb-h4 m-0">@php print($sharedData['categories'][4]->name); @endphp </h4></a>
                                                    <ul>
                                                            <li><a href="#" class="m-dark mcbModalProducts" data-toggle="modal" data-target="#mcbModal"><i class="far fa-comments fa-lg"></i></a></li>
                                                            <li><a target="_blank" href="https://api.whatsapp.com/send?phone={{ trim(setting('contact.whatsapp'),'+') }}&text=Good%20day%20Mineral%20Circles%20Bearings%20Team%21%20Please%20contact%20me%20as%20soon%20as%20possible%20as%20I%20want%20to%20know%20about%20Automotive%20Bearings.&source=&data=%22"
                                                                   class="m-dark"><i class="fab fa-whatsapp fa-lg"></i></a></li>
                                                    </ul>
                                            </div>
                                            <div class="bearing-img bearing-img-fill mt-3">
                                                    <i class="fas fa-long-arrow-alt-right"></i>

                                                    <span>
                                                            <a href="/products/{{ $sharedData['categories'][4]->slug }}">@include('Front.svg.automotive-bearings', $svgData)</a>
                                                    </span>

                                            </div>
                                    </div>
                                    <!-- parts -->
                            </div>
                            <div class="col-6 col-sm-6 col-md-6 col-lg-4 product-padding-left-10-320">
                                    <div class="parts">
                                            <div class="auto">
                                                    <a href="/products/{{ $sharedData['categories'][3]->slug }}"><h4 class="mcb-h4 m-0">{{ $sharedData['categories'][3]->name }}</h4></a>
                                                    <ul>
                                                            <li><a href="#" class="m-dark mcbModalProducts" data-toggle="modal" data-target="#mcbModal"><i class="far fa-comments fa-lg"></i></a></li>
                                                            <li><a target="_blank" href="https://api.whatsapp.com/send?phone={{ trim(setting('contact.whatsapp'),'+') }}&text=Good%20day%20Mineral%20Circles%20Bearings%20Team%21%20Please%20contact%20me%20as%20soon%20as%20possible%20as%20I%20want%20to%20know%20about%Industrial%20Bearings.&source=&data=%22"
                                                                   class="m-dark"><i class="fab fa-whatsapp fa-lg"></i></a></li>
                                                    </ul>
                                            </div>
                                            <div class="bearing-img bearing-img-fill mt-3">
                                                    <i class="fas fa-long-arrow-alt-right"></i>
                                            <span>
                                                    <a href="/products/{{ $sharedData['categories'][3]->slug }}">@include('Front.svg.industrial-bearings')</a>
                                            </span>
                                            </div>
                                    </div>
                                    <!-- parts -->
                            </div>
                            <div class="col-6 col-sm-6 col-md-6 col-lg-4 product-padding-right-10-320">
                                    <div class="parts">
                                            <div class="auto">
                                                    <a href="/products/{{ $sharedData['categories'][2]->slug }}"><h4 class="mcb-h4 m-0">{{ $sharedData['categories'][2]->name }}</h4></a>
                                                    <ul>
                                                            <li><a href="#" class="m-dark mcbModalProducts" data-toggle="modal" data-target="#mcbModal"><i class="far fa-comments fa-lg"></i></a></li>
                                                            <li><a target="_blank" href="https://api.whatsapp.com/send?phone={{ trim(setting('contact.whatsapp'),'+') }}&text=Good%20day%20Mineral%20Circles%20Bearings%20Team%21%20Please%20contact%20me%20as%20soon%20as%20possible%20as%20I%20want%20to%20know%20about%20Universal%20Joint.&source=&data=%22"
                                                                   class="m-dark"><i class="fab fa-whatsapp fa-lg"></i></a></li>
                                                    </ul>
                                            </div>
                                            <div class="bearing-img bearing-img-fill mt-3">
                                                    <i class="fas fa-long-arrow-alt-right"></i>

                                                    <span>
                                                            <a href="/products/{{ $sharedData['categories'][2]->slug }}">@include('Front.svg.universal-joints')</a>
                                                    </span>


                                            </div>
                                    </div>
                                    <!-- parts -->
                            </div>
                            <div class="col-6 col-sm-6 col-md-6 col-lg-4 product-padding-left-10-320">
                                    <div class="parts">
                                            <div class="auto">
                                                    <a href="/products/{{ $sharedData['categories'][1]->slug }}"><h4 class="mcb-h4 m-0">{{ $sharedData['categories'][1]->name }}</h4></a>
                                                    <ul>
                                                            <li><a href="#" class="m-dark mcbModalProducts" data-toggle="modal" data-target="#mcbModal"><i class="far fa-comments fa-lg"></i></a></li>
                                                            <li><a target="_blank" href="https://api.whatsapp.com/send?phone={{ trim(setting('contact.whatsapp'),'+') }}&text=Good%20day%20Mineral%20Circles%20Bearings%20Team%21%20Please%20contact%20me%20as%20soon%20as%20possible%20as%20I%20want%20to%20know%20about%20Oil%20Seal.&source=&data=%22"
                                                                   class="m-dark"><i class="fab fa-whatsapp fa-lg"></i></a></li>
                                                    </ul>
                                            </div>
                                            <div class="bearing-img bearing-img-outline mt-3">
                                                    <i class="fas fa-long-arrow-alt-right"></i>
                                                    <span>
                                                            <a href="/products/{{ $sharedData['categories'][1]->slug }}">
                                                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                     width="80px" height="80px" viewBox="0 0 80 80" enable-background="new 0 0 80 80" xml:space="preserve">
                                                            <g>

                                                                            <circle fill-rule="evenodd" clip-rule="evenodd" fill="none" stroke="#FAA224" stroke-miterlimit="10" cx="40.122" cy="39.869" r="38.675"/>

                                                                            <circle fill-rule="evenodd" clip-rule="evenodd" fill="none" stroke="#FAA224" stroke-miterlimit="10" cx="40.122" cy="39.869" r="36.152"/>

                                                                            <circle fill-rule="evenodd" clip-rule="evenodd" fill="none" stroke="#FAA224" stroke-miterlimit="10" cx="40.122" cy="39.869" r="24.583"/>

                                                                            <circle fill-rule="evenodd" clip-rule="evenodd" fill="none" stroke="#FAA224" stroke-miterlimit="10" cx="40.122" cy="39.869" r="22.298"/>

                                                                            <circle fill-rule="evenodd" clip-rule="evenodd" fill="none" stroke="#FAA224" stroke-miterlimit="10" cx="40.122" cy="39.869" r="20.11"/>
                                                            </g>
                                                            </svg>
                                                            </a>
                                                    </span>

                                            </div>
                                    </div>
                                    <!-- parts -->
                            </div>
                            <div class="col-6 col-sm-6 col-md-6 col-lg-4 product-padding-right-10-320">
                                    <div class="parts">
                                            <div class="auto">
                                                    <a href="/products/{{ $sharedData['categories'][0]->slug }}"><h4 class="mcb-h4 m-0">{{ $sharedData['categories'][0]->name }}</h4></a>
                                                    <ul>
                                                            <li><a href="#" class="m-dark mcbModalProducts" data-toggle="modal" data-target="#mcbModal"><i class="far fa-comments fa-lg"></i></a></li>
                                                            <li><a target="_blank" href="https://api.whatsapp.com/send?phone={{ trim(setting('contact.whatsapp'),'+') }}&text=Good%20day%20Mineral%20Circles%20Bearings%20Team%21%20Please%20contact%20me%20as%20soon%20as%20possible%20as%20I%20want%20to%20know%20about%20CV%20Joint.&source=&data=%22"
                                                                   class="m-dark"><i class="fab fa-whatsapp fa-lg"></i></a></li>
                                                    </ul>
                                            </div>
                                            <div class="bearing-img bearing-img-outline mt-3">
                                                    <i class="fas fa-long-arrow-alt-right"></i>
                                                    <span>
                                                            <a href="/products/{{ $sharedData['categories'][0]->slug }}">@include('Front.svg.cv-joint')</a>
                                                    </span>

                                            </div>
                                    </div>
                                    <!-- parts -->
                            </div>
                            <div class="col-6 col-sm-6 col-md-6 col-lg-4 product-padding-left-10-320">
                                    <div class="parts">
                                            <div class="auto">
                                                    <a href="/products/{{ $sharedData['categories'][5]->slug }}"><h4 class="mcb-h4 m-0">
                                                            @if(app()->getLocale()=="ar")
                                                            {{ $sharedData['categories'][5]->name }}
                                                            @else
                                                            {{ substr($sharedData['categories'][5]->name, 0,18) }}
                                                            @endif
                                                        </h4></a>
                                                    <ul>
                                                            <li><a href="#" class="m-dark mcbModalProducts" data-toggle="modal" data-target="#mcbModal"><i class="far fa-comments fa-lg"></i></a></li>
                                                            <li><a target="_blank" href="https://api.whatsapp.com/send?phone={{ trim(setting('contact.whatsapp'),'+') }}&text=Good%20day%20Mineral%20Circles%20Bearings%20Team%21%20Please%20contact%20me%20as%20soon%20as%20possible%20as%20I%20want%20to%20know%20about%20Tools%20for%20Service.&source=&data=%22"
                                                                   class="m-dark"><i class="fab fa-whatsapp fa-lg"></i></a></li>
                                                    </ul>
                                            </div>
                                            <div class="bearing-img bearing-img-outline mt-3">
                                                    <i class="fas fa-long-arrow-alt-right"></i>
                                                    <span>
                                                            <a href="/products/{{ $sharedData['categories'][5]->slug }}">
                                                                    @include('Front.svg.tools-for-service-and-maintenance')
                                                            </a>
                                                    </span>
                                            </div>
                                    </div>
                                    <!-- parts -->
                            </div>
                            <div class="col-6 col-sm-6 col-md-6 col-lg-4 product-padding-right-10-320">
                                    <div class="parts">
                                            <div class="auto">
                                                    <a href="/products/{{ $sharedData['categories'][0]->slug }}"><h4 class="mcb-h4 m-0">Belts</h4></a>
                                                    <ul>
                                                            <li><a href="#" class="m-dark mcbModalProducts" data-toggle="modal" data-target="#mcbModal"><i class="far fa-comments fa-lg"></i></a></li>
                                                            <li><a target="_blank" href="https://api.whatsapp.com/send?phone={{ trim(setting('contact.whatsapp'),'+') }}&text=Good%20day%20Mineral%20Circles%20Bearings%20Team%21%20Please%20contact%20me%20as%20soon%20as%20possible%20as%20I%20want%20to%20know%20about%20CV%20Joint.&source=&data=%22"
                                                                   class="m-dark"><i class="fab fa-whatsapp fa-lg"></i></a></li>
                                                    </ul>
                                            </div>
                                            <div class="bearing-img bearing-img-outline mt-3">
                                                    <i class="fas fa-long-arrow-alt-right"></i>
                                                    <span>
                                                            <a href="/products/{{ $sharedData['categories'][0]->slug }}">@include('Front.svg.belts')</a>
                                                    </span>

                                            </div>
                                    </div>
                                    <!-- parts -->
                            </div>
                            <div class="col-6 col-sm-6 col-md-6 col-lg-4 product-padding-left-10-320">
                                    <div class="parts">
                                            <div class="auto">
                                                    <a href="/products/{{ $sharedData['categories'][5]->slug }}"><h4 class="mcb-h4 m-0">
                                                            @if(app()->getLocale()=="ar")
                                                            {{ $sharedData['categories'][5]->name }}
                                                            @else
                                                            {{ substr("Industrial Chains and Components", 0,18) }}
                                                            @endif
                                                        </h4></a>
                                                    <ul>
                                                            <li><a href="#" class="m-dark mcbModalProducts" data-toggle="modal" data-target="#mcbModal"><i class="far fa-comments fa-lg"></i></a></li>
                                                            <li><a target="_blank" href="https://api.whatsapp.com/send?phone={{ trim(setting('contact.whatsapp'),'+') }}&text=Good%20day%20Mineral%20Circles%20Bearings%20Team%21%20Please%20contact%20me%20as%20soon%20as%20possible%20as%20I%20want%20to%20know%20about%20Tools%20for%20Service.&source=&data=%22"
                                                                   class="m-dark"><i class="fab fa-whatsapp fa-lg"></i></a></li>
                                                    </ul>
                                            </div>
                                            <div class="bearing-img bearing-img-outline mt-3">
                                                    <i class="fas fa-long-arrow-alt-right"></i>
                                                    <span>
                                                            <a href="/products/{{ $sharedData['categories'][5]->slug }}">
                                                                @include('Front.svg.industrial-chains-and-components')
                                                            </a>
                                                    </span>
                                            </div>
                                    </div>
                                    <!-- parts -->
                            </div>
                    </div>
            </div>
            <!-- bearings -->
    </div>
    <!-- mcb-contianer -->
    </section>
    <section class="quality-bearings res-inherit-height res-qb section-bg-grey res-margintop-10">
				<div class="mcb-container">
					<div class="quality mb-blue">
						<div class="row">
							{!! $quality_bearing !!}
						</div>
					</div>
					<!-- quality -->
				</div>
				<!-- mcb-container -->
			</section>
			<!-- section-quality-bearings -->

			<section class="experience section-bg-grey section-padtop-50">
				<div class="mcb-container">
					<div class="row">
						<div class="col-sm-6 col-md-6 res-marginbottom-20">
							{!!setting($setting_site.'home_about_us_description')!!}
						</div>

						<div class="col-sm-6 col-md-6">
							{!!setting($setting_home.'mcb_about_description')!!}
						</div>
					</div>
					</div>
					<!-- mcb-contianer -->
			</section>
			<!-- section-experience -->



			<section class="mcb-data section-padtop-50 section-padbottom-50 section-bg-grey">
				<div class="mcb-container">
					<div class="row">
						<div class="col-md-12">
							<ul class="mcb-ul mb-0">
								<li class="brand-border">
									<div class="inner-data d-flex align-items-start">
										<img src="assets/img/data1.svg" class="pr-3" alt="./">
										<div class="inner-content">
											<h1 class="m-orange"><span class="count">50</span><span class="m-blue">+</span></h1>
											<p class="m-orange mcb-bold mb-0 p-14">CLIENTS FROM<br>50+ COUNTRIES</p>
										</div>
									</div>
								</li>
								<li class="brand-border">
									<div class="inner-data d-flex align-items-start">
										<img src="assets/img/data2.svg" class="pr-3" alt="./">
										<div class="inner-content">
											<h1 class="m-orange"><span class="count">4</span><span class="m-blue">+</span></h1>
											<p class="m-orange mcb-bold mb-0 p-14">MILLION OF<br>BEARING IN STOCK</p>
										</div>
									</div>
								</li>
								<li class="brand-border m-auto">
									<div class="inner-data d-flex align-items-start">
										<img src="assets/img/data3.svg" class="pr-3" alt="./">
										<div class="inner-content">
											<h1 class="m-orange"><span class="count">11</span></h1>
											<p class="m-orange mcb-bold mb-0 p-14">BRANDS</p>
										</div>
									</div>
								</li>
								<li class="brand-border">
									<div class="inner-data d-flex align-items-start">
										<img src="assets/img/data4.svg" class="pr-3" alt="./">
										<div class="inner-content">
											<h1 class="m-orange"><span class="count">19</span><span class="m-blue">+</span></h1>
											<p class="m-orange mcb-bold mb-0 p-14">BEARING<br>50+ TYPES</p>
										</div>
									</div>
								</li>
								<li>
									<div class="inner-data d-flex align-items-start">
										<img src="assets/img/data5.svg" class="pr-3" alt="./">
										<div class="inner-content">
											<h1 class="m-orange"><span class="count">50,000</span><span class="m-blue">+</span></h1>
											<p class="m-orange mcb-bold mb-0 p-14">PART<br>NEMBERS</p>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</section>
			<!-- section-mcb-data -->

			

			<section class="clients section-bg-grey home-clients section-padbottom-50">
				<div class="mcb-container">
					<div class="row">
						<div class="col-md-12">
							<h2 class="mcb-h2"><strong>{{ $setting_testimonials }}</strong></h2>
						</div>
					</div>
					<div class="row carousel-row">
						<div class="col-md-12">
							<div class="client-slider">
						@foreach($testimonials as $testimonial)
                                                
                                                 @php


                                                if(app()->getLocale()=="ar")
                                                {
                                                    $testimonial=$testimonial->translate('ar');

                                                }
                                                @endphp
				                	<div class="client-review">
				                	    <div class="client-testimonial-details">
			                  			    {!!$testimonial->description!!}
			                  			</div>
			                  			<div class="client-profile">
			                  				<div class="client-img">
			                  					<img src="{{ Voyager::image( $testimonial->image ) }}" alt="" width="56" height="58">
                                                                            
			                  				</div>
			                  				<div class="client-info">
			                  					<h5 class="m-dark mcb-h5">{{$testimonial->name}}</h5>
			                  					<p>{{$testimonial->designation}}</p>
			                  				</div>
			                  			</div> <!-- client-profile -->
			                  		</div>
			                  		<!-- client-review -->
                                                
                                                   @endforeach

			                  	
							
							</div>
						</div>
					</div>
				</div>
			</section>

			<section class="brands section-bg-grey section-padbottom-50">
				<div class="mcb-container">
					<div class="row">
						<div class="col-md-12 align-self-center">
							<div class="brand-items">
                                                            @foreach($brands as $index)
							  <div class="col"><div class="brand-logo">
                                                                  <a href="/{{ app()->getLocale()}}/brands/{{$index->slug}}">
                                                                      <img src="/storage/{{ ( $index->image ) }}" alt="">
                                                                  </a>
                                                          </div></div>
                                                          @endforeach
						</div>
					</div>
				</div>
			</section>
			<!-- brands -->
   	<section class="quote section-padtop-30 section-padbottom-30">
		<div class="mcb-container">
			<!-- <div class="row">
				<div class="offset-md-1 col-md-8">
					<div class="quote-content">
						<p class="text-white">You will get more than just a price by requesting a quote from us!</p>
					</div>
				</div>
				<div class="col-md-2">
					<div class="quote-link">
						<button class="btn mcb-btn mcb-btn-orange" data-toggle="modal" data-target="#mcbModal"><i class="fas fa-link"></i><span class="pl-2">CONNECT NOW</span></button>
					</div>
				</div>
			</div> -->
			<div class="row">
				<div class="col-md-12">
					<div class="quote-link text-center">
						<button class="btn mcb-btn mcb-btn-orange" data-toggle="modal" data-target="#mcbModal"><i class="fas fa-link"></i><span class="pl-2">CONNECT NOW</span></button>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="quote-content text-center">
						<p class="text-white">You will get more than just a price by requesting a quote from us!</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- quote -->
        
        	@include('Front.Contact.map')
	<!-- map-area -->
        
    @stop
