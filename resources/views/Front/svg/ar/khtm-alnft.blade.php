<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												 width="{{ $svgData['width'] }}" height="{{ $svgData['width'] }}" viewBox="0 0 80 80" enable-background="new 0 0 80 80" xml:space="preserve">
											<g>

													<circle fill-rule="evenodd" clip-rule="evenodd" fill="none" stroke="#FAA224" stroke-miterlimit="10" cx="40.122" cy="39.869" r="38.675"/>

													<circle fill-rule="evenodd" clip-rule="evenodd" fill="none" stroke="#FAA224" stroke-miterlimit="10" cx="40.122" cy="39.869" r="36.152"/>

													<circle fill-rule="evenodd" clip-rule="evenodd" fill="none" stroke="#FAA224" stroke-miterlimit="10" cx="40.122" cy="39.869" r="24.583"/>

													<circle fill-rule="evenodd" clip-rule="evenodd" fill="none" stroke="#FAA224" stroke-miterlimit="10" cx="40.122" cy="39.869" r="22.298"/>

													<circle fill-rule="evenodd" clip-rule="evenodd" fill="none" stroke="#FAA224" stroke-miterlimit="10" cx="40.122" cy="39.869" r="20.11"/>
											</g>
											</svg>