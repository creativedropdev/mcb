@extends('layout.default')

@section('content')


@php

if(app()->getLocale()=="ar")
{

    $setting_contact="contact-arabic.";
    $ask_a_question = setting($setting_contact.'ask_a_question');
    $contact_details = setting($setting_contact.'contact_details');


}
else
{
     $setting_contact="contact.";
     $ask_a_question = setting($setting_contact.'ask_a_question');
     $contact_details = setting($setting_contact.'contact_details');
}

@endphp


<section class="mcb-slider contact-banner">
		<h1 class="mcb-h1 m-0">{{ $ask_a_question }}</h1>
</section>

@include('Front.banner')
@include('Front.Contact.map')
 

   <section class="contact-us section-bg-grey section-padtop-50 section-padbottom-50">
		<div class="mcb-container">
			<div class="row">
				<div class="col-lg-6 pr-5">
					<div class="ask">
                                            <form class="contactMe" action="/contact" method="POST" enctype="multipart/form-data">
						<h2 class="mcb-h2 m-0">{{ $ask_a_question }}</h2>
						<div class="row section-padtop-30">
							<div class="col-lg-6">
								<div class="contact-inputs">
									<p class="font-weight-bold p-14 mb-1">Full Name *</p>
									<input type="text" name="Contact[full_name]" id="full_name" class="form-control" required="">
								</div>
								<!-- contact-inputs -->
							</div>

							<div class="col-lg-6 intern-margintop-481 mac-top-30">
								<div class="contact-inputs">
								<p class="font-weight-bold p-14 mb-1 res-paddingtop-30">Phone Number</p>
									<!-- <input type="text" class="form-control" placeholder="+971"> -->
									<input name="Contact[phone]" type="tel" class="form-control input-numbers">
								</div>
								<!-- contact-inputs -->
							</div>	
						</div>
						<!-- row -->

						<div class="row section-padtop-30">
							<div class="col-lg-6">
								<div class="contact-inputs">
									<p class="font-weight-bold p-14 mb-1" >Email *</p>
                                                                        <input type="text" id="email" name="Contact[email]" class="form-control" required="">
								</div>
								<!-- contact-inputs -->
							</div>

							<div class="col-lg-6 intern-margintop-481 mac-top-30">
								<div class="contact-inputs">
									<p class="font-weight-bold p-14 mb-1 res-paddingtop-30">Organization</p>
                                                                        <input type="text" id="organiztion" name="Contact[organization]" class="form-control">
								</div>
								<!-- contact-inputs -->
							</div>	
						</div>
						<!-- row -->

						<div class="row section-padtop-30">
							<div class="col-lg-12">
								<div class="contact-inputs">
									<p class="font-weight-bold p-14 mb-1">Types of Enquiry *</p>
                                                                        <input type="text" name="Contact[enquiry_type]" id="enquiry_type" class="form-control" placeholder="General Enquiry" required="">
								</div>
								<!-- contact-inputs -->
							</div>
						</div>
						<!-- question -->

						<div class="row section-padtop-30">
							<div class="col-lg-12">
								<div class="contact-inputs message-box">
									<p class="font-weight-bold p-14 mb-1">Message *</p>
                                                                        <textarea name="Contact[message_text]" id="message_text" class="form-control" required=""></textarea>
								</div>
								<!-- contact-inputs -->
							</div>
						</div>
						<!-- question -->
<!--                                                <div class="row section-padtop-30">
							<div class="col-lg-12">
                                                
                                                 {!! Captcha::display() !!}
                                                        </div>
                                                </div>-->

						<div class="question section-padtop-30">
							<div class="contact-inputs">@csrf
                                                            <button type="submit" class="mcb-btn mcb-trans">SEND MESSAGE</button>
							</div>
							<!-- contact-inputs -->
						</div>
						<!-- question -->
                                                 
                                            </form>
					</div>
					<!-- ask -->
				</div>
				<!-- col -->

				<div class="col-lg-6 pl-5">
					<h2 class="mcb-h2 section-padbottom-30 m-0 res-paddingtop-50 m-top">{{$contact_details}}</h2>

						<div class="contact-info mcb-trans mb-4">
							<h4 class="mcb-h4">Contact Details</h4>
							<p class="p-14 m-0">Corner S1203 and S1205 (Near AllightPrimax FZCO)<br>Jebel Ali Free Zone South Dubai, UAE</p>
							<div class="contact-numbers d-flex justify-content-between align-items-center section-padtop-30">
								<div class="call">
									<h6 class="mcb-h6 m-dark m-0">Give Us a Call</h6>
									<a href="tel:+97158865100"><p class="m-0">+971-4-886-5100</p></a>
								</div>
								<div class="call">
									<h6 class="mcb-h6 m-dark m-0">Send Us An Email</h6>
									<a href="mailto: export@mcb.ae"><p class="m-0">export@mcb.ae</p></a>
								</div>
							</div>
							<!-- contact-numbers -->

							<div class="contact-numbers d-flex justify-content-between align-items-end section-padtop-30">
								<div class="call">
									<h6 class="mcb-h6 m-dark m-0">Working Hours:</h6>
									<p class="m-0">Sunday - Thursday</p>
								</div>
								<div class="call">
									<p class="m-0">8:00AM - 5:00PM</p>
								</div>
							</div>
							<!-- contact-numbers -->
						</div>
						<!-- contact-info -->

						<div class="contact-info mcb-trans">
							<h4 class="mcb-h4">Dubai Showroom</h4>
							<h5 class="mcb-h5 m-dark">Mineral Circles Auto Spare Parts</h5>
							<p class="p-14 m-0">Nasser Square Al Burj St.(behind Tara Hotel)<br>Deira Dubai, UAE</p>

							<div class="contact-numbers d-flex justify-content-between align-items-center section-padtop-30">
								
								<div class="call">
									<a href="https://wa.me/+971557895388"><h6><span class="pr-2"><i class="fab fa-whatsapp fa-lg"></i></span>+971-55-789-5388</h6></a>
									<a href="tel:+97142247047"><h6><span class="pr-2"><i class="fas fa-phone-alt fa-lg"></i></span>+971-4-224-7047</h6></a>
								</div>

								<div class="call">
									<a href="fax:+97142216075"><h6><span class="pr-2"><i class="fas fa-fax fa-lg"></i></span>+971-4-221-6075</h6></a>
									<a href="mailto: sales@mcb.ae"><h6><span class="pr-2"><i class="fas fa-envelope-open-text fa-lg"></i></span>sales@mcb.ae</h6></a>
								</div>
							</div>
							<!-- contact-numbers -->

							<h6 class="mcb-h6 m-dark section-padtop-30">Operating Hours:</h6>

							<div class="contact-numbers d-flex justify-content-between align-items-start">
								<div class="call">
									<p class="mcb-h6 m-dark m-0">Saturday-Wednesday</p>
								</div>
								<div class="call">
									<p class="m-0">(8:00AM - 1:30PM)<br>(4:00PM - 8:30PM)</p>
								</div>
							</div>
							<!-- contact-numbers -->

							<div class="contact-numbers d-flex justify-content-between align-items-center mt-3">
								<div class="call">
									<p class="mcb-h6 m-dark m-0">Thursday</p>
								</div>
								<div class="call">
									<p class="m-0">(8:00AM - 5:30PM)</p>
								</div>
							</div>
							<!-- contact-numbers -->
						</div>
						<!-- contact-info -->

				</div>
				<!-- col -->
			</div>
		</div>
		<!-- container -->
	</section>
	<!-- contact-us -->


@stop
