@extends('layout.default')

@section('content')

@php
if(app()->getLocale()=="ar")
{

    $setting_site="site-arabic.";


}
else
{
    $setting_site="site.";

}

@endphp

<div class="banner-area bg-overlay" id="banner-area" style="background-image:url(images/banner/contact_banner.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="banner-heading">
                    <h1 class="banner-title">Request for <span>Quotation</span></h1>
                    <ol class="breadcrumb">
                        <li>Home</li>
                        <li><a href="#">contact</a></li>
                    </ol>
                    <!-- Breadcumb End -->
                </div>
                <!-- Banner Heading end -->
            </div>
            <!-- Col end-->
        </div>
        <!-- Row end-->
    </div>
    <!-- Container end-->
</div>
<!-- Banner area end-->

<section id="main-container" class="main-container ts-contact-us">
    <div class="container">



        <div class="space-medium">
            <div class="container">
                <div class="row">
                    <div class="col-xl-5 col-lg-12 col-md-12 col-sm-12 col-12">
                        
                        {!! setting($setting_site.'quotation_content') !!}
                    
                    </div>
                    <!-- /.feature-sections -->
                    <div class="offset-xl-1 col-xl-6 offset-lg-1 col-lg-10 offset-md-1 col-md-10 col-sm-12 col-12 mt30">
                        <form class="contactMe" action="/request-for-quotation" method="POST"
                            enctype="multipart/form-data">
                            <!-- service-form -->
                            <div class="service-form">
                                <div class="row">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mb10 ">
                                        <h3>Request a Quotation</h3>
                                    </div>

                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12  ">
                                        <div class="form-group service-form-group">
                                            <label class="control-label sr-only" for="name"></label>
                                            <input id="name" name="name" type="text" placeholder="Name"
                                                class="form-control" required>
                                            <div class="form-icon"><i class="fa fa-user"></i></div>
                                        </div>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                        <div class="form-group service-form-group">
                                            <label class="control-label sr-only" for="email"></label>
                                            <input name="email" id="email" type="email" placeholder="Email"
                                                class="form-control" required>
                                            <div class="form-icon"><i class="fa fa-envelope"></i></div>
                                        </div>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                        <div class="form-group service-form-group">
                                            <label class="control-label sr-only" for="phone"></label>
                                            <input name="phone" id="phone" type="text" placeholder="Phone"
                                                class="form-control" required>
                                            <div class="form-icon"><i class="fa fa-phone"></i></div>
                                        </div>
                                    </div>

 <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                        <div class="form-group service-form-group">
                                            <label class="control-label sr-only" for="company"></label>
                                            <input name="company" id="company" type="text" placeholder="Company"
                                                class="form-control" required>
                                            <div class="form-icon"><i class="fa fa-building"></i></div>
                                        </div>
                                    </div>

                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                        <div class="form-group service-form-group">
                                            Select File:
                                            <label class="control-label sr-only" for="file">asd</label>
                                            <input id="file" type="file" placeholder="Quotation File"
                                                class="form-control">
                                            <div class="form-icon"><i class="fa fa-link"></i></div>
                                        </div>
                                    </div>

                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                        <div class="form-group">
                                            <label class="control-label sr-only" for="textarea"></label>
                                            <textarea class="form-control" id="textarea" name="textarea" rows="3"
                                                placeholder="Messages"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                        <label> <span class="checkbox"><input type="checkbox" name="acceptanceGdpr"
                                                    value="1" aria-invalid="false" id="acceptance"></span> <span
                                                class="label">By using this form you agree with the storage and handling
                                                of your data by this website </span></label>


                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                        <div class="form-group"> @csrf


                                            {!! Captcha::display() !!}

                                        </div>

                                    </div>


                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <div class="msg"></div>
                                        </div>
                                    </div>




                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                        <div class="form-group">

                                            <input style="margin-right:0px;" type="submit" name="submit" id="submit"
                                                class="btn btn-red btn-block mb101 form-control" value="Submit" />
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- /.service-form -->
                    </div>


                </div>
            </div>
        </div>
        <link rel="stylesheet" href="/telinput/css/intlTelInput.css">
        <style>
        .iti__flag {
            background-image: url("/telinput/img/flags.png");
        }

        @media (-webkit-min-device-pixel-ratio: 2),
        (min-resolution: 192dpi) {
            .iti__flag {
                background-image: url("/telinput/img/flags@2x.png");
            }
        }

        .iti {
            width: 100%;
        }

        ul,
        ol {}

        a {
            text-decoration: none;
            color: #50595e;
            -webkit-transition: all 0.3s;
            -moz-transition: all 0.3s;
            transition: all 0.3s;
        }

        a:focus,
        a:hover {
            text-decoration: none;
            color: #ce0058;
        }

        sub {
            bottom: 0em;
        }

        .lead {
            font-size: 20px;
            font-weight: 500;
            line-height: 1.7;
            font-family: 'Barlow Semi Condensed', sans-serif;
            margin-bottom: 20px;
            color: #06131a;
        }


        .feature-left {
            margin-bottom: 40px;
        }

        .feature-left .feature-icon {
            font-size: 32px;
            color: #4cbec5;
            display: inline-block;
            float: left;
            padding-right: 40px;
            padding-top: 0px;
        }

        .feature-left .feature-content {
            font-size: 20px;
        }

        .feature-left:last-child {}

        textarea.form-control {
            height: 90px;
        }

        .required {}

        .form-group {
            margin-bottom: 5px;
        }

        select.form-control:not([size]):not([multiple]) {
            height: 56px;
            color: #50595e;
        }

        .hero-section .form-control {
            border: 1px solid #8c897d;
            height: 58px;
        }

        .form-control {
            border-radius: 0px;
            font-size: 14px;
            font-weight: 500;
            width: 100%;
            height: 56px;
            padding: 14px 18px;
            line-height: 1.42857143;
            border: 1px solid #d2d8db;
            background-color: #fff;
            text-transform: capitalize;
            letter-spacing: 0px;
            margin-bottom: 14px;
            -webkit-box-shadow: inset 0 0px 0px rgba(0, 0, 0, 1);
            box-shadow: inset 0 0px 0px rgba(0, 0, 0, 1);
            -webkit-appearance: none;
        }

        .form-control:focus {
            color: #06131a !important;
            outline: 0;
            box-shadow: 0 0 0 0.1rem rgb(234, 231, 222);
            border-color: #06131a;
        }

        input::-webkit-input-placeholder {
            color: #50595e !important;
        }

        textarea::-webkit-input-placeholder {
            color: #50595e !important;
        }

        .input-group-addon {
            background-color: transparent;
            border: 1px solid #eee;
            border-radius: 0px;
            position: absolute;
            right: 16px;
            top: 16px;
            font-size: 12px;
        }

        .focus {
            border: 1px solid #d2d8db;
        }

        .focus:focus {
            border: 1px solid #06131a;
        }

        .input-group-addon i {
            color: #63480f;
        }

        .btn-select {
            font-size: 14px;
        }

        select option {
            margin: 40px;
            background: rgba(255, 255, 255, 1);
            color: #50595e;
            text-shadow: 0 1px 0 rgba(0, 0, 0, 0.4);
            position: relative;
        }

        select.form-control {
            position: relative;
        }

        select {
            -moz-appearance: none;
            border-radius: 0px;
            font-size: 14px;
            font-weight: 700;
            width: 100%;
            padding: 14px;
            line-height: 1.42857143;
            border: 1px solid #e0e5e9;
            background-color: #fff;
            text-transform: capitalize;
            -webkit-appearance: none;
            -webkit-box-shadow: inset 0 0px 0px rgba(0, 0, 0, .075);
            box-shadow: inset 0 0px 0px rgba(0, 0, 0, .075);
        }

        .select {
            position: relative;
            display: block;
            line-height: 2.2;
            overflow: hidden;
        }

        select {
            width: 100%;
            height: 100%;
            margin: 0;
            padding: 0 0 0 .5em;
            cursor: pointer;
            color: #50595e;
            font-size: 16px;
        }

        .select::after {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            padding: 10px 17px 0px 0px;
            background: transparent;
            pointer-events: none;
            font-family: 'FontAwesome';
            content: "\f107";
            font-size: 16px;
        }

        .select:hover::after {
            color: #50595e;
        }

        .select::after {
            -webkit-transition: .25s all ease;
            -o-transition: .25s all ease;
            transition: .25s all ease;
        }

        select option {
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
            cursor: pointer;
            padding: 10px;
            width: 100%;
            background-color: #fff;
            font-family: 'Barlow Semi Condensed', sans-serif;
        }

        .input-group>.custom-select:not(:last-child),
        .input-group>.form-control:not(:last-child) {
            border-top-right-radius: 0px;
            border-bottom-right-radius: 0px;
        }

        .service-form {
            background-color: #fafbfb;
            border: 1px solid #e4e8ea;
            padding: 40px 40px 20px 40px;
        }

        .service-form-group {
            position: relative;
        }

        .form-icon {
            position: absolute;
            right: 14px;
            bottom: 15px;
            font-size: 13px;
        }

        .btn {
            font-family: 'Barlow Semi Condensed', sans-serif;
            font-size: 16px;
            text-transform: uppercase;
            font-weight: 700;
            padding: 10px 26px;
            letter-spacing: 0px;
            border-radius: 0px;
            line-height: 2;
            -webkit-transition: all 0.3s;
            -moz-transition: all 0.3s;
            transition: all 0.3s;
            word-wrap: break-word;
            white-space: normal !important;
        }


        .btn-default {
            background-color: #ce0058;
            color: #fff;
            border: 2px solid #ce0058;
        }

        .btn-default:hover {
            background-color: transparent;
            color: #ce0058;
            border: 2px solid #ce0058;
        }

        .btn-default.focus,
        .btn-default:focus {
            background-color: transparent;
            color: #ce0058;
            border: 2px solid #ce0058;
            box-shadow: 0 0 0 0.2rem rgb(206, 0, 88);
        }

        .space-medium {
            padding-top: 80px;
            padding-bottom: 80px;
        }
        </style>











</section>

<script src="/telinput/js/intlTelInput.js"></script>
<script>
  var input = document.querySelector("#phone");
  window.intlTelInput(input, {
  initialCountry: "ae" });

</script>

<style>
img#refresh {

    margin-left: 165px;
    cursor: pointer;
    position: absolute;
    top: 12%;
    z-index: 99999999;
}
</style>



<!-- Main container end -->
@stop
