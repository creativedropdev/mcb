	<section class="map-area">
		<div class="row no-gutters">
			<div class="col-sm-6 col-md-6">
				<div class="map-slider">
					<div id="demo" class="carousel slide">
                                            
					  <div class="carousel-inner">
                                              @foreach($branches as $bkey=> $branch)
                                            <div class="carousel-item <?php if($bkey == 0){ ?> active <?php }?>">
					      <!--<img src="{{ Voyager::image( $branch->image ) }}" alt="" height="400px" width="100%">-->
					      <img src="https://mcb.workspace.destring.com/storage/{{  $branch->image }}" alt="{!!$branch->title !!}" height="400px" width="100%">
					      <div class="carousel-caption">
					        <h3 class="text-white float-left p-2 mb-orange">{!!$branch->title !!}</h3>
					      </div>
					    </div>
                                              @endforeach
                                              
					    
					  </div>
					  <a class="carousel-control-prev" href="#demo" data-slide="prev">
					    <span class="carousel-control-prev-icon"></span>
					  </a>
					  <a class="carousel-control-next" href="#demo" data-slide="next">
					    <span class="carousel-control-next-icon"></span>
					  </a>
					</div>
				</div>
				<!-- map-slider -->

				<div class="map-dropdown">
					<select class="form-control">
                                            @foreach($branches as $branch)
                                                <option value="{!! str_slug($branch->title) !!}">{!!$branch->title !!}</option>
                                            @endforeach
					</select>
				</div>

			</div>
			<!-- col -->

			<div class="col-sm-6 col-md-6">
				<div  id="map">
				</div> <!-- map -->
			</div>
		</div>
		<!-- row -->
	</section>
	<!-- map-area -->
        
        