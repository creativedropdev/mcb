<div class="row">
                        <div class="col-lg-12">
                           

<hr>
<h1>Apply for Internship Program</h1>

<div id="contact-form" class="form-container contact-us">
                  <!-- START copy section: Hotel Contact Form -->
                  <form class="contactMe" action="{{action('JobsController@internshipHandler')}}" method="POST" enctype="multipart/form-data">
                     <section>
                        <div class="form-row">
                           <div class="col-lg-6">
                               <select required class="select2" name="department">
                                   <option value="">Department</option>
                                   <option >Sales</option>
                                   <option >Marketing</option>
                                   <option >Accounts</option>
                                   <option >Purchase</option>
                                   <option >Admin</option>
                                   <option >Other</option>
                               </select>
                           </div>
                            <div class="col-lg-6">
                               <select required class="select2" name="department">
                                   <option value="">Duration</option>
                                   <option >0-1 Month</option>
                                   <option >2-3 Months</option>
                                   <option >3-6 Months</option>
                                   <option >More than 6 months</option>
                                   
                               </select>
                           </div>
                          
                        </div>
                       
                        <div class="form-row">
                           <div class="col-lg-6">
                              <input class="field" type="text" name="graduation" placeholder="Graduation Date">
                           </div>
                            <div class="col-lg-6">
                               <select required class="select2" name="department">
                                   <option value="">Valid UAE Visa?</option>
                                   <option >Yes</option>
                                   <option >No</option>
                                   
                                   
                               </select>
                           </div>
                          
                        </div>
                        
                         <div class="form-row">
                             <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12"> Resume: </div>
                            
                            <div class="col-md-10">
                              <input type="file" name="resume" class="field">
                               </div>
                              </div>
                           </div>
                           <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12"> Recommendation Letter: </div>
                            
                            <div class="col-md-10">
                              <input type="file" name="recommendation" class="field">
                               </div>
                              </div>
                           </div>
                        </div>
                        
                        <div class="form-row">
                           <div class="col-md-12">
                              <textarea name="message" data-displayname="Message" class="field" placeholder="Message"  required=""></textarea>
                           </div>
                        </div>
                         <div class="msg"></div>
                          <div class="form-row">
<div class="col-md-6">
                       @csrf
  
  {!! Captcha::display() !!}

</div>
<div class="col-md-6 text-right">
   <button class="buttonPrimary" type="submit" data-sending="Sending...">Apply for Internship</button>
</div>
    </div>
                       

                        
                     </section>
                     <!-- Ection end -->
                  </form>
                  <!-- END copy section:Service Contact Form -->
               </div>
    
                        </div>



                     </div>        
                     
