@extends('layout.default')
@section('content')
<section class="mcb-slider current-opening-banner">
	<h1 class="mcb-h1">{{$page->title}}</h1>
</section>
@include('Front.banner')


<section class="work section-bg-grey section-padtop-50 section-padbottom-50">
	<div class="mcb-container">
		<div class="row intern-head">
			<div class="col-md-10 m-auto">
				<div class="work-content text-center">
					<h2 class="mcb-h2 m-0">
						THE CITY OF DUBAI IS WAVING TO OUR NEXT<br>
						INTERN STUDENT.
					</h2>
					<p class="mt-3 mb-0">We always make sure that there are vacant seats available for a young, dynamic, go-getter and extraordinary Intern who wishes to kickstart his/her career journey with us! Our Internship program offers a platform for university and college students to grow and develop their skills in an actual business environment.</p>
				</div>
			</div>
		</div>
	</div>
	<!-- contianer -->
</section>

<section class="section-bg-grey">
	<div class="mcb-container">
		<div class="section-padbottom-50">
			<div class="row internship-requirements">
				
				<div class="col-md-6">
					<div class="trusted-img intern-img">
						<img src="/assets/img/internship-man.png" alt="./">
					</div>
				</div>

				<div class="col-md-6 align-self-center intern-margintop-481">
					<div class="trusted-content pl-5">
						<h4 class="mcb-h4">APPLICATION REQUIREMENTS:</h4>
						<p class="para-twenty requirement font-weight-bold mb-0">This Internship program is for students who:</p>
						<p class="para-twenty requirement ml-4 m-0">Resides in UAE with a valid UAE visa.</p>
						<p class="para-twenty requirement ml-4 m-0">Foreign students who are willing to work in UAE (specifically in the magnificent city of Dubai!) with their own valid UAE visa. </p>
						<br>
						<p class="para-twenty requirement font-weight-bold m-0">Possesses unwavering commitment.</p>
						<p class="para-twenty requirement font-weight-bold m-0">Supports our diversity program and has a strong grasp of TOLERANCE.</p>
						<p class="para-twenty requirement font-weight-bold m-0">A sentient human being. We are keen to welcome someone who values work-life balance likewise, those who know how to take a break when needed. Being too focused is dull and boring. Have a break, get a cup of free coffee down our office.</p>
						<br>
						<p class="para-twenty m-0">Interested applicants can feel free to fill-up the form below with their valid and legitimate credentials along with their applications. From there, qualified and interesting applicants will be contacted by our HR Manager to discuss further.</p>
					</div> <!-- trusted-content -->
				</div>
				
			</div>
		</div>
	</div>
</section>

<section class="section-padbottom-50 section-bg-grey">
	<div class="mcb-container">
		<div class="row">
			<div class="col-md-12">
				<h3 class="mcb-h3 m-0">
					Apply for Internship Program
				</h3>
			</div>
		</div>
            <form class="contactMe" action="{{action('JobsController@internshipHandler')}}" method="POST" enctype="multipart/form-data">

		<div class="row mt-5 res-margintop-20">
                    
			<div class="col-md-6">
				<div class="contact-inputs intership-form">
					<select required class="select2 form-control" name="department">
                                   <option value="">Department</option>
                                   <option >Sales</option>
                                   <option >Marketing</option>
                                   <option >Accounts</option>
                                   <option >Purchase</option>
                                   <option >Admin</option>
                                   <option >Other</option>
                               </select>
				</div>
			</div>
			<div class="col-md-6">
				<div class="contact-inputs intership-form">
					<select required class="select2 form-control" name="duration" >
                                   <option value="">Duration</option>
                                   <option >0-1 Month</option>
                                   <option >2-3 Months</option>
                                   <option >3-6 Months</option>
                                   <option >More than 6 months</option>
                                   
                               </select>
				</div>
			</div>
			<div class="col-md-6">
				<div class="contact-inputs intership-form">
                                    <input type="text" name="graduation_year" class="form-control" placeholder="Graduation Year">
				</div>
			</div>
			<div class="col-md-6">
				<div class="contact-inputs intership-form">
					<select required class="select2 form-control" name="uae-visa">
                                   <option value="">Valid UAE Visa?</option>
                                   <option >Yes</option>
                                   <option >No</option>
                                   
                                   
                               </select>
				</div>
			</div>
<!--			<div class="col-md-6">
				<div class="contact-inputs intership-form">
					<input type="file" name="resume" accept="application/pdf,.doc,.docx" class="form-control choose-file-input">
				</div>
			</div>-->
<!--			<div class="col-md-6">
				<div class="contact-inputs intership-form">
					<input type="file" name="recommendation" accept="application/pdf,.doc,.docx" class="form-control choose-file-input">
				</div>
			</div>-->
			<div class="col-md-12">
				<div class="contact-inputs message-box intership-form">
					<textarea name="message" class="form-control" placeholder="Message"></textarea>
				</div>
				<!-- contact-inputs -->

				@csrf
  
<!--                                <div class="contact-inputs section-padbottom-15">
					<div class="g-recaptcha" data-sitekey="6LcmQc0ZAAAAAEDSVqw-cF5NtfC3FDLx2n2RUdgE"><div style="width: 304px; height: 78px;"><div><iframe src="https://www.google.com/recaptcha/api2/anchor?ar=1&amp;k=6LcmQc0ZAAAAAEDSVqw-cF5NtfC3FDLx2n2RUdgE&amp;co=aHR0cDovL3BvcGFsdHJhaW5pbmcuY29tOjgw&amp;hl=en&amp;v=T9w1ROdplctW2nVKvNJYXH8o&amp;size=normal&amp;cb=gjogoozg5e6g" role="presentation" name="a-9x9t2a7lodrw" scrolling="no" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-top-navigation allow-modals allow-popups-to-escape-sandbox allow-storage-access-by-user-activation" width="304" height="78" frameborder="0"></iframe></div><textarea id="g-recaptcha-response" name="g-recaptcha-response" class="g-recaptcha-response" style="width: 250px; height: 40px; border: 1px solid rgb(193, 193, 193); margin: 10px 25px; padding: 0px; resize: none; display: none;"></textarea></div><iframe style="display: none;"></iframe></div>
				</div>-->
				<!-- contact-inputs -->

                                <button type="submit" class="btn mcb-btn-blue mcb-bold pl-5 pr-5">APPLY FOR INTERNSHIP</button>
			</div>
                        
		</div>
            </form>
	</div>
</section>
@endsection