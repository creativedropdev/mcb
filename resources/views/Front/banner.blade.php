@php
if(app()->getLocale()=="ar")
{

    $setting_site="site-arabic.";
    $setting_home="home-arabic.";
    $setting_testimonials =  setting('home-arabic.testimonials');
    $request_callback_desc =  setting('home-arabic.request_callback_desc_ar');
    $quality_bearing =  setting('home-arabic.quality_bearing_ar');
    $all_products = setting('menu-arabic.products');

}
else
{
    $setting_site="site.";
    $setting_home="home.";
    $setting_testimonials = setting('home.testimonials') ;
    $request_callback_desc = setting('home.request_callback_desc') ;
    $quality_bearing = setting('home.quality_bearing') ;
    $all_products =  "View All Products";
}

@endphp
<div class="request">
	<div class="mcb-container">
		<div class="row">
			<div class="col-12 col-sm-6 col-md-7 align-self-center res-margintop-20 res-marginbottom-20 res-center">
				<p class="m-0">{{ $request_callback_desc }}</p>
			</div>
			<div class="col-12 col-sm-6 col-md-5 align-self-center res-marginbottom-20 res-center">
				<button class="btn mcb-btn mcb-btn-orange float-right res-no-float" data-toggle="modal" data-target="#mcbModal"><img src="/assets/img/call-icon.png" width="20px" alt=""><span class="ml-2">REQUEST FOR A CALLBACK</span></button>
			</div>
		</div>
	</div>
</div>
<!-- request -->