@extends('layout.default')

@section('content')

@php if(app()->getLocale()=="ar"){
$brand_product = setting('site-arabic.brand-product');

}else{
$brand_product = "Brand Products";
}
@endphp

<section class="mcb-slider brand-banner">
    <img src="/storage/{{ ( $brand->image ) }}" alt="" width="25%">
       <h1 class="mcb-h1 m-0">{{-- $brand->title --}}</h1>
</section>
<div class="mcb-main-container">
<section class="brands-skf section-bg-grey section-padtop-50 section-padbottom-50">
	<div class="mcb-container">
            <div class="row">
                <div class="col-lg-4">
                    <ul class="nav nav-tabs product-page-tabs tabs-left">
                         @foreach($sharedData['brands'] as $brandItem)
                        <li class="   products-item">
                            <a aria-selected="true" 
                               class="pn-ProductNav_Link" href="{{url('/'.app()->getLocale().'/brands/'.$brandItem->slug)}}">{{$brandItem->title}}</a>
                        </li>
                        @endforeach
                    </ul>
                    
                    <div class="rfq-section desktop-rfq">
                        <h3 class="mcb-h3 text-white">Request For Quotation</h3>
                        <div class="rfq-form">
                            <form class="contactMe" action="/request-for-quotation" method="POST" enctype="multipart/form-data">

                                <div class="contact-inputs mt-3">
                                        <p class="font-weight-bold p-14 mb-1 text-white">Contact Person*</p>
                                        <input name="RFQContact[full_name]" id="RFQContact_name" type="text" class="form-control" required="">
                                </div>

                                <div class="contact-inputs mt-3">
                                        <p class="font-weight-bold p-14 mb-1 text-white">Email*</p>
                                        <input name="RFQContact[email]" id="RFQContact_email" type="text" class="form-control" required="">
                                </div>

                                <div class="contact-inputs mt-3">
                                        <p class="font-weight-bold p-14 mb-1 text-white">Phone</p>
                                        <input name="RFQContact[phone]" id="RFQContact_phone" type="text" class="form-control">
                                </div>

                                <div class="contact-inputs mt-3">
                                        <p class="font-weight-bold p-14 mb-1 text-white">Company Name</p>
                                        <input name="RFQContact[company]" id="RFQContact_company" type="text" class="form-control">
                                </div>

                                <div class="mt-3">
                                        <div class="contact-inputs enquiry message-box">
                                                <p class="font-weight-bold p-14 mb-1 text-white">Message *</p>
                                                <textarea name="RFQContact[message_text]"
                                                          class="form-control w-100" style="height: 100px;" required=""></textarea>
                                        </div>
                                        <!-- contact-inputs -->
                                </div>


<!--						<div class="contact-inputs mt-3">
                                        <input id="file" type="file" name="filename" class="form-control h-100">
                                </div>-->

<!--						<div class="contact-inputs rfq-captcha section-padtop-15">
                                        <div class="g-recaptcha" data-sitekey="6LcmQc0ZAAAAAEDSVqw-cF5NtfC3FDLx2n2RUdgE"></div>
                                </div>-->
                                <!-- contact-inputs -->

                                <div class="form-group">
                                            <input type="hidden" name="RFQContact[enquiry_type]" id="RFQContact_enquiry_type"
                                                   value="{{$brand->title}}" class="form-control" placeholder="General Enquiry" required="">
                                        </div>
                                <div class="question mt-3">
                                        <div>@csrf
                                                <button class="mcb-btn mcb-trans mcb-btn-orange minus-margin-for-rfq">SUBMIT
                                                        REQUEST</button>
                                        </div>
                                        <!-- contact-inputs -->
                                </div>
                                <input type="hidden" name="_token" value="Nfz50LFxvuKuHmwNxgIN89cIx5uhJx7D68n6apJd">						<!-- question -->
                            </form>
                        </div>
                </div>
                </div>
                <div class="col-lg-8">
                    {!!$brand->description!!}<br />
                   
                    <!--Media-->
                    <div class="col-md-6">{{$brand->range}}</div>
                    <!--<div class="col-md-6">Dist Certificate</div>-->
                </div>
            </div>
		
	</div>
</section>

    <section class="clients brand-clients section-bg-grey section-padbottom-50" style="direction: ltr">
	<div class="mcb-container">
            <div class="row" style="text-align: left">
		       <div class="col-md-12">
                            <h2 class="mcb-h2 section-padbottom-30 m-0"><strong>{{ $brand_product}}</strong></h2>
                     </div>
              </div>
              <div class="row carousel-row">
              <div class="col-md-12">
	        <div class="product-slider">
                    @foreach($brand->product as $product)

                   @php
                $img=$product->thumbnail('cropped');

                if(app()->getLocale()=="ar")
                {
                $product=$product->translate('ar');
                }
                @endphp

                @if($product->slug!="")
	        	<div class="col product-padding-left-10-320 product-padding-right-10-320">
              		<a href="/products/{{$product->slug}}">
                            
                            
                                   <div class="product-brand">
                                   <!-- social-brand -->
                                   <div class="product-img text-center section-padbottom-30">
                                          <!--<img src="assets/img/skf-product.png" alt="" class="w-50">-->
                                          <img class="img-fluid brand-image" src="/storage/{{ ( $product->image ) }}" alt="" class="w-50">
                                   </div>
                                   <!-- product-img -->
                                   <div class="truck product-wheel">
                                          <h6 class="mcb-h6">{{ $product->title }}</h6>
                                          <p class="p-14 m-0">
                                              
                                              View Product <span class="ml-2"><i class="fas fa-long-arrow-alt-right"></i></span></p>
                                   </div>
                            </div>
                            <!-- product-brand -->
                            </a>
                            <div class="social-brand">
                                   <ul>
                                          <li><a href="#" class="m-dark mcbModalProducts" data-toggle="modal" data-target="#mcbModal"><i class="far fa-comments"></i></a></li>
                                          <li><a class="m-dark" target="_blank" href=https://api.whatsapp.com/send?phone={{ trim(setting('contact.whatsapp'),'+') }}&text=Good%20day%20Mineral%20Circles%20Bearings%20Team%21%20Please%20contact%20me%20as%20soon%20as%20possible%20as%20{{rawurlencode('I want to know about '.$product->title.'.') }}&source=&data=" class="readmore contact" ><i class="fab fa-whatsapp"></i></a></li>
                                   </ul>
                            </div>
                        </div>
                    @endif    
                    @endforeach
	        </div>
	            
	        </div>
		</div>
	</div>
	<!-- mcb-container -->
</section>
<!-- clients -->

<section class="section-bg-grey section-padbottom-50 touch-device-rfq">
	<div class="mcb-container">
		<div class="row">
			<div class="col-12">
				<div class="rfq-section">
					<h3 class="mcb-h3 text-white">Request For Quotation</h3>
					<div class="rfq-form">
                            <form class="contactMe" action="/request-for-quotation" method="POST" enctype="multipart/form-data">

                                <div class="contact-inputs mt-3">
                                        <p class="font-weight-bold p-14 mb-1 text-white">Contact Person*</p>
                                        <input name="RFQContact[full_name]" id="RFQContact_name" type="text" class="form-control" required="">
                                </div>

                                <div class="contact-inputs mt-3">
                                        <p class="font-weight-bold p-14 mb-1 text-white">Email*</p>
                                        <input name="RFQContact[email]" id="RFQContact_email" type="text" class="form-control" required="">
                                </div>

                                <div class="contact-inputs mt-3">
                                        <p class="font-weight-bold p-14 mb-1 text-white">Phone</p>
                                        <input name="RFQContact[phone]" id="RFQContact_phone" type="text" class="form-control">
                                </div>

                                <div class="contact-inputs mt-3">
                                        <p class="font-weight-bold p-14 mb-1 text-white">Company Name</p>
                                        <input name="RFQContact[company]" id="RFQContact_company" type="text" class="form-control">
                                </div>

                                <div class="mt-3">
                                        <div class="contact-inputs enquiry message-box">
                                                <p class="font-weight-bold p-14 mb-1 text-white">Message *</p>
                                                <textarea name="RFQContact[message_text]"
                                                          class="form-control w-100" style="height: 100px;" required=""></textarea>
                                        </div>
                                        <!-- contact-inputs -->
                                </div>


<!--						<div class="contact-inputs mt-3">
                                        <input id="file" type="file" name="filename" class="form-control h-100">
                                </div>-->

<!--						<div class="contact-inputs rfq-captcha section-padtop-15">
                                        <div class="g-recaptcha" data-sitekey="6LcmQc0ZAAAAAEDSVqw-cF5NtfC3FDLx2n2RUdgE"></div>
                                </div>-->
                                <!-- contact-inputs -->

                                <div class="form-group">
                                            <input type="hidden" name="RFQContact[enquiry_type]" id="RFQContact_enquiry_type"
                                                   value="{{$product->title}}" class="form-control" placeholder="General Enquiry" required="">
                                        </div>
                                <div class="question mt-3">
                                        <div>@csrf
                                                <button class="mcb-btn mcb-trans mcb-btn-orange minus-margin-for-rfq">SUBMIT
                                                        REQUEST</button>
                                        </div>
                                        <!-- contact-inputs -->
                                </div>
                               						<!-- question -->
                            </form>
                        </div>
				</div>
				<!-- rfq-form -->
			</div>
		</div>
	</div>
</section> 

</div>
@stop