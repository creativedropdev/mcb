<?php use Illuminate\Support\Facades\Storage;
//Storage::exists('brands/October2020/CGJVOGucXSEIjaL9bhT3-cropped.jpg')
?>
@extends('layout.default')

@section('content')

@php

if(app()->getLocale()=="ar")
{

    $setting_site="site-arabic.";
    $brands_list_title=setting('brands.brands_list_title_ar');
    $brands_list_description=setting('brands.brands_description_ar');


}
else
{
    $setting_site="site.";
    $brands_list_title=setting('brands.brands_list_title');
    $brands_list_description=setting('brands.brands_description');
}

@endphp

@php
$svgData=['width'=>'86px', 'height'=>'86px'];

@endphp
<section class="mcb-slider brand-banner">
	<h1 class="mcb-h1 m-0 custom-width">{{ $brands_list_title }} </h1>
</section>


<style>
    .container
    {
    max-width:90%;
    }
    </style>

@include('Front.banner')

<section class="section-bg-grey section-padtop-50 section-padbottom-50">
	<div class="mcb-container">
		<div class="row">
			<div class="col-lg-4">
                            <h4 class="mcb-h4 mb-3">{{ $brands_list_description }}</h4>

                    <div class="rfq-section desktop-rfq">
					<h3 class="mcb-h3 text-white">Request For Quotation</h3>
					<div class="rfq-form">
                                            <form class="contactMe" action="/request-for-quotation" method="POST"
                            enctype="multipart/form-data">

						<div class="contact-inputs mt-3">
							<p class="font-weight-bold p-14 mb-1 text-white">Contact Person*</p>
                                                        <input name="RFQContact[full_name]" id="RFQContact_name" type="text" class="form-control" required="">
						</div>

						<div class="contact-inputs mt-3">
							<p class="font-weight-bold p-14 mb-1 text-white">Email*</p>
                                                        <input name="RFQContact[email]" id="RFQContact_email" type="text" class="form-control" required="">
						</div>
                                                
						<div class="contact-inputs mt-3">
							<p class="font-weight-bold p-14 mb-1 text-white">Phone</p>
							<input name="RFQContact[phone]" id="RFQContact_phone" type="text" class="form-control">
						</div>

						<div class="contact-inputs mt-3">
							<p class="font-weight-bold p-14 mb-1 text-white">Company Name</p>
							<input name="RFQContact[company]" id="RFQContact_company" type="text" class="form-control">
						</div>

						<div class="mt-3">
							<div class="contact-inputs enquiry message-box">
								<p class="font-weight-bold p-14 mb-1 text-white">Message *</p>
                                                                <textarea name="RFQContact[message_text]" class="form-control w-100" style="height: 100px;" required=""></textarea>
							</div>
							<!-- contact-inputs -->
						</div>


<!--						<div class="contact-inputs mt-3">
							<input id="file" type="file" name="filename" class="form-control h-100">
						</div>-->

<!--						<div class="contact-inputs rfq-captcha section-padtop-15">
							<div class="g-recaptcha" data-sitekey="6LcmQc0ZAAAAAEDSVqw-cF5NtfC3FDLx2n2RUdgE"></div>
						</div>-->
						<!-- contact-inputs -->
                                                
                                                <div class="form-group">
                                                    @if(isset($_cat))
                                                    <input type="hidden" name="RFQContact[enquiry_type]" id="RFQContact_enquiry_type" value="{{ $_cat }}" class="form-control" placeholder="General Enquiry" required="">
                                                    @endif
                                                </div>
						<div class="question mt-3">
							<div>
								<button class="mcb-btn mcb-trans mcb-btn-orange minus-margin-for-rfq">SUBMIT
									REQUEST</button>
							</div>
							<!-- contact-inputs -->
						</div>
                                                @csrf
						<!-- question -->
                                            </form>
					</div>
				</div>
				<!-- rfq-form -->

                  </div>
                  
			<!-- col -->

		<div class="col-lg-8 res-paddingtop-30">
                        <div class="row">
                    
                 @foreach($allbrands as $index=>$brand)

                 @php
                  $img= Voyager::image($brand->image );

                    if(app()->getLocale()=="ar")
                    {
                        $brand=$brand->translate('ar');
                    }
                 @endphp

                 @if($brand->slug!="")
                 <div class="col-sm-6 col-md-4">
                                        <a href="/{{app()->getLocale()}}/brands/{{$brand->slug}}">
                            <div class="brand-single-image">
                            @if(!$img) 
                                {{ "NO IMAGE" }}
                            @else
                                <img src="{{  $img  }}" alt="">
                            @endif
                        </div>
                        <!-- brand-brand -->
                    </a>
                </div> <!-- col end -->
               
                

            
            @endif
            @endforeach
                </div>
            <!-- Row End -->
         </div>
         <!-- Service Container -->
      </div>
      <!-- Container end -->
   </section>





@stop
