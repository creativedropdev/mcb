@extends('layout.default')

@section('content')

<section class="section-bg-grey section-padtop-50 section-padbottom-50">
		<div class="mcb-container">
			<div class="row">
				<div class="col-lg-3">
					<div class="blog-links">
						<ul class="nav nav-tabs blog-page-tabs tabs-left">

              @foreach ($CatalogCategory as $key => $category)
                <li class="products-item category-item {{ $key == 0 ? 'active' : '' }}" data-catalog="catalog-{{ $category->id }}"><a aria-selected="{{ $key == 0 ? 'true' : '' }}" href="{{ url('/'.app()->getLocale().'/onlinecatalogs#catalog-'.$category->id) }}" data-toggle="tab" class="d-flex justify-content-between m-dark p-14">{{ $category->name }}</a></li>
              @endforeach

						</ul>
					</div>

					<div class="media-box desktop-media-box-online-catalog">
						<h5 class="mcb-h5 text-white">Any media partnership?</h5>
						<p class="text-white">Send us a message at <span><a href="http://popaltraining.com/mcb/catalog.php#" class="text-white">export@mcb.ae</a></span> now</p>
					</div>
				</div>

				<div class="col-lg-9 col-xl-9">
					<div class="clients">
						<div class="row">
							<div class="col-md-12">
								<div class="search-catalog">
								<input type="text" id="filterCatalogs" placeholder="Search Catalogs" class="form-control">
								<div class="catalog-icon">
									<a href="http://popaltraining.com/mcb/catalog.php#" class="m-blue"><i class="fas fa-search fa-lg"></i></a>
								</div>
							</div>
							<!-- search-catalog -->
								<div class="tab-content">

                  @foreach ($CatalogCategory as $key => $category)

                    <div class="tab-pane catalogCategory blog-tab-pane {{ $key == 0 ? 'active' : '' }}" id="catalog-{{ $category->id }}">
										  <div class="row">
                        
                        @foreach ($category->catalog as $catalog)
                       
                        <div class="catalogItem col-6 col-sm-4 col-md-4 col-lg-3 col-xl-3 res-padright-10">
									  			<div class="industrial section-padtop-30 res-paddingtop-20">
									  				<div class="card catalog-card">
									  					<div class="img-zoom">
															{{-- @if ($catalog->cover)
																<img class="card-img-top" src="{{ asset('assets/img/catalog.jpg') }}" alt="image" width="100%">	
															@else
																<img class="card-img-top" src="{{ asset('assets/img/catalog.jpg') }}" alt="image" width="100%">
															@endif   --}}
															
															<img class="card-img-top" src="{{ asset('assets/img/catalog.jpg') }}" alt="image" width="100%">
															{{-- <img class="card-img-top" src="https://mcb.workspace.destring.com/storage/{{ $catalog->cover }}" alt="{{ $catalog->title }}" width="100%"> --}}
									  					</div>
									  					<!-- img-zoom -->
									  				  <div class="card-body">
									  				    <div class="d-flex justify-content-between align-items-center">
                                  {{-- Auxiliary<br>System 2017 --}}
									  				    	<h6 class="catalogTitle mb-0 p-14">{{ $catalog->title }}</h6>
									  				    	<i class="fas fa-long-arrow-alt-right"></i>
									  				    </div>
									  				    <ul>
                                      @if($catalog->file!="")
										<a class="badge badge-primary" href="https://mcb.workspace.destring.com/storage/{{json_decode($catalog->file)[0]->download_link}}"> EN </a>
									 	 {{-- <a class="badge badge-primary" href="{{Storage::disk(config('voyager.storage.disk'))->url(json_decode($catalog->file)[0]->download_link)}}"> EN </a>  --}}
                                      @endif
                                  
                                      @if($catalog->file_ar!="")
                                        <a class="badge badge-primary" href="https://mcb.workspace.destring.com/storage/{{json_decode($catalog->file_ar)[0]->download_link}}"> AR </a> 
                                      @endif
                                      
                                      @if($catalog->file_sp!="")
                                        <a class="badge badge-primary" href="https://mcb.workspace.destring.com/storage/{{json_decode($catalog->file_sp)[0]->download_link}}"> SP </a>
                                      @endif

                                      @if($catalog->file_pt!="")
                                        @if(isset(json_decode($catalog->file_fr)[0]))
                                          <a class="badge badge-primary" href="https://mcb.workspace.destring.com/storage/{{json_decode($catalog->file_pt)[0]->download_link}}"> PT </a> 
                                        @endif
                                      @endif

                                      @if($catalog->file_fr!="")
                                        @if(isset(json_decode($catalog->file_fr)[0]))
                                          <a class="badge badge-primary" href="https://mcb.workspace.destring.com/storage/{{json_decode($catalog->file_fr)[0]->download_link}}"> FR </a> 
                                        @endif
                                      @endif

                                      @if($catalog->file_de!="")
                                        @if(isset(json_decode($catalog->file_de)[0]))
                                          <a class="badge badge-primary" href="https://mcb.workspace.destring.com/storage/{{json_decode($catalog->file_de)[0]->download_link}}"> DE </a> 
                                        @endif
                                      @endif

                                      @if($catalog->file_it!="")
                                        @if(isset(json_decode($catalog->file_it)[0]))
                                          <a class="badge badge-primary" href="https://mcb.workspace.destring.com/storage/{{json_decode($catalog->file_it)[0]->download_link}}"> IT </a> 
                                        @endif
                                      @endif

                                      @if($catalog->file_ru!="")
                                        @if(isset(json_decode($catalog->file_ru)[0]))
                                          <a class="badge badge-primary" href="https://mcb.workspace.destring.com/storage/{{json_decode($catalog->file_ru)[0]->download_link}}"> RU </a> 
                                        @endif
                                      @endif

                                      @if($catalog->file_pl!="")
                                        @if(isset(json_decode($catalog->file_pl)[0]))
                                          <a class="badge badge-primary" href="https://mcb.workspace.destring.com/storage/{{json_decode($catalog->file_pl)[0]->download_link}}"> PL </a> 
                                        @endif
                                      @endif

									  					  	    {{-- <li><a href="http://popaltraining.com/mcb/catalog.php#" class="mb-blue text-white">EN</a></li> --}}
									  					  </ul>
									  				  </div>
									  				</div>
									  			</div>
									  			<!-- industrial -->
                        </div>

                        @endforeach
									  		   
									  	</div>
									  </div> <!-- blogOne -->
                  
                  @endforeach
                  
								</div> <!-- tab-content -->	            
					    </div> <!-- col -->
						</div>
					</div>
          <!-- clients -->
				

					<section class="section-padtop-50">
						<div class="media-box responsive-media-box">
							<h5 class="mcb-h5 text-white">Any media partnership?</h5>
							<p class="text-white">Send us a message at <span><a href="mailto:%20export@mcb.ae" class="text-white">export@mcb.ae</a></span> now</p>
						</div>
					</section>
				</div> <!-- col -->
			</div>
		</div>
	</section>

@stop