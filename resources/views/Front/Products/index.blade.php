<?php use Illuminate\Support\Facades\Storage;
//Storage::exists('products/October2020/CGJVOGucXSEIjaL9bhT3-cropped.jpg')
?>
@extends('layout.default')

@section('content')

@php

if(app()->getLocale()=="ar")
{

    $setting_site="site-arabic.";


}
else
{
    $setting_site="site.";

}
$_cat ="";
foreach($sharedData['categories'] as $ckey =>$categorycat){

                          
                           if($slug == $categorycat->slug) $_cat = $categorycat->name;
                           if(app()->getLocale()=="ar")
                           {
                                $categorycat=$categorycat->translate('ar');
                           }
}

@endphp

@php
$svgData=['width'=>'86px', 'height'=>'86px'];

@endphp
<section class="mcb-slider products-banner">
	<h1 class="mcb-h1 m-0">{{$_cat}} </h1>
</section>


<style>
    .container
    {
    max-width:90%;
    }
    </style>

@include('Front.banner')
<div class="mcb-main-container">
<section class="section-bg-grey section-padtop-50 section-padbottom-50">
	<div class="mcb-container">
		<div class="row">
			<div class="col-lg-4">
    
         	@foreach($categories as $category)
         	
            @if($category->product->count()>0)
            
                        <ul class="nav nav-tabs product-page-tabs tabs-left">

                          @foreach($sharedData['categories'] as $ckey =>$categorycat)

                           @php
                           
                           if($slug == $categorycat->slug) $_cat = $categorycat->name;
                           if(app()->getLocale()=="ar")
                           {
                                $categorycat=$categorycat->translate('ar');
                           }
                           @endphp

                           @if($categorycat->slug!="")
                           <li class=" <?php if( ($slug == $categorycat->slug) )  { echo "active";  } ?>  products-item">
                               <a aria-selected="true" class="pn-ProductNav_Link" 
                                  href="/products/{{$categorycat->slug}}" >
                                   <span class="svg-ico-nav">@include('Front.svg/'.app()->getLocale().'/'.$categorycat->slug, $svgData)</span>
                               {{ $categorycat->name }}</a>
                           </li>
                           @endif

                           @endforeach

                        </ul>
                     


                    <div class="rfq-section desktop-rfq">
					<h3 class="mcb-h3 text-white">Request For Quotation</h3>
					<div class="rfq-form">
                                            <form class="contactMe" action="/request-for-quotation" method="POST"
                            enctype="multipart/form-data">

						<div class="contact-inputs mt-3">
							<p class="font-weight-bold p-14 mb-1 text-white">Contact Person*</p>
                                                        <input name="RFQContact[full_name]" id="RFQContact_name" type="text" class="form-control" required="">
						</div>

						<div class="contact-inputs mt-3">
							<p class="font-weight-bold p-14 mb-1 text-white">Email*</p>
                                                        <input name="RFQContact[email]" id="RFQContact_email" type="text" class="form-control" required="">
						</div>
                                                
						<div class="contact-inputs mt-3">
							<p class="font-weight-bold p-14 mb-1 text-white">Phone</p>
							<input name="RFQContact[phone]" id="RFQContact_phone" type="text" class="form-control">
						</div>

						<div class="contact-inputs mt-3">
							<p class="font-weight-bold p-14 mb-1 text-white">Company Name</p>
							<input name="RFQContact[company]" id="RFQContact_company" type="text" class="form-control">
						</div>

						<div class="mt-3">
							<div class="contact-inputs enquiry message-box">
								<p class="font-weight-bold p-14 mb-1 text-white">Message *</p>
                                                                <textarea name="RFQContact[message_text]" class="form-control w-100" style="height: 100px;" required=""></textarea>
							</div>
							<!-- contact-inputs -->
						</div>


<!--						<div class="contact-inputs mt-3">
							<input id="file" type="file" name="filename" class="form-control h-100">
						</div>-->

<!--						<div class="contact-inputs rfq-captcha section-padtop-15">
							<div class="g-recaptcha" data-sitekey="6LcmQc0ZAAAAAEDSVqw-cF5NtfC3FDLx2n2RUdgE"></div>
						</div>-->
						<!-- contact-inputs -->
                                                
                                                <div class="form-group">
                                                    @if(isset($_cat))
                                                    <input type="hidden" name="RFQContact[enquiry_type]" id="RFQContact_enquiry_type" value="{{ $_cat }}" class="form-control" placeholder="General Enquiry" required="">
                                                    @endif
                                                </div>
						<div class="question mt-3">
							<div>
								<button class="mcb-btn mcb-trans mcb-btn-orange minus-margin-for-rfq">SUBMIT
									REQUEST</button>
							</div>
							<!-- contact-inputs -->
						</div>
                                                @csrf
						<!-- question -->
                                            </form>
					</div>
				</div>
				<!-- rfq-form -->

                  </div>
                  
			<!-- col -->

		<div class="col-lg-8 res-paddingtop-30">
                        <div class="row">
                            
                    @if($mainCatText == "")   
                    <div class="col-lg-12 col-md-12 ts-service-content">
                        <h2 class="section-title">
                           <span>{{ $category->translate(app()->getLocale())->name }}</span>
                        </h2>
                    </div>
                    
                   @endif 

                 @foreach($category->product as $product)

                 @php
                  $img=$product->thumbnail('cropped');

                    if(app()->getLocale()=="ar")
                    {
                        $product=$product->translate('ar');
                    }
                 @endphp

                 @if($product->slug!="")
                 <div class="col-6 col-sm-4 col-md-3 product-padding-right-10-320 product-card">
                                        <a href="/products/{{$product->slug}}">
                            <div class="product-brand">
                                <!-- social-brand -->
                                <div class="product-img text-center">
                                    
                                       
                                        @if(!$img) 
                                        <img class="img-fluid blank-thumb" src="/assets/img/no-product-photo.jpg" alt="">
                                    @else
                                        <img class="img-fluid w-100" src="{{ Voyager::image( $img ) }}" alt="" >
                                        
                                        <!--<img src="/assets/img/skf-product.png" alt="" class="w-50">-->
                                    @endif
                                </div>
                                <!-- product-img -->
                                <div class="truck product-wheel">
                                        <h6 class="mcb-h6">{{$product->title}}</h6>
                                        <p class="p-14 m-0">View Product <span class="ml-2"><i
                                                                class="fas fa-long-arrow-alt-right"></i></span></p>
                                </div>
                        </div>
                        <!-- product-brand -->
                    </a>
                    <div class="social-brand">
                            <ul>
                                    <li><a href="#"  class="m-dark mcbModalProducts" data-toggle="modal" data-target="#mcbModal"><i class="far fa-comments"></i></a></li>
                                    <li><a class="m-dark" target="_blank" href=https://api.whatsapp.com/send?phone={{ trim(setting('contact.whatsapp'),'+') }}&text=Good%20day%20Mineral%20Circles%20Bearings%20Team%21%20Please%20contact%20me%20as%20soon%20as%20possible%20as%20{{rawurlencode('I want to know about '.$product->title.'.') }}&source=&data=" class="readmore contact"><i class="fab fa-whatsapp"></i></a></li>
                                    <!--<li><a href="#" class="m-dark"><i class="fab fa-whatsapp"></i></a></li>-->
                            </ul>
                    </div>
                </div>
                 
                    @endif
               <!-- Col end -->
               @endforeach

</div>
                    </div>
                

            </div>
            <!-- Row End -->
           
            @endif
            
            @endforeach
           {{-- count($category->product)--}}
           @if( !isset($category) or count($category->product) == 0 ) <p>No Items Available.</p> @endif
         </div>
         <!-- Service Container -->
      </div>
      <!-- Container end -->
   </section>



</div>

@stop
