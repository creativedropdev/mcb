@extends('layout.default')

@section('content')

@php
if(app()->getLocale()=="ar")
{

    $setting_site="site-arabic.";


}
else
{
    $setting_site="site.";

}
$img = "";

$productVideos = explode( ",", $product->product_videos); 
$images = json_decode($product->product_images);

@endphp



{!!$schema!!}
<div class="mcb-main-container">
<section class="brands-skf section-bg-grey section-padtop-50 section-padbottom-50">
	<div class="mcb-container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="brand-image">
                                    @if(!$product->image) 
<!--                                        <img class="img-fluid" src="{{ Voyager::image( $img ) }}" alt="">-->
                                        {{ "NO IMAGE" }}
                                    @else
                                        <img src="{{ Voyager::image( $product->image) }}" alt="" class="single-product-image img-fluid w-100">
                                        
                                        <!--<img src="/assets/img/skf-product.png" alt="" class="w-50">-->
                                    @endif
                                        @if(app()->getLocale()=="en")


                      <!--<img src="{{ Voyager::image( $product->thumbnail('cropped')) }}" alt="{{ $product->title }}" class="single-product-image img-fluid">-->
                        @else

                        @php

                        @endphp
                                              

   @endif
				</div>
                    
                    <div class="rfq-section desktop-rfq">
                        <h3 class="mcb-h3 text-white">Request For Quotation</h3>
                        <div class="rfq-form">
                            <form class="contactMe" action="/request-for-quotation" method="POST" enctype="multipart/form-data">

                                <div class="contact-inputs mt-3">
                                        <p class="font-weight-bold p-14 mb-1 text-white">Contact Person*</p>
                                        <input name="RFQContact[full_name]" id="RFQContact_name" type="text" class="form-control" required="">
                                </div>

                                <div class="contact-inputs mt-3">
                                        <p class="font-weight-bold p-14 mb-1 text-white">Email*</p>
                                        <input name="RFQContact[email]" id="RFQContact_email" type="text" class="form-control" required="">
                                </div>

                                <div class="contact-inputs mt-3">
                                        <p class="font-weight-bold p-14 mb-1 text-white">Phone</p>
                                        <input name="RFQContact[phone]" id="RFQContact_phone" type="text" class="form-control">
                                </div>

                                <div class="contact-inputs mt-3">
                                        <p class="font-weight-bold p-14 mb-1 text-white">Company Name</p>
                                        <input name="RFQContact[company]" id="RFQContact_company" type="text" class="form-control">
                                </div>

                                <div class="mt-3">
                                        <div class="contact-inputs enquiry message-box">
                                                <p class="font-weight-bold p-14 mb-1 text-white">Message *</p>
                                                <textarea name="RFQContact[message_text]"
                                                          class="form-control w-100" style="height: 100px;" required=""></textarea>
                                        </div>
                                        <!-- contact-inputs -->
                                </div>


<!--						<div class="contact-inputs mt-3">
                                        <input id="file" type="file" name="filename" class="form-control h-100">
                                </div>-->

<!--						<div class="contact-inputs rfq-captcha section-padtop-15">
                                        <div class="g-recaptcha" data-sitekey="6LcmQc0ZAAAAAEDSVqw-cF5NtfC3FDLx2n2RUdgE"></div>
                                </div>-->
                                <!-- contact-inputs -->

                                <div class="form-group">
                                            <input type="hidden" name="RFQContact[enquiry_type]" id="RFQContact_enquiry_type"
                                                   value="{{$product->title}}" class="form-control" placeholder="General Enquiry" required="">
                                        </div>
                                <div class="question mt-3">
                                        <div>@csrf
                                                <button class="mcb-btn mcb-trans mcb-btn-orange minus-margin-for-rfq">SUBMIT
                                                        REQUEST</button>
                                        </div>
                                        <!-- contact-inputs -->
                                </div>
                               						<!-- question -->
                            </form>
                        </div>
                </div>
                </div>
                <div class="col-lg-8 product-content" >
                    <h3>{{$product->title}}</h3>
                    
                    @if($product->video){
                    <iframe width="340" height="315" src="{{$product->video}}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    @endif
					<h3>Applications</h3>
					@if(count($product->productApplications)>0)
						@foreach($product->productApplications as $app)
							{{$app->title}}
						@endforeach
					@endif	
					<h3>Advantages</h3>
					@if(count($product->productAdvantages)>0)
						@foreach($product->productAdvantages as $adv)
							{{$adv->title}}
						@endforeach
					@endif	
					
					<h3>Addons</h3>
					@if(count($product->productAddons)>0)
						@foreach($product->productAddons as $addon)
							{{$addon->title}}
						@endforeach
					@endif	
					
					<h3>Videos</h3>
					@if(count($productVideos)>0)
						@foreach($productVideos as $video)
							{{$video}}<br />
						@endforeach
					@endif
					<h3>Images</h3>
					@if(is_array($images))
						@if(count($images)>0)
						@foreach($images as $image)
						<img src="{{ Voyager::image($image) }}" />
						@endforeach
						@endif
					@endif
					
					
					{!! $product->description !!}
                    <section class="clients brand-clients section-bg-grey section-padtop-50 section-padbottom-50">

		<div class="row">
		       <div class="col-md-12">
                            <h2 class="mcb-h2 section-padbottom-30 m-0"><strong>{{ setting($setting_site.'available_brands') }}</strong></h2>
                     </div>
              </div>
              <div class="row carousel-row">
              <div class="col-md-12">
	        <div class="product-slider product-inner-slider">
	            @foreach($product->brand as $brand)
	        	<div class="col product-padding-left-10-320 product-padding-right-10-320">
              		<a href="/{{app()->getLocale()}}/brands/{{$brand->slug}}">
                                   <div class="product-brand">
                                   <!-- social-brand -->
                                   <div class="product-img text-center section-padtop-30 section-padbottom-30">
                                          <!--<img src="assets/img/skf-product.png" alt="" class="w-50">-->
                                          @if(!$brand->image) 
                    <!--                                        <img class="img-fluid" src="{{ Voyager::image( $img ) }}" alt="">-->
                                                            {{ "NO IMAGE" }}
                                                        @else
                                                            <img src="{{ Voyager::image( $brand->image) }}" alt="{{ $brand->title }}" class="single-product-image img-fluid">
                                                            
                                                            <!--<img src="/assets/img/skf-product.png" alt="" class="w-50">-->
                                                        @endif
                                   </div>
                                   <!-- product-img -->
                                   <div class="truck product-wheel">
                                          <h6 class="mcb-h6">{{$brand->title}}</h6>
                                          <p class="p-14 m-0">View Product <span class="ml-2"><i class="fas fa-long-arrow-alt-right"></i></span></p>
                                   </div>
                            </div>
                            <!-- product-brand -->
                            </a>
                            <div class="social-brand">
                                   <ul>
                                                              <li><a href="#" class="m-dark mcbModalProducts" data-toggle="modal" data-target="#mcbModal"><i class="far fa-comments"></i></a></li>
                                                              <li>
                                                                  <a class="m-dark" target="_blank" href=https://api.whatsapp.com/send?phone={{ trim(setting('contact.whatsapp'),'+') }}&text=Good%20day%20Mineral%20Circles%20Bearings%20Team%21%20Please%20contact%20me%20as%20soon%20as%20possible%20as%20{{rawurlencode('I want to know about '.$brand->title.'.') }}&source=&data=" class="readmore contact" ><i class="fab fa-whatsapp"></i></a></li>
                                                       </ul>
                            </div>
              	</div>
              	@endforeach

                     

	        </div>
	            
	        </div> <!-- col-md-12 -->
		</div>
	
</section>
<!-- clients -->
                    
                </div>
            </div>
@if(count($product->brand)>0)


@endif

</div>
</section>
</div>

<section class="section-bg-grey section-padbottom-50 touch-device-rfq">
	<div class="mcb-container">
		<div class="row">
			<div class="col-12">
				<div class="rfq-section">
					<h3 class="mcb-h3 text-white">Request For Quotation</h3>
					<div class="rfq-form">
                            <form class="contactMe" action="/request-for-quotation" method="POST" enctype="multipart/form-data">

                                <div class="contact-inputs mt-3">
                                        <p class="font-weight-bold p-14 mb-1 text-white">Contact Person*</p>
                                        <input name="RFQContact[full_name]" id="RFQContact_name" type="text" class="form-control" required="">
                                </div>

                                <div class="contact-inputs mt-3">
                                        <p class="font-weight-bold p-14 mb-1 text-white">Email*</p>
                                        <input name="RFQContact[email]" id="RFQContact_email" type="text" class="form-control" required="">
                                </div>

                                <div class="contact-inputs mt-3">
                                        <p class="font-weight-bold p-14 mb-1 text-white">Phone</p>
                                        <input name="RFQContact[phone]" id="RFQContact_phone" type="text" class="form-control">
                                </div>

                                <div class="contact-inputs mt-3">
                                        <p class="font-weight-bold p-14 mb-1 text-white">Company Name</p>
                                        <input name="RFQContact[company]" id="RFQContact_company" type="text" class="form-control">
                                </div>

                                <div class="mt-3">
                                        <div class="contact-inputs enquiry message-box">
                                                <p class="font-weight-bold p-14 mb-1 text-white">Message *</p>
                                                <textarea name="RFQContact[message_text]"
                                                          class="form-control w-100" style="height: 100px;" required=""></textarea>
                                        </div>
                                        <!-- contact-inputs -->
                                </div>


<!--						<div class="contact-inputs mt-3">
                                        <input id="file" type="file" name="filename" class="form-control h-100">
                                </div>-->

<!--						<div class="contact-inputs rfq-captcha section-padtop-15">
                                        <div class="g-recaptcha" data-sitekey="6LcmQc0ZAAAAAEDSVqw-cF5NtfC3FDLx2n2RUdgE"></div>
                                </div>-->
                                <!-- contact-inputs -->

                                <div class="form-group">
                                            <input type="hidden" name="RFQContact[enquiry_type]" id="RFQContact_enquiry_type"
                                                   value="{{$product->title}}" class="form-control" placeholder="General Enquiry" required="">
                                        </div>
                                <div class="question mt-3">
                                        <div>@csrf
                                                <button class="mcb-btn mcb-trans mcb-btn-orange minus-margin-for-rfq">SUBMIT
                                                        REQUEST</button>
                                        </div>
                                        <!-- contact-inputs -->
                                </div>
                               						<!-- question -->
                            </form>
                        </div>
				</div>
				<!-- rfq-form -->
			</div>
		</div>
	</div>
</section>            

@stop
