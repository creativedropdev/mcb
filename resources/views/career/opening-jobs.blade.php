@extends('layout.default')

@section('content')
@php

if(app()->getLocale()=="ar")
{
    $title= setting('menu-arabic.current-openings');
    $setting_site="site-arabic.";
    $career_content = setting($setting_site.'career_content-ar');


}
else
{   $title= "Current Job Openings";
    $setting_site="site.";
    $career_content = setting($setting_site.'career_content');

}

@endphp
<section class="mcb-slider current-opening-banner">
	<h1 class="mcb-h1">{{ $title }}</h1>
</section>
@include('Front.banner')


<section class="work section-bg-grey section-padtop-50 section-padbottom-50">
	<div class="mcb-container">
		<div class="row intern-head">
			<div class="col-md-10 m-auto">
				<div class="work-content text-center">
					<h2 class="mcb-h2 m-0">{{ $title }}</h2>
					<p class="mt-3 mb-0">{!! $career_content !!}</p>
				</div>
			</div>
		</div>
	</div>
	<!-- contianer -->
</section>

<section class="marketing section-bg-grey section-padbottom-50">
	<div class="mcb-container">
		<div class="row section-padbottom-15">
			<div class="col-lg-4">
				<div class="current-video res-paddingbottom-30">
					<iframe src="https://www.youtube.com/embed/WWBpRAF6_5I" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" width="560" height="315" frameborder="0"></iframe>
				</div>
			</div>
			<div class="col-lg-8">
                            @if(count($jobs)>0)
                                @foreach($jobs as $index)

				<div class="vacancy mb-3">
					<h4 class="m-0 text-white mb-blue">{{$index->title}}</h4>
					<ul class="bg-white">
						<li>
                                                    <p class="p-14" >Location: <span>{{$index->location}}</span></p>
							<p class="p-14">Commitment: {{$index->availability}}</p>
							<p class="p-14">No. of Openings: {{$index->no_of_openings}}</p>
							<p class="p-14">Posted: {{$index->created_at}}</p>
                                                        
						</li>
					</ul>
					<a href="/{{app()->getLocale()}}/career/{{$index->slug}}" target="_blank" class="btn mcb-btn mcb-btn-orange">APPLY NOW</a>
				</div>
                                
                                @endforeach
                            @else
                             <div class="alert alert-info" role="alert"></div>
                            @endif
			</div>
		</div>
	</div>
</section>

@stop


