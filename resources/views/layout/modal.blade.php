<!-- Modal -->
<div class="modal fade" id="mcbModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title" id="exampleModalLabel">Ask a Question</h2>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="contactMe" action="/contact" method="POST" enctype="multipart/form-data">
          <div class="contact-inputs">
            <div class="form-group">
              <input type="text" name="Contact[full_name]" class="form-control" placeholder="Name*" required>
            </div>
            <div class="form-group">
              <input name="Contact[phone]"  type="tel" class="form-control input-numbers">
            </div>
            <div class="form-group">
              <input type="text" name="Contact[email]" class="form-control" placeholder="Email*" required>
            </div>
            <div class="form-group">
                @if(isset($product->title))
                <input type="hidden" name="Contact[enquiry_type]" id="enquiry_type" value="{!! $product->title !!}" class="form-control" placeholder="General Enquiry" required="">
                @else
                <input type="hidden" name="Contact[enquiry_type]" id="enquiry_type" value="{!! Request::url() !!}" class="form-control" placeholder="General Enquiry" required="">
                @endif
            </div>
              
            <div class="form-group">
                <textarea class="form-control" name="Contact[message_text]" id="message_text" rows="5" placeholder="Message..." required=""></textarea>
            </div>
              @csrf
            <button type="submit" class="mcb-btn mcb-trans">SEND MESSAGE</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- mcb-modal -->

<!-- Modal -->
<div class="modal fade" id="lubricationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title" id="exampleModalLabel">Lubrication Basics</h2>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="contact-inputs">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="First Name" required>
            </div>
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Family Name*" required>
            </div>
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Company Name*" required>
            </div>
            <div class="form-group">
              <input name="phone" type="tel" class="form-control input-numbers">
            </div>
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Country*" required>
            </div>
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Email Address*" required>
            </div>
            <button type="button" class="mcb-btn mcb-trans">SEND MESSAGE</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- mcb-modal -->

<!-- Modal -->
<div class="modal fade" id="toolsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title" id="exampleModalLabel">Tools for Bearings & Lubrication</h2>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="contact-inputs">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Name*" required>
            </div>
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Family Name*" required>
            </div>
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Company Name*" required>
            </div>
            <div class="form-group">
              <input name="phone" type="tel" class="form-control input-numbers">
            </div>
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Country*" required>
            </div>
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Email Address*" required>
            </div>
            <button type="button" class="mcb-btn mcb-trans">SEND MESSAGE</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- mcb-modal -->


<!-- Modal -->
<div class="modal fade" id="subscribeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title" id="exampleModalLabel">Subscribe Newsletter:</h2>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="contact-inputs">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Name*" required>
            </div>
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Family Name*" required>
            </div>
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Company Name*" required>
            </div>
            <div class="form-group">
              <input name="phone" type="tel" class="form-control input-numbers">
            </div>
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Country*" required>
            </div>
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Email Address*" required>
            </div>
            <button type="button" class="mcb-btn mcb-trans">SUBSCRIBE NOW</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- mcb-modal -->

<!-- Modal -->
<div class="modal fade" id="emailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title" id="exampleModalLabel">Drop a Message</h2>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="contact-inputs">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Name*" required>
            </div>
            <div class="form-group">
              <input type="email" class="form-control" placeholder="Email*" required>
            </div>
            <div class="form-group">
              <input name="phone" type="tel" class="form-control input-numbers">
            </div>
            <div class="form-group">
              <textarea class="form-control" rows="5" placeholder="Message..."></textarea>
            </div>
            <button type="button" class="mcb-btn mcb-trans">SEND MESSAGE</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- mcb-modal -->
<!-- 13 oct video -->
<div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog video-dialog" role="document">
    <div class="modal-content">

      
      <div class="modal-body video-body">

       <button type="button" class="close video-close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>        
        <!-- 16:9 aspect ratio -->
        <div class="embed-responsive embed-responsive-16by9">
          <iframe class="embed-responsive-item" src="" id="video"  allowscriptaccess="always" allow="autoplay"></iframe>
        </div>
        
        
      </div>

    </div>
  </div>
</div>