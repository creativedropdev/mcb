<!doctype html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!-- Basic Page Needs =====================================-->


    <!-- Mobile Specific Metas ================================-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Site Title- -->
    <title> {{ isset($common['title']) ? $common['title'] : setting('site.title')}}</title>

    @if(isset($common))


    {!! (isset($common['keywords']))?(($common['keywords']!="") ? "
    <meta name='keywords' content='".$common['keywords']."' />" : ""):""!!}
    {!! (isset($common['description']))?(($common['description']!="") ? "
    <meta name='description' content='".$common['description']."' />" : ""):""!!}
    {!! (isset($common['canonical']))?(($common['canonical']!="") ? "
    <link rel='canonical' href='".$common['canonical']."' />" : ""):""!!}
    @endif
    <!-- CSS
    ==================================================== -->

    <!-- Font Awesome -->
    <link rel="stylesheet" href="/front/css/font-awesome.min.css">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="/front/css/animate.css">
    <link rel="stylesheet" href="/css/features-block.css">
    <link rel="stylesheet" href="/css/timeline.css">

    <!-- IcoFonts -->
    <link rel="stylesheet" href="/front/css/icofonts.css">
    <link rel="stylesheet" href="/front/css/automobil_icon.css">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="/front/css/bootstrap.min.css">

    <!-- Owl Carousel -->
    <link rel="stylesheet" href="/front/css/owlcarousel.min.css">

    <!-- Contactme -->
    <link rel="stylesheet" href="/front/css/contactme/bootstrap-datepicker.standalone.min.css">
    <link rel="stylesheet" href="/front/css/contactme/contactme-1.3.css">
    <link rel="stylesheet" href="/front/css/contactme/jquery.timepicker.css">
    <link rel="stylesheet" href="/front/css/contactme/select2.min.css">

    <!-- Style -->
    <!-- <link rel="stylesheet" href="/front/css/style.css"> -->

    <!-- Responsive -->
    <!-- <link rel="stylesheet" href="/front/css/responsive.css"> -->

    <script type='text/javascript'
        src='//platform-api.sharethis.com/js/sharethis.js#property=5cbd3056f3971d0012e24693&product=inline-share-buttons'
        async='async'></script>
    <!---New CSS---->
    <link rel="stylesheet" href="/new/css/all.css">

		<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="/new/css/bootstrap.min.css">

	<link rel="stylesheet" href="/new/css/slick.css">
	<link rel="stylesheet" href="/new/css/slick-theme.css">

	<link rel="stylesheet" href="/new/css/theme.css">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>

<body>
    <header>
		<div class="topbar">
			<div class="mcb-container">
				<nav class="navbar navbar-expand-lg navbar-light bg-white">
				  <a class="navbar-brand" href="index.php"><img src="/new/assets/img/brand-logo.png" alt="brand-logo" width="75%"></a>

				  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				    <span class="navbar-toggler-icon"></span>
				  </button>

				  <div class="collapse navbar-collapse" id="navbarSupportedContent">
				    <ul class="navbar-nav ml-auto top-nav pr-3 mr-3">
				      <li class="nav-item">
				        <a class="nav-link" href="#">{{setting('site.email')}}</a>
				      </li>
				      <li class="nav-item">
				        <a class="nav-link" href="#">{{setting('site.phone')}}</a>
				      </li>
				    </ul>
				    <form class="form-inline my-2 my-lg-0">
				      <input class="form-control mr-sm-2 mcb-trans" type="search" placeholder="Search" aria-label="Search">
				      <i class="fas fa-search"></i>
				    </form>
				    <button class="btn my-2 my-sm-0 mcb-btn mcb-btn-orange" data-toggle="modal" data-target="#mcbModal">QUOTATION</button>
				  </div>
				</nav>
			</div>
			<!-- mcb-container -->
		</div>
		<!-- topbar -->


		<div class="menu">
			<div class="mcb-container">
				<nav class="navbar navbar-expand-lg navbar-light">

				  <div class="collapse navbar-collapse" id="navbarSupportedContent">
				    <ul class="headermainmenu navbar-nav m-auto">

						<li class="nav-item dropdown">
					        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					          Products
					        </a>
					        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
					          <a class="dropdown-item" href="products.php">Automotive Bearings</a>
					          <a class="dropdown-item" href="products.php">Industrial Bearings</a>
					          <a class="dropdown-item" href="products.php">Grease For Service And Maintenance</a>
					          <a class="dropdown-item" href="products.php">Tools For Service And Maintenance</a>
					          <a class="dropdown-item" href="products.php">CV Joint</a>
					          <a class="dropdown-item" href="products.php">Univeral Joints</a>
					          <a class="dropdown-item" href="products.php">Oil Seal</a>
					        </div>
					    </li>

						<li class="nav-item dropdown">
					        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					          Brands
					        </a>
					        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
					          <a class="dropdown-item" href="brands.php">SKF</a>
					          <a class="dropdown-item" href="press-release.php">Mineral Circles Bearings</a>
					          <a class="dropdown-item" href="press-release.php">NTN Corporate Profile</a>
					          <a class="dropdown-item" href="press-release.php">Forms</a>
					          <a class="dropdown-item" href="press-release.php">Brochures</a>
					          <a class="dropdown-item" href="press-release.php">Flyers</a>
					          <a class="dropdown-item" href="press-release.php">Catalogs</a>
					          <a class="dropdown-item" href="press-release.php">Material Safety Data Sheet</a>
					          <a class="dropdown-item" href="press-release.php">MBS</a>
					          <a class="dropdown-item" href="press-release.php">MCB</a>
					          <a class="dropdown-item" href="press-release.php">ILJIN</a>
					          <a class="dropdown-item" href="press-release.php">KBC</a>
					        </div>
					      </li>

						<li class="nav-item dropdown">
					        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					          Company
					        </a>
					        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
					          <a class="dropdown-item" href="about-us.php">About Us</a>
					          <a class="dropdown-item" href="the-goal.php">The Goal</a>
					          <a class="dropdown-item" href="core-values.php">Core Values</a>
					          <a class="dropdown-item" href="infrastructure.php">Infrastructure</a>
					          <a class="dropdown-item" href="leadership.php">Leadership</a>
					          <a class="dropdown-item" href="milestones.php">Milestones</a>
					        </div>
					    </li>

						<li class="nav-item dropdown">
					        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					          Careers
					        </a>
					        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
					          <a class="dropdown-item" href="our-team.php">Our Team</a>
					          <a class="dropdown-item" href="current-opening.php">Current Openings</a>
					        </div>
					    </li>

					  	<li class="nav-item">
				        	<a class="nav-link" href="blog.php">Blog</a>
				     	</li>

						<li class="nav-item">
				        	<a class="nav-link" href="press-release.php">Press Release</a>
				     	</li>

				    	<li class="nav-item dropdown">
					        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					          Technical Tips
					        </a>
					        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
					          <a class="dropdown-item" href="#">Action</a>
					          <a class="dropdown-item" href="#">Another action</a>
					        </div>
				    	</li>

				    	<li class="nav-item">
				        	<a class="nav-link" href="contact-us.php">Contact Us</a>
				     	</li>

				    </ul>
				  </div>
				</nav>
			</div>
			<!-- mcb-container -->
		</div>
		<!-- menu -->
    </header>

    @yield('content')

    <footer>
		<section class="subscribe section-padtop-30 section-padbottom-30 mb-orange">
			<div class="mcb-container">
				<div class="row">
					<div class="offset-md-1 col-md-5">
						<div class="subscribe-head">
							<p class="mcb-h2">Subscribe for Newsletter</p>
						</div>
					</div>
					<div class="col-md-5">
						<div class="newsletter">
							<input type="text" placeholder="Enter your email">
							<div class="sub-icon">
								<a href="#" class="text-white"><i class="fas fa-paper-plane"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- subscribe -->

		<section class="footer-links">
			<div class="mcb-container">
				<div class="row">
					<div class="col-md-2 footer">
						<div class="foot-1 section-padtop-50 section-padbottom-50">
							<h5 class="mcb-h5">Company</h5>
							<ul>
								<li><a href="#">About Us</a></li>
								<li><a href="#">The Goal</a></li>
								<li><a href="#">Core Values</a></li>
								<li><a href="#">Leadership</a></li>
								<li><a href="#">Infrastructure</a></li>
								<li><a href="#">Milestone</a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-2 footer">
						<div class="foot-1 section-padtop-50 section-padbottom-50">
							<h5 class="mcb-h5">Products</h5>
							<ul>
								<li><a href="#">About Us</a></li>
								<li><a href="#">The Goal</a></li>
								<li><a href="#">Core Values</a></li>
								<li><a href="#">Leadership</a></li>
								<li><a href="#">Infrastructure</a></li>
								<li><a href="#">Milestone</a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-2 footer">
						<div class="foot-1 section-padtop-50 section-padbottom-50">
							<h5 class="mcb-h5">Automotive</h5>
							<ul>
								<li><a href="#">Automotive bearings</a></li>
								<li><a href="#">Truck wheel applications</a></li>
								<li><a href="#">Wheel bearing kit</a></li>
								<li><a href="#">Lubricatin solution</a></li>
								<li><a href="#">and more</a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-2 footer">
						<div class="foot-1 section-padtop-50 section-padbottom-50">
							<h5 class="mcb-h5">Industrial</h5>
							<ul>
								<li><a href="#">Industrial Chain</a></li>
								<li><a href="#">Tools for service and</a></li>
								<li><a href="#">maintenance</a></li>
								<li><a href="#">Industrial bearings</a></li>
								<li><a href="#">Lubrication solution</a></li>
								<li><a href="#">and more</a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-2 footer">
						<div class="foot-1 section-padtop-50 section-padbottom-50">
							<h5 class="mcb-h5">Career</h5>
							<ul>
								<li><a href="#">Job Openings</a></li>
								<li><a href="#">About Us</a></li>
								<li><a href="#">The Goal</a></li>
								<li><a href="#">Core Values</a></li>
								<li><a href="#">Leadership</a></li>
								<li><a href="#">Infrastructure</a></li>
								<li><a href="#">Milestone</a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-2">
						<div class="foot-1 section-padtop-50 section-padbottom-50">
							<h5 class="mcb-h5">Media Publications</h5>
							<ul>
								<li><a href="#">Blog</a></li>
								<li><a href="#">Press Release</a></li>
								<li><a href="#">Technical Tips.</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<!-- mcb-container -->
		</section>

		<section class="foot-end section-padbottom-30 section-padtop-30">
			<div class="mcb-container">
				<div class="row">
					<div class="col-md-2">
						<div class="foot-logo">
							<img src="/new/assets/img/brand-logo.png" alt="">
						</div>

					</div>
					<div class="col-md-5">
						<div class="corner">
							<p>{{setting('contact.address')}}</p>
							<ul>
								<li><a href="#">Terms & Condition</a></li>
								<li>|</li>
								<li><a href="#">Privacy Policy</a></li>
								<li>|</li>
								<li><a href="#">Disclaimer</a></li>
							</ul>
						</div>
					</div>

					<div class="offset-md-1 col-md-4">
						<div class="foot-links">
							<ul>
								<li><a href="{{setting('contact.twitter')}}"><i class="fab fa-twitter"></i></a></li>
								<li><a href="{{setting('contact.youtube')}}"><i class="fab fa-youtube"></i></a></li>
								<li><a href="{{setting('contact.fb')}}"><i class="fab fa-facebook-f"></i></a></li>
								<li><a href="{{setting('contact.instagram')}}"><i class="fab fa-instagram"></i></a></li>
							</ul>
							<p class="mb-0">&copy; 2020. Mineral Circles Bearings. All Rights Reserved.</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- foot-end -->
	</footer>


<div id="rfqModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
          <h4 class="modal-title">Request for Quotation</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>

      </div>
       <form class="contactMe" action="/request-for-quotation" method="POST"
                            enctype="multipart/form-data">
      <div class="modal-body">






       <div class="col-12 mt30">

                            <!-- service-form -->
                            <div class="service-form">
                                <div class="row">

                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12  ">
                                        <div class="form-group service-form-group">
                                            <label class="control-label sr-only" for="name"></label>
                                            <input id="name" name="name" type="text" placeholder="Name"
                                                class="form-control" required>
                                            <div class="form-icon"><i class="fa fa-user"></i></div>
                                        </div>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                        <div class="form-group service-form-group">
                                            <label class="control-label sr-only" for="email"></label>
                                            <input name="email" id="email" type="email" placeholder="Email"
                                                class="form-control" required>
                                            <div class="form-icon"><i class="fa fa-envelope"></i></div>
                                        </div>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                        <div class="form-group service-form-group">
                                            <label class="control-label sr-only" for="phone"></label>
                                            <input name="phone" id="phone" type="text" placeholder="Phone"
                                                class="form-control" required>
                                            <div class="form-icon"><i class="fa fa-phone"></i></div>
                                        </div>
                                    </div>

 <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                        <div class="form-group service-form-group">
                                            <label class="control-label sr-only" for="company"></label>
                                            <input name="company" id="company" type="text" placeholder="Company"
                                                class="form-control" required>
                                            <div class="form-icon"><i class="fa fa-building"></i></div>
                                        </div>
                                    </div>

                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                        <div class="form-group service-form-group">
                                            Select File:
                                            <label class="control-label sr-only" for="file">asd</label>
                                            <input id="file" type="file" placeholder="Quotation File"
                                                class="form-control">
                                            <div class="form-icon"><i class="fa fa-link"></i></div>
                                        </div>
                                    </div>

                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                        <div class="form-group">
                                            <label class="control-label sr-only" for="textarea"></label>
                                            <textarea class="form-control" id="textarea" name="textarea" rows="3"
                                                placeholder="Messages"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                        <label> <span class="checkbox"><input type="checkbox" name="acceptanceGdpr"
                                                    value="1" aria-invalid="false" id="acceptance"></span> <span
                                                class="label">By using this form you agree with the storage and handling
                                                of your data by this website </span></label>


                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                        <div class="form-group"> @csrf


                                            {!! Captcha::display() !!}

                                        </div>

                                    </div>


                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <div class="msg"></div>
                                        </div>
                                    </div>




                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">

                                    </div>
                                </div>
                            </div>
                        <!-- /.service-form -->
                    </div>








      </div>
      <div class="modal-footer">


                                            <input style="margin-right:0px;" type="submit" name="submit" id="submit"
                                                class="btn btn-red btn-block mb101 form-control" value="Submit" />

                                        </div>
                                                                </form>



    </div>

  </div>
</div>


    <!-- javaScript Files
   =============================================================================-->

    <!-- initialize jQuery Library -->
    <script src="/front/js/jquery.min.js"></script>
    <!-- Popper JS -->
    <script src="/front/js/popper.min.js"></script>
    <!-- Bootstrap jQuery -->
    <script src="/front/js/bootstrap.min.js"></script>
    <!-- Owl Carousel -->
    <script src="/front/js/owl-carousel.2.3.0.min.js"></script>
    <!-- START js copy section -->
    <script src="/front/js/contactme/bootstrap-datepicker.min.js"></script>
    <script src="/front/js/contactme/bootstrap-datepicker-lang/en.js"></script>
    <script src="/front/js/contactme/jquery.timepicker.min.js"></script>
    <script src="/front/js/contactme/select2.full.min.js"></script>
    <script src="/front/js/contactme/select2-lang/en.js"></script>
    <!--[if lt IE 9]><script src="contactme/js/EQCSS-polyfills-1.7.0.min.js"></script><![endif]-->
    <script src="/front/js/contactme/EQCSS-1.7.0.min.js"></script>
    <script src="/front/js/contactme/contactme-config.js"></script>
    <script src="/front/js/contactme/contactme-1.4.js"></script>
    <script src="/js/bootstrap-select-country.min.js"></script>
    <!-- To enable Google reCAPTCHA, uncomment the next line: -->
    <!-- <script src="https://www.google.com/recaptcha/api.js?onload=initRecaptchas&render=explicit" async defer></script> -->
    <!-- END js copy section -->
    <!-- MCB JS Custom -->


    <script src="/front/js/jquery-modal-video.min.js"></script>
    <link rel="stylesheet" href="/front/css/modal-video.min.css" />
    <script src="/front/js/main.js"></script>
    <script>
    $('#search').on('keyup', function() {

        $value = $(this).val();
        $.ajax({
            type: 'get',
            url: '{{URL::to('search')}}',
            data: {
                'search': $value
            },
            success: function(data) {
                // $(".searchResult").slideDown();
                // $('#searchResultsContainer');

                if (data == "") {
                    $("#mainSearchResult").hide();
                } else {
                    $('#searchResultsContainer').html(data);
                    $("#mainSearchResult").show();
                }

            }
        });
    })


    $("#closeSearch").click(function() {

        $("#mainSearchResult").slideUp();
    });
    $('body').click(function() {


        // $(".searchResult").slideUp();
    })

    $(".js-modal-btn").modalVideo();
    </script>
    <script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'csrftoken': '{{ csrf_token() }}'
        }
    });
    </script>
    <!-- livezilla.net PLACE SOMEWHERE IN BODY -->
    <script type="text/javascript" id="a977835ce83c3ad285aebef5309a4ac6"
        src="//mcb.workspace.destring.com/livezilla/script.php?id=a977835ce83c3ad285aebef5309a4ac6" defer></script>
    <!-- livezilla.net PLACE SOMEWHERE IN BODY -->


    <script>
    $(document).ready(function() {
        $("#t1,#t2,#t3").keypress(function(e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                $("#errmsg1,#errmsg2,#errmsg3,").html("Digits Only").show().fadeOut("slow");
                return false;
            }
        });

        var max_fields = 10; //maximum input boxes allowed
        var wrapper = $(".input_fields_wrap"); //Fields wrapper
        var wrapper2 = $("div.input_fields_wrap"); //Fields wrapper
        var add_button = $(".add_field_button"); //Add button ID

        var x = 1; //initlal text box count

        var max_fields2 = 10; //maximum input boxes allowed
        var wrapperb = $(".input_fields_wrap2"); //Fields wrapper
        var wrapperb2 = $("div.input_fields_wrap2"); //Fields wrapper
        var add_button2 = $(".add_field_button2"); //Add button ID

        var x2 = 1; //initlal text box count


        $(add_button).click(function(e) { //on add input button click

            e.preventDefault();


            if (x < max_fields) { //max input box allowed
                x++; //text box increment
                $(".input_fields_wrap").append('<div  id="' + x + '" class="row" style="margin:0 auto;"> \
                <div class="col-lg-1"> ' + x + '\
                </div>\
                <div class="col-lg-3">\
                  <div class="form-group">\
                                      \
                   <input type="text" class="form-control" name="items[]" id="items">\
                   </div>\
                </div>\
                  <div class="col-lg-3">\
                    <div class="form-group">\
                           \
                   <input type="text" class="form-control" name="brands[]" id="brands">\
                   </div>\
                </div>\
                  <div class="col-lg-3">\
                   <div class="form-group">\
                \
                       \
                   <input type="text" class="form-control" name="quantity[]" id="quantity">\
                   </div>\
                </div>\
                 <div class="col-lg-1">\
                   <a href="#" class="remove_field" id="' + x + '"><img src="https://mcb.ae/wp-content/themes/MCB/images/delete.png" border="0" /></a>\
                </div>      </div>'); //add input box
            }
        });


        // <div class="row"  style="margin:0 auto;"  id="'+ x + '"><div class="col-lg-8"><div class="form-group">'+ x + ') Year of Graduation<br/><input type="date" class="form-control" name="graduation[]"    ></div></div><div class="col-lg-8"><div class="form-group">School/University<br/><input type="text" class="form-control" name="school[]"    ></div></div><div class="col-lg-8"><div class="form-group">Inclusive Years<br/><input type="date" class="form-control" name="inclusive[]"     ></div></div><div class="col-lg-1 no-padding"><br/><a href="#" class="remove_field" id="'+ x + '"><img src="https://mcb.ae/wp-content/themes/MCB/images/delete.png" border="0" /></a></div></div>


        $(wrapper2).on("click", ".remove_field", function(e) { //user click on remove text

            var counts = document.getElementsByName('items[]');
            var cnt = counts.length;
            // alert(cnt);
            //  e.preventDefault();
            var xx = $(this).attr('id');



            $('div#' + xx).remove();
            x--;
            return false;

        });




        $(add_button2).click(function(e) { //on add input button click

            e.preventDefault();

            if (x2 < max_fields2) { //max input box allowed
                x2++; //text box increment
                $(".input_fields_wrap2").append('<div class="row"  style="margin:0 auto;"  id="' + x2 +
                    '"> <div class="col-lg-8"><div class="form-group">' + x2 +
                    ') Company<br/><input type="text" class="form-control" name="company[]"  ></div></div><div class="col-lg-8"><div class="form-group">Contacts<br/><input type="text" class="form-control" name="contacts[]"     ></div></div><div class="col-lg-8"><div class="form-group">Designation<br/><input type="text" class="form-control" name="designation[]"    ></div></div><div class="col-lg-8"><div class="form-group">Date<br/><input type="date" class="form-control" name="dates[]"     ></div></div><div class="col-lg-8"><div class="form-group">Last Salary<br/><input type="text" class="form-control" name="salary[]"     ></div></div><div class="col-lg-1 no-padding"><br/><a href="#" class="remove_field" id="' +
                    x2 +
                    '"><img src="https://mcb.ae/wp-content/themes/MCB/images/delete.png" border="0" /></a></div></div>'
                ); //add input box
            }
        });






        $(wrapper2).on("click", ".remove_field", function(e) { //user click on remove text
            var counts = document.getElementsByName('countss[]');
            var cnt = counts.length;
            //  e.preventDefault();
            var xx = $(this).attr('id');



            $('div#' + xx).remove();
            x--;
            return false;

        })



    });
    </script>

    <script type="text/javascript">
    $(document).ready(function(e) {

        $("#filterCatalogs").on('change keydown paste input', function(){


            let text = $(this).val().toLowerCase();

           $(".catalogItem").filter(function() {
            console.log($(this).find(".catalogTitle").text().toLowerCase().indexOf(text));
      $(this).toggle($(this).find(".catalogTitle").text().toLowerCase().indexOf(text) > -1)
    });



     $(".catalogCategory").filter(function() {

            //   console.log($(this).find(".catalogTitle").text());
            // $('').length

            console.log("len"+$(this).find('.catalogItem[style="display: none;"]').length);
            if($(this).find('.catalogItem[style=""]').length == 0)
            {
                $(this).hide();
            }
            else
            {
                $(this).show();
            }
    //   $(this).toggle($(this).find(".catalogItem:visible").length == 0)
    });




            // $(".catalogTitle").each(function(){

            //   $(this).hide();
            // });


        })

        $('.flip').hover(function() {
            $(this).find('.card').toggleClass('flipped');

        });


    });
    </script>






    <!-- Open Sans -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet" type="text/css" />

    <!-- Icons -->
    <link rel="stylesheet" type="text/css" media="screen"
        href="/youtube-video-player/packages/icons/css/icons.min.css" />

    <!-- Main Stylesheet -->
    <link rel="stylesheet" type="text/css" media="screen"
        href="/youtube-video-player/css/youtube-video-player.min.css" />

    <!-- jQuery -->

    <!-- Main Javascript -->
    <script type="text/javascript" src="/youtube-video-player/js/youtube-video-player.jquery.min.js"></script>

    <!-- Perfect Scrollbar -->
    <link rel="stylesheet" type="text/css" media="screen"
        href="/youtube-video-player/packages/perfect-scrollbar/perfect-scrollbar.css" />
    <script type="text/javascript" src="/youtube-video-player/packages/perfect-scrollbar/jquery.mousewheel.js"></script>
    <script type="text/javascript" src="/youtube-video-player/packages/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script type="text/javascript" src="/js/lightbox.js"></script>
    <!-- jQuery library -->
	<script src="/new/assets/js/jquery.min.js"></script>

	<!-- Popper JS -->
	<script src="/new/assets/js/popper.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="/new/assets/js/bootstrap.min.js"></script>

	<script>
	//Map


	      var myLatLng = { lat: 25.6920788, lng: 55.2641677 };
	    function initMap() {

	        // Create a new StyledMapType object, passing it an array of styles,
	        // and the name to be displayed on the map type control.
	        var styledMapType = new google.maps.StyledMapType(
	            [
    {
        "featureType": "all",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "saturation": 36
            },
            {
                "color": "#000000"
            },
            {
                "lightness": 40
            }
        ]
    },
    {
        "featureType": "all",
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#000000"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "featureType": "all",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 17
            },
            {
                "weight": 1.2
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative.country",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "administrative.country",
        "elementType": "geometry",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "administrative.country",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "administrative.province",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative.locality",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            },
            {
                "saturation": "-100"
            },
            {
                "lightness": "30"
            }
        ]
    },
    {
        "featureType": "administrative.neighborhood",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative.land_parcel",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            },
            {
                "gamma": "0.00"
            },
            {
                "lightness": "74"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#0a2360"
            },
            {
                "lightness": "-37"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "all",
        "stylers": [
            {
                "lightness": "3"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 21
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#0e204d"
            },
            {
                "lightness": "0"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 29
            },
            {
                "weight": 0.2
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#7d7c9b"
            },
            {
                "lightness": "43"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#0e204d"
            },
            {
                "lightness": "1"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#7d7c9b"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#0e204d"
            },
            {
                "lightness": "-1"
            },
            {
                "gamma": "1"
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "hue": "#ff0000"
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#7d7c9b"
            },
            {
                "lightness": "-31"
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#0e204d"
            },
            {
                "lightness": "-36"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#0e204d"
            },
            {
                "lightness": "0"
            },
            {
                "gamma": "1"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    }
],
	            { name: 'Styled Map' });


	        //UAE
	        var map = new google.maps.Map(document.getElementById('map'), {
	            zoom: 8,
	            center: { lat: 25.632210, lng: 53.912410 }
	        });


	        //Associate the styled map with the MapTypeId and set it to display.
	        map.mapTypes.set('styled_map', styledMapType);
	        map.setMapTypeId('styled_map');

	        pointers = [
	            {
	                "position": {
	                  lat: 25.395610,
	                  lng: 55.462454
	            },
	                "content": " <div id=\"content\"><div id=\"siteNotice\"></div><h1 class=\"mcb-h5\" id=\"firstHeadingMap\" class=\"firstHeadingMap\">Mineral Circles Bearings</h1><hr><h4 id=\"firstParaMap\" class=\"mcb-h6\" class=\"fisrtparaMap\">Ajman, United Arab Emirates</h4></div>"

	            }

	        ]
	        markers = [];
	        for(var i = 0; i < pointers.length; i++) {
	            let pointer = pointers[i];
	            let marker=  new google.maps.Marker({
	                position: pointer.position,
	                map: map,
	                title: 'Alhamad.ae'
	            })
	            marker.addListener('click',function() {
	                    new google.maps.InfoWindow({
	                        content: pointer.content
	                    }).open(map,marker)
	                })
	        }
	    }

	</script>
			<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRlwzku4Z02K2_FWQu_g-TVHRsP8OdKLM&callback=initMap"
    		async defer></script>
    		<script src="assets/js/slick.min.js"></script>

    		<script>
				$(document).ready(function(){
			      $('.multiple-items').slick({
					  infinite: true,
					  slidesToShow: 3,
					  slidesToScroll: 3
					});
			    });
    		</script>
    		<script src="assets/js/jquery.waypoints.min.js"></script>
    		<script src="assets/js/jquery.counterup.min.js"></script>
    		<script>
    			$('.count').counterUp({
				    delay: 10,
				    time: 1000
				});
    		</script>

    		<!-- press-release -->
    		<script>
		(function() {

		  $(".panel").on("show.bs.collapse hide.bs.collapse", function(e) {
		    if (e.type=='show'){
		      $(this).addClass('active');
		    }else{
		      $(this).removeClass('active');
		    }
		  });

		}).call(this);

	</script>

	<script>
		$(function(){
		    var currentURL = location.pathname;
		    $('.headermainmenu li a').each(function(){
		    	var currentAnchor = $(this);
		        if(currentURL.indexOf(currentAnchor.attr('href'))  !== -1){
		            currentAnchor.parents('li').addClass('active');
		        }
		    })
		})
	</script>


    <script>
    $(document).ready(function() {

        {!!(isset($common)) ? ((isset($common['slidersJs'])) ? ($common['slidersJs'] == "") ? "" : $common[
                'slidersJs'] : "") : ""!!}
    });

    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });
    </script>

<!-- This site is converting visitors into subscribers and customers with Rocketbots - https://rocketbots.io -->
<script src="https://app.rocketbots.io/facebook/chat/plugin/24343/435776213540217" async></script>
<!-- https://rocketbots.io/ -->






<script src="/telinput/js/intlTelInput.js"></script>
<script>
  var input = document.querySelector("#phone");
  window.intlTelInput(input, {
  initialCountry: "ae" });

</script>

</body>

</html>
