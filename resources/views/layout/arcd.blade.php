<!doctype html>
<html lang="ar">

<head>
    <!-- Basic Page Needs =====================================-->
    <meta charset="utf-8">

    <!-- Mobile Specific Metas ================================-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Site Title- -->
    <title> {{ isset($common['title']) ? $common['title'] : setting('site-arabic.title')}}</title>
    @if(isset($common))
    {!! (isset($common['keywords']))?(($common['keywords']!="") ? "<meta name='keywords' content='".$common['keywords']."' />" : ""):""!!}
    {!! (isset($common['description']))?(($common['description']!="") ? "<meta name='description' content='".$common['description']."' />" : ""):""!!}
    {!! (isset($common['canonical']))?(($common['canonical']!="") ? "<link rel='canonical' href='".$common['canonical']."' />" : ""):""!!}
    @endif
    <!-- CSS
    ==================================================== -->

    <!-- Font Awesome -->
    <link rel="stylesheet" href="/front/css/font-awesome.min.css">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="/front/css/animate.css">

    <!-- IcoFonts -->
    <link rel="stylesheet" href="/front/css/icofonts.css">
    <link rel="stylesheet" href="/front/css/automobil_icon.css">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="/front/css/bootstrap.min.css">

    <!-- Owl Carousel -->
    <link rel="stylesheet" href="/front/css/owlcarousel.min.css">

    <!-- Contactme -->
    <link rel="stylesheet" href="/front/css/contactme/bootstrap-datepicker.standalone.min.css">
    <link rel="stylesheet" href="/front/css/contactme/contactme-1.3.css">
    <link rel="stylesheet" href="/front/css/contactme/jquery.timepicker.css">
    <link rel="stylesheet" href="/front/css/contactme/select2.min.css">

    <!-- Style -->
    <link rel="stylesheet" href="/front/css/style.css">

    <!-- Responsive -->
    <link rel="stylesheet" href="/front/css/responsive.css">
     <link rel="stylesheet" href="/front/css/ar.css">
    <script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5cbd3056f3971d0012e24693&product=inline-share-buttons' async='async'></script>


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>

<body>
<div class="ts-top-bar-2 standard">
    <div class="container2">
        <div class="row">
            <div class="col-lg-4 col-md-4">
                <div class="top-bar-social-icon ml-auto">
                    <ul>
                        <li><a href="{{setting('contact.fb')}}"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="{{setting('contact.twitter')}}"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="{{setting('contact.youtube')}}"><i class="fa fa-youtube"></i></a></li>
                        <li><a href="{{setting('contact.linkedin')}}"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="{{setting('contact.instagram')}}"><i class="fa fa-instagram"></i></a></li>
                    </ul>
                </div> <!-- Social End -->
            </div>

            <div class="col-lg-8 col-md-8 text-right">
                <div class="top-bar-event">

                    <div>
                        <ul class="navbar-nav">
                            <li class="language dropdown">
                                @php

                                   if(!isset($layoutData['langSwitchUrl']))
                                   {

                                    if(app()->getLocale()=="en")
                                    {
                                      $layoutData['langSwitchUrl']="/ar/";
                                    }
                                    else
                                    {
                                         $layoutData['langSwitchUrl']="/en/";
                                    }
                                   }
                                @endphp

                                @if(app()->getLocale()=='ar')

                                <a id="dropdownMenu2" aria-expanded="false" href="{{$layoutData['langSwitchUrl']}}">
                                    <span class="flag-img"><img src="/front/images/icons/en.jpg" alt=""></span>English
                                </a>

                                @else
                                <a id="dropdownMenu2" aria-expanded="false" href="{{$layoutData['langSwitchUrl']}}">
                                    <span class="flag-img"><img src="/front/images/icons/ar.png" alt=""></span>Arabic
                                </a>

                                @endif

                            </li>
                        </ul>
                    </div>

                </div>

            </div><!-- Col End -->


            <div class="col-lg-4 col-md-4 d-lg-none d-md-none d-xl-none">
                <div class="top-bar-event">
                  <span><i class="icon icon-phone3"></i>{{setting('contact-us.phone_number')}} &nbsp; &nbsp; &nbsp;
                     <i class="icon icon-envelope"></i>{{setting('contact-us.email')}} </span>
                </div> <!-- Top Bar Text End -->


            </div><!-- Col End -->


        </div> <!-- Row End -->
    </div> <!-- Container End -->
</div> <!-- Top Bar End -->


<header id="ts-header-classic" class="ts-header-classic header-default">

    <div class="ts-logo-area">
        <div class="container2">
            <div class="row align-items-center">
                <div class="col-md-4 col-sm-4">
                    <a class="ts-logo" href="{{url('/')}}" class="ts-logo">
                        <img src="{{Voyager::image(setting('site-arabic.logo'))}}" alt="logo">
                    </a>
                </div>
                <!-- Col End -->
                <div id="searchContainer" class="col-md-4 col-sm-12 d-none d-md-block text-center searchContainer" >
                    <form class="search" method="#">
                        <input  autocomplete="off" type="text" class="textbox" id="search" placeholder="{{setting('site-arabic.search_placeholder')}}">
                        <input title="Search" value="" type="submit" class="button">
                    </form>
                    <table id="searchResult1" class="table table-bordered table-hover searchResult1 text-left">
                    <tbody>
                    </tbody>
                </table>
                </div>
                <div class="col-md-4 col-sm-12 d-none d-md-block  float-right">

                    <ul class="top-contact-info">
                        <li>
                            <div class="info-wrapper">
                                <p class="info-subtitle"> <i class="icon icon-phone3"></i> +971 458-654-528</p>
                            </div>
                        </li>
                        <!-- li End -->
                        <li>
                            <div class="info-wrapper" style="margin-left: 0px;">
                                <p class="info-subtitle"> <i class="icon icon-envelope"></i> export@mcb.com</p>
                            </div>
                        </li>
                        <!-- Li End -->
                    </ul>
                    <!-- Contact info End -->
                </div>


                <!-- Col End -->
            </div>



<div class="card  searchResultsContainer" id="mainSearchResult" >
  <div class="card-header"> <span id="closeSearch" class="pull-right closeSearch"><i class="icon icon-cross"></i></span></div>
  <div class="row" id="searchResultsContainer"></div>

</div>


            <!-- Row End -->
        </div>
        <!-- Container End -->
    </div>
    <!-- Logo End -->

    <div class="header-angle">
        <div class="container1">
            <nav class="navbar navbar-expand-lg navbar-light">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse ts-navbar" id="navbarSupportedContent">
                    <ul class="navbar-nav">
                         <li class="nav-item active">
                            <a class="nav-link active" href="{{url('/')}}" >
                                {{setting('menu-arabic.home_page')}}
                            </a>
                        </li>


                         <div class="megaDropdown">
                            <li class="nav-item dropbtn">

                             <a class="nav-link" href="#" data-toggle="dropdown">
                               {{ setting('menu-arabic.products')}}
                                <span class="ts-indicator"><i class="fa fa-angle-down"></i></span>
                            </a>

                        </li>

    <div class="megaDropdown-content">
      <div class=" row">
        <div class="col-lg-6 ">
            <div class="header">
            <h2> {{ setting('menu-arabic.automotive')}}</h2></div>
               <div class="row">
              @foreach($sharedData['categories'] as $category)

                @if($category->parent==0)


                    <div class="col-md-6">
                        <div class="row">

                            <div class="col-md-3"><a href="/products/{{$category->slug}}">
                      <img class="img-fluid" src="{{Voyager::image( $category->product->first()->thumbnail('cropped'))}}" alt=""> </a></div>

                      <div class="col-md-9"> <a href="/products/{{$category->slug}}">{{$category->name}}</a></div>

                        </div>


                  </div>



                @endif

              @endforeach
      </div>
        </div>
        <div class="col-lg-6 ">
            <div class="header">
            <h2>{{ setting('menu-arabic.industrial')}}</h2></div>
      <div class="row">
              @foreach($sharedData['categories'] as $category)

                @if($category->parent==1)


                        <div class="col-md-6">
                        <div class="row">

                            <div class="col-md-3"><a href="/products/{{$category->slug}}">
                      <img class="img-fluid" src="{{Voyager::image( $category->product->first()->thumbnail('cropped'))}}" alt=""> </a></div>

                      <div class="col-md-9"> <a href="/products/{{$category->slug}}">{{$category->name}}</a></div>

                        </div>


                  </div>


                @endif

              @endforeach
      </div>
        </div>

      </div>

    </div>
  </div>
  <style>



.megaDropdown {
  float: left;
  /*overflow: hidden;*/
}

.megaDropdown .dropbtn {
  font-size: 16px;
  border: none;
  outline: none;
  color: white;
  /*padding: 14px 16px;*/
  background-color: inherit;
  font: inherit;
  margin: 0;
}

.megaDropdown:hover .dropbtn {
  background-color: red;
}

.megaDropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  width: 100%;
  left: 0;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.megaDropdown-content .header {

  padding: 16px;
  color: white;
}

.megaDropdown:hover .megaDropdown-content {
  display: block;
}

/* Create three equal columns that floats next to each other */
.column {
  float: left;
  width: 33.33%;
  padding: 10px;
  background-color: #ccc;
  height: 250px;
}

.column a {
  float: none;
  color: black;
  padding: 16px;
  text-decoration: none;
  display: block;
  text-align: left;
}

.column a:hover {
  /*background-color: #ddd;*/
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Responsive layout - makes the three columns stack on top of each other instead of next to each other */
@media screen and (max-width: 600px) {
  .column {
    width: 100%;
    height: auto;
  }
}
</style>
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('/'.app()->getLocale().'/brands-ar')}}" >
                               {{ setting('menu-arabic.brands')}}
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="#" data-toggle="dropdown">
                                {{setting('menu-arabic.company')}}
                                <span class="ts-indicator"><i class="fa fa-angle-down"></i></span>
                            </a>
                            <ul class="dropdown-menu ts-dropdown-menu">
                                @foreach($sharedData['pages'] as $page)
                                @if($page->title!="")
                                <li><a href="{{url('/'.app()->getLocale().'/company-ar/'.$page->slug)}}">{{$page->title}}</a></li>
                                @endif
                                @endforeach

                            </ul>
                             <!--End of Dropdown menu -->
                        </li>


                        <li class="nav-item">
                           <a class="nav-link" href="{{url('/'.app()->getLocale().'/career/current-openings')}}" >
                                {{setting('menu-arabic.career')}} [ {{$sharedData['jobs']}} ]
                            </a>

                        </li>



                        <li class="nav-item">
                            <a class="nav-link" href="{{url('/ar/contact-ar')}}" >
                               {{ setting('menu-arabic.contact')}}
                            </a>
                        </li> <!-- End Dropdown -->

                    </ul> <!-- End Navbar Nav -->


                </div> <!-- End of navbar collapse -->
                <div class="right-side-box">

                    <a href="{{url('/ar/request-for-quotation-2')}}" class="rqa-btn"><span class="inner">{{setting('menu-arabic.rfq') }}<i class="fa fa-caret-right"></i></span></a>

                </div>
            </nav>
            <!-- End of Nav -->

        </div>
        <!-- End of Container -->
    </div>
    <!-- End of Header Angle-->

</header> <!-- End of Header area-->

@yield('content')

<footer class="footer" id="footer">
    <div class="footer-top">
        <div class="container2">
            <div class="row">
                <div class="col-md-4 footer-box">
                    <i class="icon icon-map-marker2"></i>
                    <div class="footer-box-content">
                       {!!setting('site-arabic.footer_box1')!!}
                    </div>
                </div><!-- Box 1 end-->
                <div class="col-md-4 footer-box">
                    <i class="icon icon-phone3"></i>
                    <div class="footer-box-content">
                       {!!setting('site-arabic.footer_box2')!!}
                    </div>
                </div><!-- Box 2 end-->
                <div class="col-md-4 footer-box">
                    <i class="icon icon-envelope"></i>
                    <div class="footer-box-content">
                        {!!setting('site-arabic.footer_box3')!!}
                    </div>
                </div><!-- Box 3 end-->
            </div><!-- Content row end-->
        </div><!-- Container end-->
    </div><!-- Footer top end-->
    <div class="footer-main">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 footer-widget footer-about">
                    <div class="footer-logo">
                        <a href="index-2.html">
                            <img class="img-fluid" src="/front/images/logo/logo2.png" alt="">
                        </a>
                    </div>
                    <p>{{setting('site-arabic.footer_below_logo')}}</p>
                    <div class="footer-social">
                        <ul class="unstyled">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        </ul> <!-- Ul end -->
                    </div><!-- Footer social end-->
                </div> <!-- Col End -->
                <!-- About us end-->
                <div class="col-lg-3 col-md-6 footer-widget widget-service">
                    <h3 class="widget-title">{{setting('site-arabic.our_products')}}</h3>
                    <ul class="unstyled">
                        @foreach($sharedData['categories'] as $category)
                        <li><a href="/products/{{$category->slug}}">{{ $category->name }}</a></li>
                        @endforeach

                    </ul> <!-- Ul end -->
                </div> <!-- Col End -->
                <div class="col-lg-3 col-md-6 footer-widget news-widget">
                    <h3 class="widget-title">{{setting('site-arabic.twitter_updates')}}</h3>
                   <a class="twitter-timeline" data-height="250" data-theme="light" href="https://twitter.com/MineralCircles?ref_src=twsrc%5Etfw">Tweets by MineralCircles</a> <script async src="/js/widgets.js" charset="utf-8"></script>
                </div> <!-- Col End -->
                <div class="col-lg-3 col-md-6 footer-widget">
                    <h3 class="widget-title">{{setting('site-arabic.contact_mcb_heading')}}</h3>
                    <ul class="unstyled service-time">
                        <li>
                           {!!setting('site-arabic.contact_mcb')!!}
                        </li>

                    </ul> <!-- Service Time -->
                </div> <!-- Col End -->
            </div><!-- Content row end-->
        </div><!-- Container end-->
    </div><!-- Footer Main-->
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <div class="copyright-info"><span>{{setting('site-arabic.copyright')}}</span>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="footer-menu">
                        <ul class="nav unstyled">
                            <li><a href="/">{{setting('menu-arabic.home_page')}}</a></li>
                            <li><a href="#">Terms</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">{{setting('menu-arabic.contact')}}</a></li>
                        </ul> <!-- Nav End -->
                    </div> <!-- Footer menu end -->
                </div> <!-- Col End -->
            </div><!-- Row end-->
        </div><!-- Container end-->
        <div class="back-to-top" id="back-to-top" data-spy="affix" data-offset-top="10" style="display: block;">
            <button class="back-btn" title="Back to Top">
                <i class="fa fa-angle-double-up"></i><!-- icon end-->
            </button><!-- button end-->
        </div><!-- Back to top -->
    </div><!-- Copyright end-->
</footer> <!-- Footer End -->

<!-- javaScript Files
   =============================================================================-->

<!-- initialize jQuery Library -->
<script src="/front/js/jquery.min.js"></script>
<!-- Popper JS -->
<script src="/front/js/popper.min.js"></script>
<!-- Bootstrap jQuery -->
<script src="/front/js/bootstrap.min.js"></script>
<!-- Owl Carousel -->
<script src="/front/js/owl-carousel.2.3.0.min.js"></script>
<!-- START js copy section -->
<script src="/front/js/contactme/bootstrap-datepicker.min.js"></script>
<script src="/front/js/contactme/bootstrap-datepicker-lang/en.js"></script>
<script src="/front/js/contactme/jquery.timepicker.min.js"></script>
<script src="/front/js/contactme/select2.full.min.js"></script>
<script src="/front/js/contactme/select2-lang/en.js"></script>
<!--[if lt IE 9]><script src="contactme/js/EQCSS-polyfills-1.7.0.min.js"></script><![endif]-->
<script src="/front/js/contactme/EQCSS-1.7.0.min.js"></script>
<script src="/front/js/contactme/contactme-config.js"></script>
<script src="/front/js/contactme/contactme-1.4.js"></script>
<!-- To enable Google reCAPTCHA, uncomment the next line: -->
<!-- <script src="https://www.google.com/recaptcha/api.js?onload=initRecaptchas&render=explicit" async defer></script> -->
<!-- END js copy section -->
<!-- MCB JS Custom -->


<script src="/front/js/jquery-modal-video.min.js"></script>
<link rel="stylesheet" href="/front/css/modal-video.min.css"/>
<script src="/front/js/main.js"></script>
<script>
    $('#search').on('keyup', function () {

        $value = $(this).val();
        $.ajax({
            type: 'get',
            url: '{{URL::to('search')}}',
            data: {'search': $value},
            success: function (data) {
               // $(".searchResult").slideDown();
// $('#searchResultsContainer');

if(data=="")
{
    $("#mainSearchResult").hide();
}
else
{
    $('#searchResultsContainer').html(data);
    $("#mainSearchResult").slideDown();
}

            }
        });
    })


    $("#closeSearch").click(function()
    {

 $("#mainSearchResult").slideUp();
    });
$('body').click(function()
{


  // $(".searchResult").slideUp();
})

    $(".js-modal-btn").modalVideo();
</script>
<script type="text/javascript">
    $.ajaxSetup({headers: {'csrftoken': '{{ csrf_token() }}'}});
</script>
<!-- livezilla.net PLACE SOMEWHERE IN BODY -->
<script type="text/javascript" id="a977835ce83c3ad285aebef5309a4ac6" src="//mcb.workspace.destring.com/livezilla/script.php?id=a977835ce83c3ad285aebef5309a4ac6" defer></script>
<!-- livezilla.net PLACE SOMEWHERE IN BODY -->


<script>
$(document).ready(function() {
  $("#t1,#t2,#t3").keypress(function (e) {
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        $("#errmsg1,#errmsg2,#errmsg3,").html("Digits Only").show().fadeOut("slow");
        return false;
    }
   });

 var max_fields      = 10; //maximum input boxes allowed
 var wrapper         = $(".input_fields_wrap"); //Fields wrapper
 var wrapper2 = $("div.input_fields_wrap"); //Fields wrapper
 var add_button      = $(".add_field_button"); //Add button ID

 var x = 1; //initlal text box count

  var max_fields2      = 10; //maximum input boxes allowed
 var wrapperb         = $(".input_fields_wrap2"); //Fields wrapper
 var wrapperb2 = $("div.input_fields_wrap2"); //Fields wrapper
 var add_button2      = $(".add_field_button2"); //Add button ID

 var x2 = 1; //initlal text box count


 $(add_button).click(function(e){ //on add input button click

 e.preventDefault();


        if(x < max_fields){ //max input box allowed
            x++; //text box increment
$(".input_fields_wrap").append('<div  id="'+ x + '" class="row" style="margin:0 auto;"> \
                <div class="col-lg-1"> '+ x + '\
                </div>\
                <div class="col-lg-3">\
                  <div class="form-group">\
                                      \
                   <input type="text" class="form-control" name="items[]" id="items">\
                   </div>\
                </div>\
                  <div class="col-lg-3">\
                    <div class="form-group">\
                           \
                   <input type="text" class="form-control" name="brands[]" id="brands">\
                   </div>\
                </div>\
                  <div class="col-lg-3">\
                   <div class="form-group">\
                \
                       \
                   <input type="text" class="form-control" name="quantity[]" id="quantity">\
                   </div>\
                </div>\
                 <div class="col-lg-1">\
                   <a href="#" class="remove_field" id="'+ x + '"><img src="https://mcb.ae/wp-content/themes/MCB/images/delete.png" border="0" /></a>\
                </div>      </div>'); //add input box
        }
    });


    // <div class="row"  style="margin:0 auto;"  id="'+ x + '"><div class="col-lg-8"><div class="form-group">'+ x + ') Year of Graduation<br/><input type="date" class="form-control" name="graduation[]"    ></div></div><div class="col-lg-8"><div class="form-group">School/University<br/><input type="text" class="form-control" name="school[]"    ></div></div><div class="col-lg-8"><div class="form-group">Inclusive Years<br/><input type="date" class="form-control" name="inclusive[]"     ></div></div><div class="col-lg-1 no-padding"><br/><a href="#" class="remove_field" id="'+ x + '"><img src="https://mcb.ae/wp-content/themes/MCB/images/delete.png" border="0" /></a></div></div>


    $(wrapper2).on("click",".remove_field", function(e){ //user click on remove text

        var counts = document.getElementsByName('items[]');
          var cnt=counts.length;
          // alert(cnt);
      //  e.preventDefault();
        var xx=$(this).attr('id');



 $('div#' + xx).remove(); x--;
 return false;

    });




     $(add_button2).click(function(e){ //on add input button click

 e.preventDefault();

         if(x2 < max_fields2){ //max input box allowed
            x2++; //text box increment
$(".input_fields_wrap2").append('<div class="row"  style="margin:0 auto;"  id="'+ x2 + '"> <div class="col-lg-8"><div class="form-group">'+ x2 + ') Company<br/><input type="text" class="form-control" name="company[]"  ></div></div><div class="col-lg-8"><div class="form-group">Contacts<br/><input type="text" class="form-control" name="contacts[]"     ></div></div><div class="col-lg-8"><div class="form-group">Designation<br/><input type="text" class="form-control" name="designation[]"    ></div></div><div class="col-lg-8"><div class="form-group">Date<br/><input type="date" class="form-control" name="dates[]"     ></div></div><div class="col-lg-8"><div class="form-group">Last Salary<br/><input type="text" class="form-control" name="salary[]"     ></div></div><div class="col-lg-1 no-padding"><br/><a href="#" class="remove_field" id="'+ x2 + '"><img src="https://mcb.ae/wp-content/themes/MCB/images/delete.png" border="0" /></a></div></div>'); //add input box
        }
    });






    $(wrapper2).on("click",".remove_field", function(e){ //user click on remove text
        var counts = document.getElementsByName('countss[]');
          var cnt=counts.length;
      //  e.preventDefault();
        var xx=$(this).attr('id');



 $('div#' + xx).remove(); x--;
 return false;

    })



});



</script>

 <script type="text/javascript">
        $(document).ready(function (e) {
            $('.flip').hover(function(){
        $(this).find('.card').toggleClass('flipped');

 });


});


</script>

  <!-- Icons -->
  <link rel="stylesheet" type="text/css" media="screen" href="/youtube-video-player/packages/icons/css/icons.min.css" />

  <!-- Main Stylesheet -->
  <link rel="stylesheet" type="text/css" media="screen" href="/youtube-video-player/css/youtube-video-player.min.css" />

  <!-- jQuery -->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

  <!-- Main Javascript -->
  <script type="text/javascript" src="/youtube-video-player/js/youtube-video-player.jquery.min.js"></script>

  <!-- Perfect Scrollbar -->
  <link rel="stylesheet" type="text/css" media="screen" href="/youtube-video-player/packages/perfect-scrollbar/perfect-scrollbar.css" />
  <script type="text/javascript" src="/youtube-video-player/packages/perfect-scrollbar/jquery.mousewheel.js"></script>
  <script type="text/javascript" src="/youtube-video-player/packages/perfect-scrollbar/perfect-scrollbar.js"></script>



  <script>
        $(document).ready(function() {

        {!! (isset($common))? ((isset($common['slidersJs']))? ($common['slidersJs']=="")? "" : $common['slidersJs']:"") : ""  !!}
        });
      </script>

</body>

</html>
