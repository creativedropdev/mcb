<!doctype html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!-- Basic Page Needs =====================================-->


    <!-- Mobile Specific Metas ================================-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <!-- Site Title- -->
    <title> {{ isset($common['title']) ? $common['title'] : setting('site.title')}}</title>

    @if(isset($common))


    {!! (isset($common['keywords']))?(($common['keywords']!="") ? "
    <meta name='keywords' content='".$common['keywords']."' />" : ""):""!!}
    {!! (isset($common['description']))?(($common['description']!="") ? "
    <meta name='description' content='".$common['description']."' />" : ""):""!!}
    {!! (isset($common['canonical']))?(($common['canonical']!="") ? "
    <link rel='canonical' href='".$common['canonical']."' />" : ""):""!!}
    @endif
    <!-- CSS
    ==================================================== -->

    <link rel="icon" type="/image/png" href="/assets/img/favicon.png">
	<link rel="stylesheet" href="/css/all.css">

		<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="/css/bootstrap.min.css">
	<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css"> -->

	<link rel="stylesheet" href="/css/slick.css">
	<link rel="stylesheet" href="/css/slick-theme.css">

	<link rel="stylesheet" href="/css/jquery.mCustomScrollbar.min.css">

	

	<link rel="stylesheet" href="/css/magnific-popup.css">

	<!-- <link rel="stylesheet" href="css/intlTelInput.css"> -->
	<link rel="stylesheet" href="/css/intlTelInput.css">

	<link rel="stylesheet" href="/css/theme.css?v=1.7">


	<script src="https://www.google.com/recaptcha/api.js" async defer></script>


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>

<body>
    <header>
		<div class="topbar">
			<div class="mcb-container">
				<nav class="navbar navbar-expand-lg navbar-light bg-white">
				  <a class="navbar-brand" href="/"><img src="/assets/img/brand-logo.png" alt="brand-logo" width="75%"></a>

				  <!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				    <span class="navbar-toggler-icon"></span>
				  </button> -->

				  <ul class="searchbar-ul list-inline p-0 m-0">
					  <li class="list-inline-item">
					  	<button type="button" id="sidebarCollapse" class="btn prc-btn-toggle" data-aos="fade-left"><i class="fas fa-bars"></i></button>
				      </li>
					  <li class="list-inline-item">
					  	<button type="button" id="searchingToggle" class="btn prc-btn-toggle" data-aos="fade-left"><i class="fas fa-search"></i></button>
					  </li>
				  </ul>

				  <div class="collapse navbar-collapse" id="navbarSupportedContent">
				    <ul class="navbar-nav ml-auto top-nav pr-3 mr-3">
				      <li class="nav-item">
				        <a class="nav-link" href="mailto: {{setting('site.email')}}">{{setting('site.email')}}</a>
				      </li>
				      <li class="nav-item">
				        <a class="nav-link" href="tel:{{setting('site.phone')}}">{{setting('site.phone')}}</a>
				      </li>
				    </ul>
				    <form class="form-inline my-2 my-lg-0">
				      <input id="search" class="form-control mr-sm-2 mcb-trans searching" type="search" placeholder="Search" aria-label="Search">
				      <a href="#" class="top-search"><i class="fas fa-search search-icon"></i></a>
				    </form>
				    <button class="btn my-2 my-sm-0 mcb-btn mcb-btn-orange" data-toggle="modal" data-target="#mcbModal">QUOTATION</button>
				  </div>
				</nav>
			</div>
			<!-- mcb-container -->
		</div>

		<div class="topbar responsive-searchbar" style="display:none">
			<form class="form-inline my-2 my-lg-0">
				<input class="form-control mr-sm-2 mcb-trans searching" type="search" placeholder="Search" aria-label="Search">
				<a href="#" class="top-search"><i class="fas fa-search search-icon"></i></a>
			</form>
		</div>
		<!-- topbar -->
		
		<div id="mainSearchResult"  class= "search-result responsive-search-result pt-3 pb-3">
                     <span id="closeSearch-1" class="pull-right closeSearch"><i class="fas fa-times fa-lg m-dark"></i></span>
			<div class="mcb-container" >
				<div class="row" id="searchResultsContainer"></div>

<!--				 <div class="close-search">
					<a href="#" id="closeSearch" class="closing-search closeSearch">Close<i class="fas fa-times fa-lg m-dark"></i></a>
				</div> -->
			</div>
		</div>


		<div class="menu">
			<div class="mcb-container">
				<nav class="navbar navbar-expand-lg navbar-light">
				  
				  <div class="collapse navbar-collapse" id="navbarSupportedContent">
				    <ul class="headermainmenu navbar-nav m-auto">
						
						<li class="nav-item dropdown">
					        <a class="nav-link dropdown-toggle current-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					          Products
					        </a>
					        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
					          @foreach($sharedData['categories'] as $category)
                                                    <a class="dropdown-item" href="/products/{{$category->slug}}">{{$category->name}}</a>
                                                  @endforeach
					        </div>
					    </li>
						
						<li class="nav-item dropdown">
					        <a class="nav-link dropdown-toggle current-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					          Brands
					        </a>
					        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
					          @foreach($sharedData['brands'] as $brand)
                                                    <a class="dropdown-item" href="{{url('/'.app()->getLocale().'/brands/'.$brand->slug)}}">@if($brand->menu!="")
                                                            {{$brand->menu}} @else {{$brand->title}} @endif</a>
                                                    @endforeach
					        </div>
					      </li>
						
						<li class="nav-item dropdown">
					        <a class="nav-link dropdown-toggle current-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					          Company
					        </a>
					        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                                  @foreach($sharedData['pages'] as $page)
                                                    <a class="dropdown-item" href="{{url('/'.app()->getLocale().'/company/'.$page->slug)}}">{{$page->title}}</a>
                                                    @endforeach  
					          <a class="dropdown-item" href="webinar">Webinar</a>
					          
					        </div>
					    </li>

						<li class="nav-item dropdown">
					        <a class="nav-link dropdown-toggle current-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					          Careers
					        </a>
					        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
					          <a class="dropdown-item" href="/{{app()->getLocale()}}/our-team">Our Team</a>
					          <a class="dropdown-item" href="/{{app()->getLocale()}}/career/who-we-are">Our Work Culture</a>
					          <!--<a class="dropdown-item" href="/our-team-2">Our Team 2</a>-->
					          <a class="dropdown-item" href="/{{app()->getLocale()}}/career/current-openings">Current Openings</a>
					          <a class="dropdown-item" href="/{{app()->getLocale()}}/career/internship-program">Internship Program</a>
					        </div>
					    </li>

					    <li class="nav-item dropdown">
					        <a class="nav-link dropdown-toggle current-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					          Publications
					        </a>
					        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
					          <a class="dropdown-item" href="/blog">Blog</a>
					          <a class="dropdown-item" href="/press-release">Press Release</a>
					          <a class="dropdown-item" href="/press-release">Technical Tips</a>

					          <!-- <a class="nav-link dropdown-toggle current-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					          Technical Tips
						      </a>
						      <div class="dropdown-menu" aria-labelledby="navbarDropdown">
						        <a class="dropdown-item" href="#">Action</a>
						        <a class="dropdown-item" href="#">Another action</a>
						      </div> -->
					        
					        </div>
					    </li>


				    	<!-- <li class="nav-item dropdown">
					        <a class="nav-link dropdown-toggle current-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					          Technical Tips
					        </a>
					        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
					          <a class="dropdown-item" href="#">Action</a>
					          <a class="dropdown-item" href="#">Another action</a>
					        </div>
				    	</li> -->

				    	<li class="nav-item">
				        	<a class="nav-link current-link" href="/{{ app()->getLocale()}}/contact">Contact Us</a>
				     	</li>	

				    </ul>
				  </div>	
				</nav>
			</div>
			<!-- mcb-container -->
		</div>
		<!-- menu -->
	</header>
    <div class="wrapper">
        @if(Session::has('message'))
        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        <!--<div class="alert alert-success">Thank you very much for contact us. We will back to you soon.</div>-->
        @endif
        <nav id="sidebar">
          <button id="dismiss" class="btn prc-btn-close">
              <i class="fas fa-times"></i>
            </button>

          <div class="sidebar-header">
            <a href="index"><img src="assets/img/brand-logo.png" alt="Logo"></a>
          </div>

          <ul class="list-unstyled components">

                  <li>
                <a href="#homeSubmenuMulti1" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">PRODUCTS</a>
                <ul class="collapse list-unstyled" id="homeSubmenuMulti1">
                                    @foreach($sharedData['categories'] as $category)
                                    <a href="/products/{{$category->slug}}">{{$category->name}}</a>
                                    @endforeach

                                </ul>
                
              </li>

              <li>
                <a href="#homeSubmenuMulti2" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">BRANDS</a>
                <ul class="collapse list-unstyled" id="homeSubmenuMulti2">
                  <li>
                      @foreach($sharedData['brands'] as $brand)
                                    <a href="{{url('/'.app()->getLocale().'/brands/'.$brand->slug)}}">@if($brand->menu!="")
                                            {{$brand->menu}} @else {{$brand->title}} @endif</a>
                                    @endforeach
                  </li>
                </ul>
              </li>

              <li>
                <a href="#homeSubmenuMulti3" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">COMPANY</a>
                <ul class="collapse list-unstyled" id="homeSubmenuMulti3">
                  <li>
                      @foreach($sharedData['pages'] as $page)
                                    <a href="{{url('/'.app()->getLocale().'/company/'.$page->slug)}}">{{$page->title}}</a>
                       @endforeach             
                                    
                  </li>
                </ul>
              </li>

              <li>
                <a href="#homeSubmenuMulti4" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">CAREERS</a>
                <ul class="collapse list-unstyled" id="homeSubmenuMulti4">
                  <li>
                      @foreach($sharedData['careerPages'] as $page)
                                    <a
                                            href="{{url('/'.app()->getLocale().'/career/'.$page->slug)}}">{{$page->title}}</a>
                                    
                                    @endforeach
                                    <a href="{{url('/'.app()->getLocale().'/career/current-openings')}}">
                                            Current Openings [ {{$sharedData['jobs']}} ]
                                        </a>
                  </li>
                </ul>
              </li>

              <li>
                <a href="#homeSubmenuMulti5" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">PUBLICATIONS</a>
                <ul class="collapse list-unstyled" id="homeSubmenuMulti5">
                  <li>
                      <a href="blog">BLOG</a>
                      <a href="press-release">PRESS RELEASE</a>
                      <a href="internship">TECHNICAL TIPS</a>
                  </li>
                </ul>
              </li>

              <li class="nav-item">
                      <a class="nav-link current-link" href="/{{ app()->getLocale()}}/contact">Contact Us</a>
              </li>	
          </ul>

        </nav>
      </div>
<div class="overlay"></div>
    

    </header> <!-- End of Header area-->

    @yield('content')

    <footer>
		<section class="subscribe section-padtop-30 section-padbottom-30 mb-orange">
			<div class="mcb-container">
				<div class="row">
					 <div class="col-md-6 offset-lg-1 col-lg-5 res-center">
						<div class="subscribe-head">
							<p class="mcb-h2">Subscribe for Newsletter</p>
						</div>
					</div>
					<div class="col-md-6 col-lg-5 res-center">
						<div class="newsletter">
                                                    
							<input type="email" id="newsletter-email-text" placeholder="Enter your email">
							<div class="sub-icon">
								<a href="#" class="text-white newsletter-sub"><i class="fas fa-paper-plane"></i></a>
							</div>
						</div>
					</div> 
<!--                    <div class="col-md-12 text-center res-center">
                        <div class="subscribe-head">
                            <p class="mcb-h2">Subscribe for Newsletter</p>
                            <button class="btn mcb-btn-blue" data-toggle="modal" data-target="#subscribeModal">SUBSCRIBE NOW!</button>
                        </div>
                    </div>-->
				</div>
			</div>
		</section>
		<!-- subscribe -->

		<section class="footer-links">
			<div class="mcb-container">
				<div class="row">
					<div class="col-6 col-sm-4 col-md-2 footer">
						<div class="foot-1 section-padtop-50 section-padbottom-50 res-paddingtop-30 res-paddingbottom-30">
							<h5 class="mcb-h5">Company</h5>
							<ul>
								<li><a href="about-us">About Us</a></li>
								<li><a href="the-goal">The Goal</a></li>
								<li><a href="core-values">Core Values</a></li>
								<li><a href="leadership">Leadership</a></li>
								<li><a href="infrastructure">Infrastructure</a></li>
								<li><a href=milestones>Milestone</a></li>
							</ul>
						</div>
					</div>
					<div class="col-6 col-sm-4 col-md-3 footer">
						<div class="foot-1 section-padtop-50 section-padbottom-50 res-paddingtop-30 res-paddingbottom-30">
							<h5 class="mcb-h5">Products</h5>
							<ul>@foreach($sharedData['categories'] as $category)
								<li><a href="/products/{{$category->slug}}">{{$category->name}}</a></li>
                                                            @endforeach    
								
							</ul>
						</div>
					</div>
                   
					<div class="col-6 col-sm-4 col-md-2 footer footer-no-right-border">
						<div class="foot-1 section-padtop-50 section-padbottom-50 res-paddingbottom-30 res-no-padding">
							<h5 class="mcb-h5">Brands</h5>
							<ul>@foreach($sharedData['brands'] as $brands)
								<li><a href="/en/brands/{{$brands->slug}}">{{$brands->title}}</a></li>
                                                            @endforeach    
							</ul>
						</div>
					</div>
<!--					<div class="col-6 col-sm-4 col-md-2 footer">
						<div class="foot-1 footer-paddingtop-0 section-padtop-50 section-padbottom-50 res-paddingbottom-30 res-no-padding">
							<h5 class="mcb-h5">Industrial</h5>
							<ul>
								<li><a href="#">Industrial Chain</a></li>
								<li><a href="#">Tools for service and</a></li>
								<li><a href="#">maintenance</a></li>
								<li><a href="#">Industrial bearings</a></li>
								<li><a href="#">Lubrication solution</a></li>
								<li><a href="#">and more</a></li>
							</ul>
						</div>
					</div>-->
					<div class="col-6 col-sm-4 col-md-2 footer">
						<div class="foot-1 footer-paddingtop-0 section-padtop-50 section-padbottom-50 res-paddingtop-30 res-paddingbottom-30">
							<h5 class="mcb-h5">Career</h5>
							<ul>
								
								<li><a href="/our-team">Our Team</a></li>
								<li><a href="/current-openings">Current Openings</a></li>
								<li><a href="/Internship">Internship Programs</a></li>
							</ul>
						</div>
					</div>
					<div class="col-6 col-sm-4 col-md-2">
						<div class="foot-1 footer-paddingtop-0 section-padtop-50 section-padbottom-50 res-paddingtop-30 res-paddingbottom-30">
							<h5 class="mcb-h5">Media Publications</h5>
							<ul>
								<li><a href="/blog">Blog</a></li>
								<li><a href="/press-release">Press Release</a></li>
								<li><a href="#">Technical Tips.</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<!-- mcb-container -->
		</section>


		<section class="foot-end section-padbottom-30 section-padtop-30">
			<div class="mcb-container">
				<div class="row">
					<div class="col-md-3 col-lg-2 res-center">
						<div class="foot-logo res-marginbottom-20">
							<img src="assets/img/brand-logo.png" alt="">
						</div>

					</div>
					<div class="col-md-5 col-lg-5 res-center">
						<div class="corner res-marginbottom-20">
							<p>Corner S1203 and S120, Jebel Ali Free Zone South Dubai, UAE</p>
							<ul>
								<li><a href="https://mcb.ae/en/terms-of-use/">Terms & Condition</a></li>
								<li>|</li>
								<li><a href="https://mcb.ae/en/privacy-tools/">Privacy Policy</a></li>
								<!-- <li>|</li>
								<li><a href="#">Disclaimer</a></li> -->
							</ul>
						</div>
					</div>

					<div class="col-md-4 offset-lg-1 col-lg-4 res-center">
						<div class="foot-links res-center res-marginbottom-20">
							<ul>
								<li><a href="https://twitter.com/MineralCircles" target="_blank"><i class="fab fa-twitter"></i></a></li>
								<li><a href="https://www.facebook.com/MineralCircles/" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
								<li><a href="https://www.instagram.com/mineralcircles/"><i class="fab fa-instagram" target="_blank"></i></a></li>
                                <li><a href="https://www.linkedin.com/company/mineral-circles-bearings-fze"><i class="fab fa-linkedin-in" target="_blank"></i></a></li>
                                <li><a href="https://www.youtube.com/channel/UC3vuO7YBCWP2ZnxcWa1rhWw"><i class="fab fa-youtube" target="_blank"></i></a></li>
							</ul>
							<p class="mb-0">&copy; 2020. Mineral Circles Bearings. All Rights Reserved.</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- foot-end -->

        <!-- Return to Top -->
        <a href="javascript:" id="return-to-top"><i class="fas fa-chevron-up"></i></a>
	
    </footer>

    @include('layout.modal')

	<!-- jQuery library -->
	<script src="/js/jquery.min.js"></script>

	<!-- Popper JS -->
	<script src="/js/popper.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<!-- <script src="/js/bootstrap.min.js"></script> -->
    <script src="/js/bootstrap.min.js"></script>

    <script src="/js/intlTelInput.min.js"></script>

    <!-- Hide Tab in Leadership Page -->

    <script>
        $(".hide-tab").click(function () {
            $("#nav-tabContent").css("display", "none");
            $('.leader-tab-link').addClass('added-class');
        });

        $(".leader-tab-link").click(function () {
            $("#nav-tabContent").css("display", "block");
            $('.leader-tab-link').removeClass('added-class');
            event.preventDefault();
              $('.leader-tab-link').animate({
                scrollLeft: "-=775px"
              }, "slow");
        });
    </script>
@if(isset($branches))
	<script>
	//Map


          var myLatLng = { lat: 25.6920788, lng: 55.2641677 };
          var markers = [];
          var map;
	    function initMap() {

	        // Create a new StyledMapType object, passing it an array of styles,
	        // and the name to be displayed on the map type control.
	        var styledMapType = new google.maps.StyledMapType(
	            [
                    {
                        "featureType": "all",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "saturation": 36
                            },
                            {
                                "color": "#000000"
                            },
                            {
                                "lightness": 40
                            }
                        ]
                    },
                    {
                        "featureType": "all",
                        "elementType": "labels.text.stroke",
                        "stylers": [
                            {
                                "visibility": "on"
                            },
                            {
                                "color": "#000000"
                            },
                            {
                                "lightness": 16
                            }
                        ]
                    },
                    {
                        "featureType": "all",
                        "elementType": "labels.icon",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "color": "#000000"
                            },
                            {
                                "lightness": 20
                            }
                        ]
                    },
                    {
                        "featureType": "administrative",
                        "elementType": "geometry.stroke",
                        "stylers": [
                            {
                                "color": "#000000"
                            },
                            {
                                "lightness": 17
                            },
                            {
                                "weight": 1.2
                            }
                        ]
                    },
                    {
                        "featureType": "administrative",
                        "elementType": "labels",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative.country",
                        "elementType": "all",
                        "stylers": [
                            {
                                "visibility": "simplified"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative.country",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "visibility": "simplified"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative.country",
                        "elementType": "labels.text",
                        "stylers": [
                            {
                                "visibility": "simplified"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative.province",
                        "elementType": "all",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative.locality",
                        "elementType": "all",
                        "stylers": [
                            {
                                "visibility": "simplified"
                            },
                            {
                                "saturation": "-100"
                            },
                            {
                                "lightness": "30"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative.neighborhood",
                        "elementType": "all",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative.land_parcel",
                        "elementType": "all",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "landscape",
                        "elementType": "all",
                        "stylers": [
                            {
                                "visibility": "simplified"
                            },
                            {
                                "gamma": "0.00"
                            },
                            {
                                "lightness": "74"
                            }
                        ]
                    },
                    {
                        "featureType": "landscape",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#0a2360"
                            },
                            {
                                "lightness": "-37"
                            }
                        ]
                    },
                    {
                        "featureType": "landscape.man_made",
                        "elementType": "all",
                        "stylers": [
                            {
                                "lightness": "3"
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "all",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#000000"
                            },
                            {
                                "lightness": 21
                            }
                        ]
                    },
                    {
                        "featureType": "road",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "visibility": "simplified"
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "color": "#0e204d"
                            },
                            {
                                "lightness": "0"
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "geometry.stroke",
                        "stylers": [
                            {
                                "color": "#000000"
                            },
                            {
                                "lightness": 29
                            },
                            {
                                "weight": 0.2
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#7d7c9b"
                            },
                            {
                                "lightness": "43"
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "labels.text.stroke",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "road.arterial",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#0e204d"
                            },
                            {
                                "lightness": "1"
                            }
                        ]
                    },
                    {
                        "featureType": "road.arterial",
                        "elementType": "labels.text",
                        "stylers": [
                            {
                                "visibility": "on"
                            }
                        ]
                    },
                    {
                        "featureType": "road.arterial",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#7d7c9b"
                            }
                        ]
                    },
                    {
                        "featureType": "road.arterial",
                        "elementType": "labels.text.stroke",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "road.local",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#0e204d"
                            },
                            {
                                "lightness": "-1"
                            },
                            {
                                "gamma": "1"
                            }
                        ]
                    },
                    {
                        "featureType": "road.local",
                        "elementType": "labels.text",
                        "stylers": [
                            {
                                "visibility": "on"
                            },
                            {
                                "hue": "#ff0000"
                            }
                        ]
                    },
                    {
                        "featureType": "road.local",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#7d7c9b"
                            },
                            {
                                "lightness": "-31"
                            }
                        ]
                    },
                    {
                        "featureType": "road.local",
                        "elementType": "labels.text.stroke",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "transit",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#0e204d"
                            },
                            {
                                "lightness": "-36"
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#0e204d"
                            },
                            {
                                "lightness": "0"
                            },
                            {
                                "gamma": "1"
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "elementType": "labels.text.stroke",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    }
                ],
	            { name: 'Styled Map' });


	        //UAE
	         map = new google.maps.Map(document.getElementById('map'), {
	            zoom: 5,
	            center: { lat: 33.5868504, lng: -7.603697}
	        });


	        //Associate the styled map with the MapTypeId and set it to display.
	        map.mapTypes.set('styled_map', styledMapType);
	        map.setMapTypeId('styled_map');
                @if(isset($branches))
	        pointers = [
                @foreach($branches as $bkey=> $branch)    
                {
                    "position": {
                      lat: parseInt(getMapll("{!! $branch->map_url !!}")[0]),
                      lng: parseInt(getMapll("{!! $branch->map_url !!}")[1])
                    },
                "key":"{{str_slug($branch->title)}}",
                        "content" : '{!! str_replace("\n", "", $branch->content) !!}' //I love this way to trim line breaks take 45 minutes to find.
//                    "content": '<h5><span style="color: #0076c0;">Mineral Cercles Roulements</span></h5><p>No. 25 Angle Bd La Gironde et Libourne<br />Casablanca, Morocco<br />Mob: +212-661-239939<br />Tel: +212-522-801470;&nbsp;+212-522-540331/33<br />Fax: +212-522-449751<br />E-mail:&nbsp;<a href="mailto:mcrindustrie@mcb.ae">mcrindustrie@mcb.ae</a></p>'
//                    "content": " <div id=\"content\"><div id=\"siteNotice\"></div><h1 class=\"mcb-h5\" id=\"firstHeadingMap\" class=\"firstHeadingMap\">Mineral Circles Bearings</h1><hr><h4 id=\"firstParaMap\" class=\"mcb-h6\" class=\"fisrtparaMap\">Ajman, United Arab Emirates</h4></div>"
                },
                @endforeach
                

	        ]
                @endif
                
	        for(var i = 0; i < pointers.length; i++) {
	            let pointer = pointers[i];
	            let marker=  new google.maps.Marker({
	                position: pointer.position,
	                map: map,
                    title: 'Alhamad.ae',
	            })
	            marker.addListener('click',function() {
	                    new google.maps.InfoWindow({
	                        content: pointer.content
	                    }).open(map,marker)
                    });
                markers.push({key:pointer.key,marker:marker});
	        }
        }
        $('.map-dropdown select').on('change',function(){
            let value = $(this).children("option:selected").val();
            let latlng = null;
            let position= null;
            let i = -1;
            markers.forEach((m,index)=>{
                if(m.key==value)
                {   marker = m.marker;
                    position = marker.getPosition();
                    i=index;
                }
            })
            markerReset(position,()=>{
                $('#demo').carousel(i);
            })
        })

        $('#demo').on('slide.bs.carousel', function (e) {
            let i = e.to;
            $('.map-dropdown select').prop('selectedIndex',i);
            let value = $('.map-dropdown select').val();
            let latlng = null;
            let position= null;
            markers.forEach((m)=>{
                if(m.key==value)
                {   marker = m.marker;
                    position = marker.getPosition();
                }
            })
            markerReset(position)
           
            
        })
        function getMapll(mapUrl){
            //console.log(mapUrl)
            
            var url = mapUrl.split('@');
            var at = url[1].split('z');
            var zero = at[0].split(',');
            var lat = zero[0];
            var lon = zero[1];
//            console.log(zero[0]+">>"+zero[1])
            return zero;
        }
        function markerReset(position,cb=()=>{}){
            if(map.getCenter()!=position)
            {  
                if(map.getZoom()==8)
                {
                    map.setCenter(position);
                    map.setZoom(10);
                    cb();
                }
                else{
                    map.setZoom(8);
                    setTimeout(()=>{
                        map.setCenter(position);
                        map.setZoom(10);
                        cb()
                    },700)
                }
            }
        }

	</script>
			<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRlwzku4Z02K2_FWQu_g-TVHRsP8OdKLM&callback=initMap"
    		async defer></script>
@endif                
    		<script src="/js/slick.min.js"></script>

    		<script>
				$(document).ready(function(){
			      $('.multiple-items').slick({
					  infinite: true,
					  slidesToShow: 3,
					  slidesToScroll: 3
					});
			    });
    		</script>
    		<script src="/js/jquery.waypoints.min.js"></script>
    		<script src="/js/jquery.counterup.min.js"></script>
    		<script>
    			$('.count').counterUp({
				    delay: 10,
				    time: 1000
				});
    		</script>

    		<!-- press-release -->
    		<script>
		(function() {

		  $(".panel").on("show.bs.collapse hide.bs.collapse", function(e) {
		    if (e.type=='show'){
		      $(this).addClass('active');
		    }else{
		      $(this).removeClass('active');
		    }
		  });

		}).call(this);

	</script>

	<script>
		$(function(){
		    var currentURL = location.pathname;
		    $('.headermainmenu li a').each(function(){
		    	var currentAnchor = $(this);
		        if(currentURL.indexOf(currentAnchor.attr('href'))  !== -1){
		            currentAnchor.parents('li').addClass('active');
		        }
		    })
		})
	</script>

    <script src="/js/jquery.magnific-popup.min.js"></script>

    <script>
        $(document).ready(function() {

            $('a.btn-gallery').on('click', function(event) {
                event.preventDefault();
                
                var gallery = $(this).attr('href');
            
                $(gallery).magnificPopup({
              delegate: 'a',
                    type:'image',
                    gallery: {
                        enabled: true
                    }
                }).magnificPopup('open');
            });
            
        });
    </script>

    <script>
        $(document).ready(function() {
            $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
                e.preventDefault();
                $(this).siblings('a.active').removeClass("active");
                $(this).addClass("active");
                var index = $(this).index();
                $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
                $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
            });
        });
    </script>

    <script>
//        $(function() {
//            $('.popup-youtube').magnificPopup({
//                // disableOn: 700,
//                type: 'iframe',
//                mainClass: 'mfp-fade',
//                removalDelay: 160,
//                preloader: false,
//                fixedContentPos: false
//            });
//        });
    </script>

    <script>
        $('.brand-items').slick({
            autoplay:true,
            autoplaySpeed:1000,
            infinite: true,
            slidesToShow: 6,
            slidesToScroll: 1,
            responsive: [
            // {
            //   breakpoint: 1024,
            //   settings: {
            //     slidesToShow: 3,
            //     slidesToScroll: 3,
            //     infinite: true,
            //     dots: true
            //   }
            // },
            {
              breakpoint: 769,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 2
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1
              }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
          ]
        });
    </script>

    

      <script>
          $(".input-numbers").intlTelInput({
            utilsScript: "/js/utils.js"
            });
      </script>





      

      <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script> -->

      <script src="/js/jquery.mCustomScrollbar.concat.min.js"></script>

      <!-- Client Review -->

      <script>
          $('.client-slider').slick({
              infinite: true,
              autoplay:true,
              autoplaySpeed:4000,
              slidesToShow: 3,
              slidesToScroll: 3,
              arrows: true,
              responsive: [
                {
                  breakpoint: 1024,
                  settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                  }
                },
                {
                  breakpoint: 769,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                  }
                },
                {
                  breakpoint: 480,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                  }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
              ]
            });


          $('.product-slider').slick({
              infinite: true,
              autoplay:true,
              autoplaySpeed:4000,              
              slidesToShow: 5,
              slidesToScroll: 5,
              arrows: true,
              responsive: [
                {
                  breakpoint: 1024,
                  settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                  }
                },
                {
                  breakpoint: 769,
                  settings: {
                    slidesToShow: 3,
                    slidesToScroll: 2
                  }
                },
                {
                  breakpoint: 480,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                  }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
              ]
            });

          $('.blog-slider').slick({
              infinite: true,
              slidesToShow: 3,
              slidesToScroll: 3,
              adaptiveHeight: true,
              arrows: true,
              responsive: [
                {
                  breakpoint: 1024,
                  settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                  }
                },
                {
                  breakpoint: 769,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                  }
                },
                {
                  breakpoint: 480,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                  }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
              ]
            });
      </script>


      <!-- Show & Hide Search Results -->

      <script>
          // $(document).ready(function(){
          //     $("#searching").click(function(){
          //       $(".search-result").css("display", "block");
          //     });
          //   });

          // $(document).ready(function(){
          //     $(".closing-search").click(function(){
          //       $(".search-result").css("display", "none");;
          //     });
          //   });

          // Bind keyup event on the input
            $('.searching').keyup(function() {
              
              // If value is not empty
              if ($(this).val().length == 0) {
                // Hide the element
                $('.search-result').hide();
                $('.top-search').show();
              } else {
                // Otherwise show it
                $('.search-result').show();
                $('.top-search').hide();
              }
            }).keyup(); // Trigger the keyup event, thus running the handler on page load
            
             $('#search').on('keyup', function() {

                $value = $(this).val();
                $.ajax({
                    type: 'get',
                    url: '/search',
                    data: {
                        'search': $value
                    },
                    success: function(data) {
                        // $(".searchResult").slideDown();
                        // $('#searchResultsContainer');

                        if (data == "") {
                            $("#mainSearchResult").hide();
                        } else {
                            $('#searchResultsContainer').html(data);
                            $("#mainSearchResult").show();
                        }

                    }
                });
            })
            $(".closeSearch").click(function() {

                $("#mainSearchResult").slideUp();
            });
            $('body').click(function() {


                // $(".searchResult").slideUp();
            })
      </script> 

      <!-- MILESTONE TIMELINE SCROLL -->

      <script>
          if( $(window).width() < 768 ) {
            $('.miles-item').click(function(){
                var len = $(this).prevAll().length;
                var itemWidth = $('.miles-item.active').width();
                var totalgap = Number(len) * Number(itemWidth);
    //             alert(totalgap);
                $( this ).parent().scrollLeft( totalgap );
            })
        }
      </script>

      <!-- PRODUCT SCROLL -->

      <script>
          if( $(window).width() < 1024 ) {
            $('.products-item').click(function(){
                var len = $(this).prevAll().length;
                var itemWidth = $('.products-item.active').width();
                var totalgap = Number(len) * Number(itemWidth);
    //             alert(totalgap);
                $( this ).parent().scrollLeft( totalgap );
            })
        }
      </script>

      <!-- LEADERSHIP SCROLL -->

      <script>
          if( $(window).width() < 2000 ) {
            $('.leader-tab-link').click(function(){
                var len = $(this).prevAll().length;
                var itemWidth = $('.leader-tab-link.active').width();
                var totalgap = Number(len) * Number(itemWidth);
    //             alert(totalgap);
                $( this ).parent().scrollLeft( totalgap );
            })
        }
      </script>

      <!-- ACTIVE CLASS ON PRODUCT TABS MENU -->
      <script>
          $(document).ready(function() {
            $(".product-page-tabs li").click(function () {
                $(".product-page-tabs li").removeClass("active");
                // $(".tab").addClass("active"); // instead of this do the below 
                $(this).addClass("active");   
            });
            });
      </script>

      <!-- ACTIVE CLASS ON BLOG TABS MENU -->
      <script>
          $(document).ready(function() {
            $(".blog-page-tabs li").click(function () {
                $(".blog-page-tabs li").removeClass("active");
                // $(".tab").addClass("active"); // instead of this do the below 
                $(this).addClass("active");   
            });
            });
        </script>

        

        <script>
                        // ===== Scroll to Top ==== 
            $(window).scroll(function() {
                if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
                    $('#return-to-top').fadeIn(200);    // Fade in the arrow
                } else {
                    $('#return-to-top').fadeOut(200);   // Else fade out the arrow
                }
            });
            $('#return-to-top').click(function() {      // When arrow is clicked
                $('body,html').animate({
                    scrollTop : 0                       // Scroll to top of body
                }, 500);
            });
            
            $('.newsletter-sub').click(function(e){
                e.preventDefault();
                console.log("Newsletter Subscription");
//                $('.newsletter').html('<br />Thank you very much for subscription.');
                var nl_email = $('#newsletter-email-text').val();
                
                $.ajax({
                    type: "POST",
                    url: "contact/nls",
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: {"eid" :nl_email},
                    cache: false,
                    success: function(data){
                       //$("#resultarea").text(data);
                    }
                  });
            })
            
            $('.mcbModalProducts').click(function(){ 
                    $("#enquiry_type").val( $(this).parents('.product-card').find($('.product-wheel h6')).html())

            })
            
             <!-- VIDEO NEW POPUP -->
        
            $(document).ready(function() {

            // Gets the video src from the data-src on each button

            var $videoSrc;  
            $('.video-btn').click(function() {
                $videoSrc = $(this).data( "src" );
            });
            console.log($videoSrc);

            // when the modal is opened autoplay it  
            $('#videoModal').on('shown.bs.modal', function (e) {
                
            // set the video src to autoplay and not to show related video. Youtube related video is like a box of chocolates... you never know what you're gonna get
            $("#video").attr('src',$videoSrc + "?autoplay=1&amp;modestbranding=1&amp;showinfo=0" ); 
            })
              
            // stop playing the youtube video when I close the modal
            $('#videoModal').on('hide.bs.modal', function (e) {
                // a poor man's stop video
                $("#video").attr('src',$videoSrc); 
            })
            // document ready  
            });
        </script>

      <script src="/js/theme.js?v=1.2"></script>
</body>

</html>