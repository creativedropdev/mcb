@extends('layout.default')

@section('content')

@php

if($post->image=="")
{

  preg_match_all('/<img[^>]+>/i',$post->body, $result);

  if(count($result)>0)
  {
   if(count($result[0])>0)
                  {
                  preg_match_all('/(alt|title|src)=("[^"]*")/i',$result[0][0], $result);

                  $image=$result[2];
                  }
  }

  if(!isset($image))
  {
    $image[0]="/storage/".setting('site.default_blog_image');
  

  }

}

@endphp

<section class="section-bg-grey section-padtop-50">
		<div class="mcb-container">
			<div class="row">
				<div class="col-lg-3">
					<div class="blog-links">
						<ul>
							@foreach ($categories as $category)
                        <li class="products-item"><a href="{{ url('blog/'.$category->slug) }}" class="d-flex justify-content-between m-dark p-14">{{ $category->name }}<span>({{ $category->post->where('status', '=', 'PUBLISHED')->count() }}) <i class="fas fa-long-arrow-alt-right ml-3 m-orange"></i></span></a></li>
                     @endforeach
						</ul>
					</div>

					<div class="media-box desktop-media-box">
						<h5 class="mcb-h5 text-white">Any media partnership?</h5>
						<p class="text-white">Send us a message at <span><a href="mailto:%20export@mcb.ae" class="text-white">export@mcb.ae</a></span> now</p>
					</div>
				</div>

				<div class="col-lg-9">
               
					<section class="blog-inner">
						<div class="blog-inner-img">
                     <img src="https://mcb.workspace.destring.com/storage/{{ $post->image }}" alt="image">
                     {{-- <img src="{{ asset('storage/'. $post->image) }}" alt="image"> --}}
						</div>
						<div class="blog-inner-content">
                     <h3 class="mcb-h3 mt-3">{{ $post->title }}</h3>
                     {!! $post->body !!}
							{{-- <h5 class="mcb-h5">Leveraging end user buying phenomenon</h5>
							<p class="mt-5">With the rise of B2B eCommerce, on-demand information access, and omni-channel commerce, customers’ behavior have dramatically shifted from buying wholesale to retail. Significant for business?
							<br>Undeniably so
							<br>According to reports from Forbes and Dubai Chamber of Commerce, companies will cash in on B2B eCommerce and e-commerce which are projected to shoot up at USD6.6 trillion globally and USD69 billion in the Middle East respectively in 2020.
							<br>Yes, end users today have limitless power (literally on their fingertips) to get the product plus services that goes with it such as delivery options, installation, replacement, etc.
							<br>So, how can B2B based enterprises like the bearing aftermarket industry get a fair market share? Let’s count the ways.</p>
							<p><span class="font-weight-bold">Reinvent the business model</span>- Shake up traditional industry practices and join the digital bandwagon. Any idea on the top marketplaces or e-commerce platforms in your region? Let your market research team help you!</p>
							<p><span class="font-weight-bold">Demographics count</span>- Digital natives (Millennials and Gen Z) are flooding not just the workforce but the world’s population bringing with them different mindset and skill set. Study them, learn from them.</p>
							<p><span class="font-weight-bold">Go small on packaging</span>- This is the future of consumption. Smaller packaging means convenience in price thus saving your customer money until the next paycheck. Not to mention, going small gives space saving capacity and portability</p>
							<p>Easy-peasy? Any thoughts? We’d love to hear from you!</p>
							<p>Visit us at Automechanika Johannesburg’s Hall 5, Stand 5F01A from the 18th-21st of September 2019 and check out the NEW 1lb/454g of NTN-SNR's HEAVY DUTY EP High Load grease!</p>
							<h5 class="mcb-h5">When SMALLER just got even better</h5> --}}
						</div>
					</section>


					<section class="section-padbottom-50">
						<div class="media-box responsive-media-box">
							<h5 class="mcb-h5 text-white">Any media partnership?</h5>
							<p class="text-white">Send us a message at <span><a href="mailto:%20export@mcb.ae" class="text-white">export@mcb.ae</a></span> now</p>
						</div>
					</section>
				</div> <!-- col -->
			</div>
		</div>
	</section>
	<!-- blog -->

{{-- 
  <div class="banner-area bg-overlay" id="banner-area" style="background-image:url({{ (isset($image)) ? $image[0] :  Voyager::image( $post->thumbnail('medium')) }});">
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <div class="banner-heading">
                  <h1 class="banner-title">Blog <span></span></h1>
                  <ol class="breadcrumb">
                     <li>Home</li>
                     
                     <li><a href="/blog"> Blog</a></li>
                    
                  </ol>
                  <!-- Breadcumb End -->
               </div>
               <!-- Banner Heading end -->
            </div>
            <!-- Col end-->
         </div>
         <!-- Row end-->
      </div>
      <!-- Container end-->
   </div>
   <!-- Banner area end-->

<section id="main-container" class="main-container pb-120">
		<div class="container">
			<div class="row">

			<div class="col-lg-8 col-md-12">
					<div class="post-content post-single">
						<div class="post-media post-image">
							<img src="{{ (isset($image)) ? rtrim(trim($image[0],'"'),'"') :  Voyager::image( $post->thumbnail('medium')) }}" class="img-fluid" alt="">
                     <div class="post-date">
                        <span class="day">{{$post->created_at->format('d')}}</span>
                        <span class="month">{{$post->created_at->format('M')}}</span>
                     </div> <!-- Post Date End -->
						</div>

						<div class="post-body clearfix">

                     <div class="entry-header">
                        <div class="post-meta">
                            @foreach($post->category as $category)
                           <span class="post-cat">
                            @if($category->icon!="")
                              <i class="fa fa-{{ $category->icon }}"></i>
                              @endif
                              <a href="/blog/{{$category->slug}}">{{$category->name}}</a>
                           </span>

                          @endforeach
                        </div>

                        <h2 class="entry-title">
                        	{{ $post->title }}

            	
             
                          
                        </h2>
                     </div><!-- header end -->

                     <div class="entry-content">
                        {!!$post->body!!}
                        
                     </div><!-- entry content end -->
                     <div class="sharethis-inline-share-buttons"></div>
                    

						</div><!-- post-body end -->
					</div><!-- Post content end -->

          
             


               

				</div>

      

            <div class="col-lg-4 col-md-12">

               <div class="sidebar sidebar-right">

             

               <form method="get" action="/blog">
               <div class="widget widget-search">
                     <div class="input-group">
                        <input name="s" class="form-control" placeholder="Search Blog" type="search" value="@if(isset($searchTerm)){{$searchTerm}}@endif">
                        <button class="input-group-btn">
                           <i class="fa fa-search"></i>
                        </button>

                     </div>
                  </div>
                  </form>

                  

                  <div class="widget">
                     <h3 class="widget-title">Categories <span class="widget-title-dash"></span></h3>
                     <ul class="widget-nav-tabs">
                      
                      @foreach($categories as $category)
                      <li><a href="/blog/{{$category->slug}}"> @if($category->icon!="")
                              <i class="fa fa-{{ $category->icon }}"></i>
                              @endif {{$category->name}}</a>
                         <span class="posts-count">({{$category->post->where('status', '=', 'PUBLISHED')->count()}})</span>
                      </li>
                      @endforeach
                   </ul>
                  </div><!-- Categories end -->

               



               </div><!-- Sidebar end -->
            </div><!-- Sidebar Col end -->

			</div><!-- Main row end -->

		</div><!-- Container end -->
	</section>
    --}}
@stop


