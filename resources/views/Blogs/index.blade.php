@extends('layout.default')

@section('content')
  
   <section class="mcb-slider blog-slider-banner">
		<h1 class="m-0">Our Knowledge &amp;<br>company updates</h1>
	</section>
	<!-- blog-slider -->


<div class="request">
	<div class="mcb-container">
		<div class="row">
			<div class="col-12 col-sm-6 col-md-7 align-self-center res-margintop-20 res-marginbottom-20 res-center">
				<p class="m-0">You've read it right! We will be back at Automechanika Dubai this October 2020!</p>
			</div>
			<div class="col-12 col-sm-6 col-md-5 align-self-center res-marginbottom-20 res-center">
				<button class="btn mcb-btn mcb-btn-orange float-right res-no-float" data-toggle="modal" data-target="#mcbModal"><img src="{{ asset('assets/img/call-icon.png') }}" width="20px" alt=""><span class="ml-2">REQUEST FOR A CALLBACK</span></button>
			</div>
		</div>
	</div>
</div>

<!-- request -->
<section class="section-bg-grey section-padtop-50 for-device-search">
	<div class="mcb-container">
		<div class="row">
			<div class="col">
				<div class="my-blogs">
               <form method="get" action="{{ url('blog') }}">
					   <ul class="search-ul">
					      <li>
	                     <button class="btn btn-light dropdown-toggle m-dark search-blog-btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Search Blog</button>
                        <div class="dropdown-menu">
                           @foreach ($categories as $category)
                              <a class="dropdown-item" href="{{ url('blog/'.$category->slug) }}">{{ $category->name }}</a>   
                           @endforeach
                        </div>
                     </li>
                     <li>
                        <input type="search" name="s" class="form-control" placeholder="Search Blog" value="@if(isset($searchTerm)){{$searchTerm}}@endif">
                     </li>
	               	<li><i class="fas fa-search fa-lg m-blue"></i></li>
                  </ul>
               </form>
				</div>
			</div>
		</div>
	</div>
</section>

	<section class="section-bg-grey section-padtop-50">
		<div class="mcb-container">
			<div class="row">
				<div class="col-lg-3">
					<div class="blog-links">
						<ul>
							@foreach ($categories as $category)
                        <li class="{{ Request::segment(2) == $category->slug ? 'active' : '' }} products-item"><a href="{{ url('blog/'.$category->slug) }}" class="d-flex justify-content-between m-dark p-14">{{ $category->name }}<span>({{ $category->post->where('status', '=', 'PUBLISHED')->count() }}) <i class="fas fa-long-arrow-alt-right ml-3 m-orange"></i></span></a></li>
                     @endforeach
						</ul>
					</div>

					<div class="media-box desktop-media-box">
						<h5 class="mcb-h5 text-white">Any media partnership?</h5>
						<p class="text-white">Send us a message at <span><a href="mailto:%20export@mcb.ae" class="text-white">export@mcb.ae</a></span> now</p>
					</div>
				</div>

				<div class="col-lg-9">
					<div class="clients my-blogs">
						<div class="row">
							<div class="col-md-12">
                        <form method="get" action="{{ url('blog') }}">
                           <ul class="search-ul for-desktop-search">
								   	<li>
								         <button class="btn btn-light dropdown-toggle m-dark search-blog-btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Search Blog</button>
					                  <div class="dropdown-menu">
                                    @foreach ($categories as $category)
                                       <a class="dropdown-item" href="{{ url('blog/'.$category->slug) }}">{{ $category->name }}</a>   
                                    @endforeach
					                  </div>
								   	</li>
								   	<li>
                                 <input type="search" name="s" class="form-control" placeholder="Search Blog" value="@if(isset($searchTerm)){{$searchTerm}}@endif">
                              </li>
								   	<li><i class="fas fa-search fa-lg m-blue"></i></li>
								   </ul>
                        </form>
                        </div>

                        <div class="row">
                           @foreach ($posts as $post)
                              <div class="col-md-4">
                                 <a href="{{ url('blog/'. $post->slug) }}">
						                  <div class="blog-card">
											{{-- <img src="{{ asset('storage/'. $post->image) }}" alt="./"> --}}
											<img src="https://mcb.workspace.destring.com/storage/{{ $post->image }}" alt="image">
						                  	<p class="p-14 mt-3">{{ $post->created_at->format('d M Y') }}</p>
						                  	<h5 class="mcb-h5">{{ $post->title }}</h5>
						                  	<div class="blog-read d-flex justify-content-between align-items-center">
						                  		<p class="m-orange mcb-trans mb-0">READ MORE</p>
						                  		<i class="fas fa-long-arrow-alt-right m-orange"></i>
						                  	</div>
                                    </div>
                                 </a>
                              </div>
                           @endforeach   
                        
                        </div>
						</div>
               </div>
               
               <div class="row mt-4 section-padbottom-50">
                   <div class="col">
                    {{ $posts->links() }}
                  </div>
               </div>
					<!-- clients -->

					<section class="section-padbottom-50">
						<div class="media-box responsive-media-box">
							<h5 class="mcb-h5 text-white">Any media partnership?</h5>
							<p class="text-white">Send us a message at <span><a href="mailto:%20export@mcb.ae" class="text-white">export@mcb.ae</a></span> now</p>
						</div>
					</section>
				</div> <!-- col -->
			</div>
		</div>
	</section>
   <!-- blog -->
   
@stop


