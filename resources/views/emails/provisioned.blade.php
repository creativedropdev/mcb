@extends('beautymail::templates.ark')

@section('content')

    @include('beautymail::templates.ark.heading', [
		'heading' => 'Welcome to middlehost!',
		'level' => 'h1'
	])

    @include('beautymail::templates.ark.contentStart')

    <h4 class="secondary"><strong>Your Event Blog is configured successfuly!</strong></h4>
    <p>&nbsp;</p>

    <p>WP Admin: {{ $url }}</p>
    <p>Username: {{ $username }}</p>
    <p>Password: {{ $password }}</p>

    @include('beautymail::templates.ark.contentEnd')



    @include('beautymail::templates.ark.contentStart')

    <h4 class="secondary"><strong>Please Change the Nameserver of your domain "{{ $domain }}" to following nameservers</strong></h4>
    <p>&nbsp;</p>
    <p>{{$ns1}}</p>
    <p>{{$ns2}}</p>

    @include('beautymail::templates.ark.contentEnd')

@stop