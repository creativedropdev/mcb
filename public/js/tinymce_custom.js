function tinymce_init_callback(editor) {
    editor.remove();
    editor = null;

    // //       tinymce.init({
    // //   selector: 'textarea.richTextBox',
    // //   toolbar: 'myCustomToolbarButton',
    // //   setup: (editor) => {
    // //     editor.ui.registry.addButton('myCustomToolbarButton', {
    // //       text: 'My Custom Button',
    // //       onAction: () => alert('Button clicked!')
    // //     });
    // //   }
    // // });

    // alert(tinymce.majorVersion + '.' + tinymce.minorVersion)
    
    css=$("textarea[name='custom_css']");
    if(css)
    {
        css=encodeURIComponent(css.text());
    }
    else
    {
        css="";
    }
    
     $("#colorPicker").on('clic', function(){
        
       alert("sadass"); 
    });
    tinymce.init({
        menubar: false,
        selector: "textarea.richTextBox",
        content_css:
            "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css,http://mcb.workspace.destring.com/css/bootstrap-editor.min.css,http://mcb.workspace.destring.com/css/features-block.css,https://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css,http://mcb.workspace.destring.com/custom_css?css="+css,

        skin_url:
            $('meta[name="assets-path"]').attr("content") +
            "?path=js/skins/voyager",
        min_height: 600,
        resize: "vertical",
        plugins: "link, fontawesome, image, code, table, textcolor, lists",
        valid_elements:"*[*]",
        extended_valid_elements:"*[*]",
       // extended_valid_elements:  "i[*],span[*],span,span[class],input[id|name|value|type|class|style|required|placeholder|autocomplete|onclick]",
        file_browser_callback: function(field_name, url, type, win) {
            if (type == "image") {
                $("#upload_file").trigger("click");
            }
        },
        toolbar:
            "layoutBlocks fontawesome styleselect fontsizeselect bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist outdent indent | link image table | code",
            textcolor_map: [
    "000000", "Black",
    "993300", "Burnt orange",
    "333300", "Dark olive",
    "003300", "Dark green",
    "003366", "Dark azure",
    "000080", "Navy Blue",
    "333399", "Indigo",
    "333333", "Very dark gray",
    "800000", "Maroon",
    "FF6600", "Orange",
    "808000", "Olive",
    "008000", "Green",
    "008080", "Teal",
    "0000FF", "Blue",
    "666699", "Grayish blue",
    "808080", "Gray",
    "FF0000", "Red",
    "FF9900", "Amber",
    "99CC00", "Yellow green",
    "339966", "Sea green",
    "33CCCC", "Turquoise",
    "3366FF", "Royal blue",
    "800080", "Purple",
    "999999", "Medium gray",
    "FF00FF", "Magenta",
    "FFCC00", "Gold",
    "FFFF00", "Yellow",
    "00FF00", "Lime",
    "00FFFF", "Aqua",
    "00CCFF", "Sky blue",
    "993366", "Red violet",
    "FFFFFF", "White",
    "FF99CC", "Pink",
    "FFCC99", "Peach",
    "FFFF99", "Light yellow",
    "CCFFCC", "Pale green",
    "CCFFFF", "Pale cyan",
    "99CCFF", "Light sky blue",
    "0076c0", "Brand Color 1",
     "ee3424", "Brand Color 2",
    "455560", "Brand Color 3",
     "6a737b", "Brand Color 4",

  ],
 
    textcolor_cols: "10",
      textcolor_rows: "10",
//       color_picker_callback: function(callback, value) {
           
//           var x = document.createElement("INPUT");
//       x.setAttribute("type", "color");
//       x.setAttribute("id", "colorPicker");
//       document.body.appendChild(x);
//     $("#colorPicker").trigger('click');
//     callback( $("#colorPicker").val());
//   },
        convert_urls: false,
        image_caption: true,
        image_title: true,
        fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt 72pt",
        // init_instance_callback: function (editor) {
        //     if (typeof tinymce_init_callback !== "undefined") {
        //         tinymce_init_callback(editor);
        //     }
        // },
        setup: function(ed) {
            ed.on('init', function() {
                $(ed.getBody()).on('click', 'a[href]', function(e) {
                    // window.location.href = $(e.currentTarget).attr('href');
                    // console.log($(e.currentTarget).attr("data-toggle"));
                    // console.log(e);
                    if (e.altKey && $(e.currentTarget).attr("data-toggle") == "tab") {

                        content = $(ed.getBody());
                        content.find(".tab-pane").removeClass("active");

                        content.find(".tab-pane").removeClass("show");
                        // console.log($(e.currentTarget).attr('href'))
                        content.find($(e.currentTarget).attr('href')).addClass("active")
                        content.find($(e.currentTarget).attr('href')).addClass("show")

                        // console.log();
                        // console.log(content.html());
                        // console.log($($(e.currentTarget).attr('href')).tab('show');
                    }
                });
            });
            ed.addButton("layoutBlocks", {
                text: "Layout Blocks",
                type: "menubutton",
                icon: false,
                menu: [
                    {
                        text: "[75%|25%]",
                        onclick: function() {
                            ed.insertContent(
                                '<div class="row"><div class="col-lg-8"></div><div class="col-lg-4"></div></div>&nbsp;'
                            );
                        }
                    },
                    {
                        text: "[25%|75%]",
                        icon: false,

                        onclick: function() {
                            ed.insertContent(
                                '<div class="row"><div class="col-lg-4"></div><div class="col-lg-8"></div></div>&nbsp;'
                            );
                        }
                    },

                    {
                        text: "[50%|50%]",
                        icon: false,

                        onclick: function() {
                            ed.insertContent(
                                '<div class="row"><div class="col-lg-6"></div><div class="col-lg-6"></div></div>&nbsp;'
                            );
                        }
                    },

                    {
                        text: "[25%|25%|25%|25%]",
                        icon: false,

                        onclick: function() {
                            ed.insertContent(
                                '<div class="row"><div class="col-lg-3"></div><div class="col-lg-3"></div><div class="col-lg-3"></div><div class="col-lg-3"></div></div>&nbsp;'
                            );
                        }
                    },

                    {
                        text: "[33%|33%|33%]",
                        icon: false,

                        onclick: function() {
                            ed.insertContent(
                                '<div class="row"><div class="col-lg-4"></div><div class="col-lg-4"></div><div class="col-lg-4"></div></div>&nbsp;'
                            );
                        }
                    },
                     {
                        text: "1 Block with Icon",
                        icon: false,

                        onclick: function() {
                            ed.insertContent(
                                '<div id="features" class="container">\
                                <div class="row">\
                                <div class="col-md-12 feature"><span>&nbsp;<span class="fa fa-fire"></span>&nbsp;</span>\
                                <h3>Lorem ipsum dolor</h3>\
                                <div class="title_border">&nbsp;</div>\
                                <p>Praediximus enim Montium sub ipso vivendi termino his vocabulis appellatos fabricarum culpasse tribunos ut adminicula futurae molitioni pollicitos</p>\
                                </div>\
                                </div>\
                                </div> &nbsp;'
                            );
                        }
                    },
                    {
                        text: "2 Blocks with Icons",
                        icon: false,

                        onclick: function() {
                            ed.insertContent(
                                '<div id="features" class="container">\
                                <div class="row">\
                                <div class="col-md-6 feature"><span>&nbsp;<span class="fa fa-fire"></span>&nbsp;</span>\
                                <h3>Lorem ipsum dolor</h3>\
                                <div class="title_border">&nbsp;</div>\
                                <p>Praediximus enim Montium sub ipso vivendi termino his vocabulis appellatos fabricarum culpasse tribunos ut adminicula futurae molitioni pollicitos</p>\
                                </div>\
                                <div class="col-md-6 feature"><span><span class="fa fa-plug"></span>&nbsp;</span>\
                                <h3>Lorem ipsum dolor</h3>\
                                <div class="title_border">&nbsp;</div>\
                                <p>Praediximus enim Montium sub ipso vivendi termino his vocabulis appellatos fabricarum culpasse tribunos ut adminicula futurae molitioni pollicitos</p>\
                                </div>\
                                </div>\
                                </div> &nbsp;'
                            );
                        }
                    },

                    {
                        text: "3 Blocks with Icons",
                        icon: false,

                        onclick: function() {
                            ed.insertContent(
                                '<div id="features" class="container">\
                                <div class="row">\
                                <div class="col-md-4 feature"><span>&nbsp;<span class="fa fa-fire"></span>&nbsp;</span>\
                                <h3>Lorem ipsum dolor</h3>\
                                <div class="title_border">&nbsp;</div>\
                                <p>Praediximus enim Montium sub ipso vivendi termino his vocabulis appellatos fabricarum culpasse tribunos ut adminicula futurae molitioni pollicitos</p>\
                                </div>\
                                <div class="col-md-4 feature"><span><span class="fa fa-plug"></span>&nbsp;</span>\
                                <h3>Lorem ipsum dolor</h3>\
                                <div class="title_border">&nbsp;</div>\
                                <p>Praediximus enim Montium sub ipso vivendi termino his vocabulis appellatos fabricarum culpasse tribunos ut adminicula futurae molitioni pollicitos</p>\
                                </div>\
                                <div class="col-md-4 feature"><span><span class="fa fa-exclamation-circle"></span>&nbsp;</span>\
                                <h3>Lorem ipsum dolor</h3>\
                                <div class="title_border">&nbsp;</div>\
                                <p>Praediximus enim Montium sub ipso vivendi termino his vocabulis appellatos fabricarum culpasse tribunos ut adminicula futurae molitioni pollicitos</p>\
                                </div>\
                                </div>\
                                </div> &nbsp;'
                            );
                        }
                    }
                ]
            });
        }
    });
}
//   tinymce.init({
//     selector: 'textarea.richTextBox',
//     skin: 'voyager',
//     min_height: 600,
//     resize: 'vertical',
//     plugins: ' preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern',
//     extended_valid_elements: 'input[id|name|value|type|class|style|required|placeholder|autocomplete|onclick]',
//     file_browser_callback: function (field_name, url, type, win) {
//         if (type == 'image') {
//             $('#upload_file').trigger('click');
//         }
//     },
//     toolbar: 'styleselect bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist outdent indent | link image table youtube giphy | codesample code',
//     convert_urls: false,
//     image_caption: true,
//     image_title: true,

// });

function tinymce_setup_callback(editor) {
    // var editor=tinymce.activeEditor; //or tinymce.editors[0], or loop, whatever
    //add a button to the editor buttons
    // editor.addButton('mysecondbutton', {
    //   text: 'My second button',
    //   icon: false,
    //   onclick: function () {
    //     editor.insertContent('&nbsp;<b>It\'s my second button!</b>&nbsp;');
    //   }
    // });
    // editor.addButton('keywords', {
    //               text: 'Insert Keywords',
    //               class: 'MyCoolBtn,someOtherClass',
    //               icon: false,
    //               onclick: function () {
    //                   if($(this.id).hasClass("mce-active"))
    //                       EditorMethods.removeKeywordsToolbar(this.id);
    //                   else
    //                       EditorMethods.displayKeywordsToolbar(this.id);
    //               }
    //           });
    // // editor.ui.registry.addButton('customInsertButton', {
    // //       text: 'My Button',
    // //       onAction: function (_) {
    // //         editor.insertContent('&nbsp;<strong>It\'s my button!</strong>&nbsp;');
    // //     }
    // //       });
    // tinymce.PluginManager.add('example', function(editor, url) {
    //     // Add a button that opens a window
    // 	editor.addButton('example', {
    // 		text: 'My button',
    // 		icon: false,
    // 		onclick: function() {
    // 			// Open window
    // 			editor.windowManager.open({
    // 				title: 'Example plugin',
    // 				body: [
    // 					{type: 'textbox', name: 'title', label: 'Title'}
    // 				],
    // 				onsubmit: function(e) {
    // 					// Insert content when the window form is submitted
    // 					editor.insertContent('Title: ' + e.data.title);
    // 				}
    // 			});
    // 		}
    // 	});
    // 	// Adds a menu item to the tools menu
    // 	editor.addMenuItem('example', {
    // 		text: 'Example plugin',
    // 		context: 'tools',
    // 		onclick: function() {
    // 			// Open window with a specific url
    // 			editor.windowManager.open({
    // 				title: 'TinyMCE site',
    // 				url: 'http://www.tinymce.com',
    // 				width: 400,
    // 				height: 300,
    // 				buttons: [{
    // 					text: 'Close',
    // 					onclick: 'close'
    // 				}]
    // 			});
    // 		}
    // 	});
    // });
    // //the button now becomes
    // var button=editor.buttons['mysecondbutton'];
    //find the buttongroup in the toolbar found in the panel of the theme
    // var bg=editor.panel.find('toolbar buttongroup')[0];
    //without this, the buttons look weird after that
    // bg._lastRepaintRect=bg._layoutRect;
    //append the button to the group
    // bg.append(button);
    //...
    // alert("sd");
    //       tinymce.init({
    //   selector: 'textarea.richTextBox',
    //   toolbar: 'myCustomToolbarButton',
    //   setup: (editor) => {
    //     editor.ui.registry.addButton('myCustomToolbarButton', {
    //       text: 'My Custom Button',
    //       onAction: () => alert('Button clicked!')
    //     });
    //   }
    // });
}
