function tinymce_init_callback(editor)
{
          editor.remove();
      editor = null;

// //       tinymce.init({
// //   selector: 'textarea.richTextBox',
// //   toolbar: 'myCustomToolbarButton',
// //   setup: (editor) => {
// //     editor.ui.registry.addButton('myCustomToolbarButton', {
// //       text: 'My Custom Button',
// //       onAction: () => alert('Button clicked!')
// //     });
// //   }
// // });

// alert(tinymce.majorVersion + '.' + tinymce.minorVersion)
  tinymce.init({
    menubar: false,
    selector:'textarea.richTextBox',
    content_css: 'https://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css',
    skin_url: $('meta[name="assets-path"]').attr('content')+'?path=js/skins/voyager',
    min_height: 600,
    resize: 'vertical',
    plugins: 'link, image, code, table, textcolor, lists',
    extended_valid_elements : 'input[id|name|value|type|class|style|required|placeholder|autocomplete|onclick]',
    file_browser_callback: function(field_name, url, type, win) {
            if(type =='image'){
              $('#upload_file').trigger('click');
            }
        },
    toolbar: 'example styleselect bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist outdent indent | link image table | code',
    convert_urls: false,
    image_caption: true,
    image_title: true,
    // init_instance_callback: function (editor) {
    //     if (typeof tinymce_init_callback !== "undefined") {
    //         tinymce_init_callback(editor);
    //     }
    // },
    setup: function (ed) {
         ed.addButton('example', {
         title: 'My title',
         text: "[Text|Image]",
         icon: false,
        
         onclick: function() {
            ed.insertContent('<div class="row"><div class="col-lg-8">TEXT HERE</div><div class="col-lg-4">IMAGE HERE</div></div>');
         }
      });

     
   		
    }

    
  });


    //   tinymce.init({
    //     selector: 'textarea.richTextBox',
    //     skin: 'voyager',
    //     min_height: 600,
    //     resize: 'vertical',
    //     plugins: ' preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern',
    //     extended_valid_elements: 'input[id|name|value|type|class|style|required|placeholder|autocomplete|onclick]',
    //     file_browser_callback: function (field_name, url, type, win) {
    //         if (type == 'image') {
    //             $('#upload_file').trigger('click');
    //         }
    //     },
    //     toolbar: 'styleselect bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist outdent indent | link image table youtube giphy | codesample code',
    //     convert_urls: false,
    //     image_caption: true,
    //     image_title: true,
        

    // });
}


function tinymce_setup_callback(editor)
{


	// var editor=tinymce.activeEditor; //or tinymce.editors[0], or loop, whatever

//add a button to the editor buttons
// editor.addButton('mysecondbutton', {
//   text: 'My second button',
//   icon: false,
//   onclick: function () {
//     editor.insertContent('&nbsp;<b>It\'s my second button!</b>&nbsp;');
//   }
// });

// editor.addButton('keywords', {
//               text: 'Insert Keywords',
//               class: 'MyCoolBtn,someOtherClass', 
//               icon: false,
//               onclick: function () {

//                   if($(this.id).hasClass("mce-active"))
//                       EditorMethods.removeKeywordsToolbar(this.id);
//                   else
//                       EditorMethods.displayKeywordsToolbar(this.id);  
//               }
//           });

// // editor.ui.registry.addButton('customInsertButton', {
// //       text: 'My Button',
// //       onAction: function (_) {
// //         editor.insertContent('&nbsp;<strong>It\'s my button!</strong>&nbsp;');
// //     }
// //       });

// tinymce.PluginManager.add('example', function(editor, url) {
//     // Add a button that opens a window
// 	editor.addButton('example', {
// 		text: 'My button',
// 		icon: false,
// 		onclick: function() {
// 			// Open window
// 			editor.windowManager.open({
// 				title: 'Example plugin',
// 				body: [
// 					{type: 'textbox', name: 'title', label: 'Title'}
// 				],
// 				onsubmit: function(e) {
// 					// Insert content when the window form is submitted
// 					editor.insertContent('Title: ' + e.data.title);
// 				}
// 			});
// 		}
// 	});

// 	// Adds a menu item to the tools menu
// 	editor.addMenuItem('example', {
// 		text: 'Example plugin',
// 		context: 'tools',
// 		onclick: function() {
// 			// Open window with a specific url
// 			editor.windowManager.open({
// 				title: 'TinyMCE site',
// 				url: 'http://www.tinymce.com',
// 				width: 400,
// 				height: 300,
// 				buttons: [{
// 					text: 'Close',
// 					onclick: 'close'
// 				}]
// 			});
// 		}
// 	});
// });



// //the button now becomes
// var button=editor.buttons['mysecondbutton'];

//find the buttongroup in the toolbar found in the panel of the theme
// var bg=editor.panel.find('toolbar buttongroup')[0];

//without this, the buttons look weird after that
// bg._lastRepaintRect=bg._layoutRect;

//append the button to the group
// bg.append(button);


    //...
// alert("sd");
//       tinymce.init({
//   selector: 'textarea.richTextBox',
//   toolbar: 'myCustomToolbarButton',
//   setup: (editor) => {
//     editor.ui.registry.addButton('myCustomToolbarButton', {
//       text: 'My Custom Button',
//       onAction: () => alert('Button clicked!')
//     });
//   }
// });


}