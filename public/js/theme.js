//Sidebar

$(document).ready(function () {
    
    $("#sidebar").mCustomScrollbar({
        theme: "minimal"
    });

    $('#dismiss, .overlay').on('click', function () {
        $('#sidebar').removeClass('active');
        $('.overlay').removeClass('active');
    });

    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').addClass('active');
        $('.overlay').addClass('active');
        $('.collapse.in').toggleClass('in');
        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });

    //secondary-sidebar
    $("#secondary-sidebar").mCustomScrollbar({
        theme: "minimal"
    });

    $('#secondary-dismiss, .overlay').on('click', function () {
        $('#secondary-sidebar').removeClass('active');
        $('.overlay').removeClass('active');
    });

    $('.secondarySidebarCollapse').on('click', function () {
        $('#secondary-sidebar').addClass('active');
        $('.overlay').addClass('active');
        $('.collapse.in').toggleClass('in');
        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });
});

$("#searchingToggle").click(function () {
    $(".responsive-searchbar").toggle('slow');
});

$("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
    e.preventDefault();
    $(this).siblings('a.active').removeClass("active");
    $(this).addClass("active");
    var index = $(this).index();
    $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
    $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
});

